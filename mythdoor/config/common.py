"""Common settings and globals."""
from __future__ import absolute_import

import os

from datetime import timedelta
from os.path import abspath, basename, dirname, join, normpath
from os import environ
from sys import path

from django.utils.translation import ugettext_lazy as _

import djcelery
djcelery.setup_loader()

ROOT_PATH = os.path.dirname(__file__)
path.append(ROOT_PATH)

SITE_NAME = os.path.basename(ROOT_PATH)
SITE_DOMAIN = SITE_NAME + ".com"

DEBUG = False

########## MANAGER CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#admins
ADMINS = (
	('Dang Nguyen', 'teammythdoor@gmail.com'),
)

ADMINS_USERNAME = (
	"dtn29",
	"dtn1712",
)
# See: https://docs.djangoproject.com/en/dev/ref/settings/#managers
MANAGERS = ADMINS
########## END MANAGER CONFIGURATION

SENDGRID_API_KEY = environ.get("SENDGRID_API_KEY")
DEFAULT_FROM_EMAIL = environ.get("DEFAULT_FROM_EMAIL")

EMAIL_BACKEND = "sgbackend.SendGridBackend"
EMAIL_HOST = 'smtp.sendgrid.net'
EMAIL_HOST_USER = environ.get("EMAIL_HOST_USER")
EMAIL_HOST_PASSWORD = environ.get("EMAIL_HOST_PASSWORD")

EMAIL_PORT = 587
EMAIL_USE_TLS = True

########## GENERAL CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#time-zone
TIME_ZONE = 'America/Los_Angeles'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#language-code
LANGUAGE_CODE = 'en-us'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#site-id
SITE_ID = 1

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-i18n
USE_I18N = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-l10n
USE_L10N = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-tz
USE_TZ = True
########## END GENERAL CONFIGURATION


########## MEDIA CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-root
MEDIA_ROOT = os.path.join(ROOT_PATH, 'assets/media')

# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-url
MEDIA_URL = '/media/'
########## END MEDIA CONFIGURATION


########## STATIC FILE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-root
STATIC_ROOT = 'staticfiles'

# See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS
STATICFILES_DIRS = (
	os.path.join(ROOT_PATH, 'assets/static'),
)

# See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#staticfiles-finders
STATICFILES_FINDERS = (
	'django.contrib.staticfiles.finders.FileSystemFinder',
	'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)
########## END STATIC FILE CONFIGURATION


########## SECRET CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
SECRET_KEY = "chbrc3p7q%g9e80a(&bm$ci6ygdc_ak99q6ep90!#evq7yc@@@"

# See: https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-FIXTURE_DIRS
FIXTURE_DIRS = (
	os.path.join(ROOT_PATH, 'fixtures'),
)
########## END FIXTURE CONFIGURATION

DEFAULT_TEMPLATE_DIRS = [
	os.path.join(ROOT_PATH, 'assets/templates'),
	os.path.join(ROOT_PATH, 'assets/templates/sites'),
	os.path.join(ROOT_PATH, 'assets/templates/common'),
	os.path.join(ROOT_PATH, 'assets/templates/sites/apps'),
	os.path.join(ROOT_PATH, 'assets/templates/sites/apps/auth'),
]

DEFAULT_TEMPLATE_CONTEXT_PROCESSOR = [
	'django.contrib.auth.context_processors.auth',
	'django.core.context_processors.debug',
	'django.core.context_processors.i18n',
	'django.core.context_processors.media',
	'django.core.context_processors.static',
	'django.core.context_processors.tz',
	'django.contrib.messages.context_processors.messages',
	'django.core.context_processors.request',
	'django_mobile.context_processors.flavour',
	"mythdoor.apps.main.context_processors.site_data",
	"mythdoor.apps.main.context_processors.global_data",
	"mythdoor.apps.main.context_processors.constants_data",
]

DEFAULT_TEMPLATE_EXTENSIONS = [
    "jinja2.ext.do",
    "jinja2.ext.loopcontrols",
    "jinja2.ext.with_",
    "jinja2.ext.i18n",
    "jinja2.ext.autoescape",
    "django_jinja.builtins.extensions.CsrfExtension",
    "django_jinja.builtins.extensions.CacheExtension",
    "django_jinja.builtins.extensions.TimezoneExtension",
    "django_jinja.builtins.extensions.UrlsExtension",
    "django_jinja.builtins.extensions.StaticFilesExtension",
    "django_jinja.builtins.extensions.DjangoFiltersExtension",
]

DEFAULT_TEMPLATE_FILTERS = {
    "get_dictionary_item_value": "mythdoor.apps.main.templatetags.filters.get_dictionary_item_value",
    "display_elapse_time": "mythdoor.apps.main.templatetags.filters.display_elapse_time",
   	"words_first_char_upper": "mythdoor.apps.main.templatetags.filters.words_first_char_upper",
   	"first_word_only": "mythdoor.apps.main.templatetags.filters.first_word_only",
   	"first_char_only": "mythdoor.apps.main.templatetags.filters.first_char_only",
   	"initial": "mythdoor.apps.main.templatetags.filters.initial",
}

DEFAULT_TEMPLATE_GLOBALS = {
	"load_css": "mythdoor.apps.main.templatetags.static_loader.load_css",
    "load_js": "mythdoor.apps.main.templatetags.static_loader.load_js",
    "get_image_url": "mythdoor.apps.main.templatetags.global_functions.get_image_url",
    "get_image_element": "mythdoor.apps.main.templatetags.global_functions.get_image_element",
    "shorten_content": "mythdoor.apps.main.templatetags.global_functions.shorten_content",
}

DEFAULT_TEMPLATE_FILE_EXTENSION = ".jinja.html"

TEMPLATES = [
	{
        "NAME": "jinja",
        "BACKEND": "django_jinja.backend.Jinja2",
        'DIRS': DEFAULT_TEMPLATE_DIRS,
        "OPTIONS": {
            "match_extension": DEFAULT_TEMPLATE_FILE_EXTENSION,
            "match_regex": r"^(?!admin/|!debug_toolbar/).*",
            "newstyle_gettext": True,
            'context_processors': DEFAULT_TEMPLATE_CONTEXT_PROCESSOR,
            "filters": DEFAULT_TEMPLATE_FILTERS,
            "globals": DEFAULT_TEMPLATE_GLOBALS,
            "extensions": DEFAULT_TEMPLATE_EXTENSIONS,
            "bytecode_cache": {
                "name": "default",
                "backend": "django_jinja.cache.BytecodeCache",
                "enabled": False,
            },
            "autoescape": False,
            "translation_engine": "django.utils.translation",
        }
    },
    {
        "NAME": "django",
        "BACKEND": 'django.template.backends.django.DjangoTemplates',
        'DIRS': DEFAULT_TEMPLATE_DIRS,
        'OPTIONS': {
            'context_processors': DEFAULT_TEMPLATE_CONTEXT_PROCESSOR,
            "loaders": [
        		('django_mobile.loader.CachedLoader', [
					  'django_mobile.loader.Loader',
					  'django.template.loaders.filesystem.Loader',
					  'django.template.loaders.app_directories.Loader',
				]),
            ]
        },
    },
]


ROOT_URLCONF = '%s.urls' % SITE_NAME


DJANGO_APPS = (
	# Default Django apps:
	'django.contrib.auth',
	'django.contrib.contenttypes',
	'django.contrib.sessions',
	#'django.contrib.sites',
	'django.contrib.messages',
	'django.contrib.staticfiles',
	"django.contrib.gis",

	# Useful template tags:
	'django.contrib.humanize',

	# Admin panel and documentation:
	'django.contrib.admin',
	#'django.contrib.admindocs',
)

THIRD_PARTY_APPS = (

	# Asynchronous task queue:
	'djcelery',

	# Search
	"haystack",

	# Metrics
	"app_metrics",

	# Others'
	'django_ajax',
	"django_jinja",
	"django_redis",
	'django_mobile',
	"djrill",
	'tastypie',
	'crispy_forms',
	"kombu",
	'kombu.transport.django',
	"firebase_token_generator",
	"clearbit",
	"timelog",
	"pyzipcode",
	"cloudinary",

	"imagekit",
	'defender',

	#'sorl.thumbnail',
	# 'easy_thumbnails',
	# 'django_jinja.contrib._easy_thumbnails',
)

LOCAL_APPS = (
	"mythdoor.apps.about",
	"mythdoor.apps.ajax",
	"mythdoor.apps.api",
	"mythdoor.apps.account",
	"mythdoor.apps.company",
	"mythdoor.apps.mailer",
	"mythdoor.apps.main",
	"mythdoor.apps.member",
	"mythdoor.apps.message",
	"mythdoor.apps.post",
	"mythdoor.apps.photo",
	"mythdoor.apps.search",
)

INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

TIMELOG_LOG = os.path.join(ROOT_PATH,"logs","timelog.log")

SITE_LOG_HANDLER = "logfile_" + SITE_NAME

LOGGING = {
	'version': 1,
	'disable_existing_loggers': False,
	'formatters': {
		'verbose': {
			'format' : "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
			'datefmt' : "%d/%b/%Y %H:%M:%S"
		},
		'simple': {
			'format': '%(levelname)s %(message)s'
		},
		'plain': {
			'format': '%(asctime)s %(message)s'
		},
	},
	'filters': {
		'require_debug_true': {
			'()': 'django.utils.log.RequireDebugTrue',
		},
		'require_debug_false': {
			'()': 'django.utils.log.RequireDebugFalse',
		},
	},
	'handlers': {
		'console':{
			'level': 'DEBUG',
			'class': 'logging.StreamHandler',
			'filters': ['require_debug_true'],
			'formatter': 'simple'
		},
		'mail_admins': {
			'level': 'ERROR',
			'filters': ['require_debug_false'],
			'class': 'django.utils.log.AdminEmailHandler',
			'formatter': 'verbose'
		},
		SITE_LOG_HANDLER: {
			'level': 'DEBUG',
			'class': 'logging.handlers.RotatingFileHandler',
			'filename':  os.path.join(ROOT_PATH,"logs",SITE_NAME + ".log"),
			'maxBytes': 1024 * 1024 * 5,  # 5 MB
			'backupCount': 5,
			'formatter': 'verbose'
		},
		'logfile_request': {
			'level': 'DEBUG',
			'class': 'logging.handlers.RotatingFileHandler',
			'filename':  os.path.join(ROOT_PATH, "logs/request.log"),
			'maxBytes': 1024 * 1024 * 5,  # 5 MB
			'backupCount': 5,
			'formatter': 'verbose'
		},
		'timelog': {
			'level': 'DEBUG',
			'class': 'logging.handlers.RotatingFileHandler',
			'filename': TIMELOG_LOG,
			'maxBytes': 1024 * 1024 * 5,  # 5 MB
			'backupCount': 5,
			'formatter': 'plain',
		},

	},
	'loggers': {
		'django': {
			'handlers': ['console'],
			'propagate': True,
		},
		'django.request': {
			'handlers': ['logfile_request'],
			'level': 'ERROR',
			'propagate': False,
		},
		SITE_NAME: {
			'handlers': [SITE_LOG_HANDLER],
			'level': 'DEBUG',
			'propogate': True,
		},
		'timelog.middleware': {
			'handlers': ['timelog'],
			'level': 'DEBUG',
			'propogate': False,
		}
	},
}

CACHE_OBJECT_LIST_TIMEOUT = 60 * 10


########## CELERY CONFIGURATION
# See: http://celery.readthedocs.org/en/latest/configuration.html#celery-task-result-expires
CELERY_TASK_RESULT_EXPIRES = timedelta(minutes=30)

# See: http://docs.celeryproject.org/en/master/configuration.html#std:setting-CELERY_CHORD_PROPAGATES
CELERY_CHORD_PROPAGATES = True

CELERY_TIMEZONE = 'UTC'

CELERYBEAT_SCHEDULER = 'djcelery.schedulers.DatabaseScheduler'

from datetime import timedelta
from celery.schedules import crontab

CELERYBEAT_SCHEDULE = {
	'check-post-status-daily': {
		'task': 'mythdoor.apps.main.tasks.check_post_status',
		'schedule': crontab(minute=0, hour=0),
	},
	'data-backup-daily': {
		'task': 'mythdoor.apps.main.tasks.data_backup',
		'schedule': crontab(minute=0, hour=0),
	},
	'update-search-index-every-hour': {
		'task': 'mythdoor.apps.search.tasks.update_search_index',
		'schedule': timedelta(minutes=60),
	},
	'renew-post-cache-every-10-min': {
		'task': 'mythdoor.apps.main.tasks.renew_post_cache',
		'schedule': timedelta(minutes=CACHE_OBJECT_LIST_TIMEOUT)
	}
}

CELERY_ACCEPT_CONTENT = ['pickle', 'json', 'msgpack', 'yaml']
########## END CELERY CONFIGURATION


########## WSGI CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#wsgi-application
WSGI_APPLICATION = 'wsgi.application'
########## END WSGI CONFIGURATION


AUTH_PROFILE_MODULE = 'main.UserProfile'

SERIALIZATION_MODULES = {
	'json': "django.core.serializers.json",
}

AUTHENTICATION_BACKENDS = (
	'mythdoor.apps.account.backends.AuthenticationBackend',
	'django.contrib.auth.backends.ModelBackend',
)

ACCOUNT_AUTHENTICATION_METHOD = 'email'
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_PASSWORD_MIN_LENGTH = 4
ACCOUNT_EMAIL_VERIFICATION = "none"
ACCOUNT_LOGOUT_ON_GET = True

#HAYSTACK_SIGNAL_PROCESSOR = 'haystack.signals.RealtimeSignalProcessor'
#HAYSTACK_DEFAULT_OPERATOR = 'OR'

LOGIN_URL = '/account/login'
LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = '/'

ACCOUNT_USERNAME_BLACKLIST = [
	'admin','signup','login','password',"accounts"
	'logout','confirm_email','search','settings',
	'buzz','messages',"about",'api','asset','photo',
	'feeds','friends'
]

GEOIP_PATH = os.path.join(ROOT_PATH, "db/geolocation")  ### path for geoip dat

GEOIP_DATABASE = os.path.join(ROOT_PATH, "db/geolocation/GeoLiteCity.dat")

CITIES_FILES = {
	'city': {
	   'filename': 'cities1000.zip',
	   'urls':     ['http://download.geonames.org/export/dump/'+'{filename}']
	},
}

MAX_MANDRILL_EMAIL_ALLOW = 12000

OW_LY_API_KEY = "6sv891CJpDcuiz8eyRHfy"

CRISPY_TEMPLATE_PACK = 'bootstrap3'

APPEND_SLASH = False
TASTYPIE_ALLOW_MISSING_SLASH = True

TASTYPIE_DEFAULT_FORMATS = ['json']

API_LIMIT_PER_PAGE = 100

KEY_PREFIX = SITE_NAME

CLEARBIT_API_KEY = "c5dd6651224810a64a4ae3c920aa5e5b"
CLEARBIT_API_PERSON_VERSION = "2015-05-27"
CLEARBIT_API_COMPANY_VERSION = "2015-06-23"

TIMELOG_IGNORE_URIS = (
	'^/admin/',         # Ignores all URIs beginning with '/admin/'
	'.jpg$',            # Ignores all URIs ending in .jpg
	'.js$',            # Ignores all URIs ending in .js
	'.css$',            # Ignores all URIs ending in .css
)


PHOTO_ALIASES = {
	'default': {
		'width': '200',
	},
	"post_detail_small": {
		"width": "50",
		"height": "50",
		"crop": "center"
	},
	"post_detail_medium": {
		"height": "350",
		"crop": "center"
	},
	"post_detail_large": {
		"width": "600",
		"height": "400",
		"crop": "center"
	},
	"search_default": {
		"width": "80",
		"height": "80",
	}
}


# THUMBNAIL_KVSTORE = 'sorl.thumbnail.kvstores.redis_kvstore.KVStore'
# THUMBNAIL_FORCE_OVERWRITE = True
# THUMBNAIL_BACKEND = 'thumbnail.backend.AsyncThumbnailBackend'

# IMAGEKIT_DEFAULT_CACHEFILE_STRATEGY = 'imagekit.cachefiles.strategies.Optimistic'
# IMAGEKIT_DEFAULT_CACHEFILE_BACKEND = 'imagekit.cachefiles.backends.Async'

DEFAULT_APP_NAME = "main"

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

import cloudinary
cloudinary.config(
  	cloud_name = "dlnrgtqqv",
  	api_key = "552849736355724",
  	api_secret = "j3lS-0qhDKK-u-ozeJg2D3X9kNI",
  	cdn_subdomain = True,
  	secure = True
)

MAX_IMAGE_WIDTH = 900
MAX_IMAGE_HEIGHT = 600
DEFAULT_IMAGE_WIDTH = 600
DEFAULT_IMAGE_HEIGHT = 400

from pyzipcode import ZipCodeDatabase
zcdb = ZipCodeDatabase()

from django.contrib.gis.geoip import GeoIP
geoip = GeoIP()
