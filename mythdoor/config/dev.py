STAGE = "dev"

DEBUG = True

THUMBNAIL_DEBUG = True

WEBSITE_URL = "http://localhost:8000"

DATABASES = {
   'default': {
       'ENGINE': 'django.db.backends.postgresql_psycopg2',
       'NAME':  "mythdoor",
       'USER':  "mythdooradmin",
       'PASSWORD': "123456",
       'HOST': "localhost",
       'PORT': "5432",
       'CONN_MAX_AGE': 600,
   }
}


########## CACHE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#caches
CACHES = {
	"default": {
		"BACKEND": "django_redis.cache.RedisCache",
		"LOCATION": "redis://127.0.0.1:6379/1",
		"OPTIONS": {
			"CLIENT_CLASS": "django_redis.client.DefaultClient",
			"SOCKET_TIMEOUT": 5,
		}
	}
}
########## END CACHE CONFIGURATION


########## CELERY CONFIGURATION
# See: http://docs.celeryq.org/en/latest/configuration.html#celery-always-eager
#CELERY_ALWAYS_EAGER = True

# See: http://docs.celeryproject.org/en/latest/configuration.html#celery-eager-propagates-exceptions
#CELERY_EAGER_PROPAGATES_EXCEPTIONS = True
########## END CELERY CONFIGURATION


########## TOOLBAR CONFIGURATION
# See: https://github.com/django-debug-toolbar/django-debug-toolbar#installation
INSTALLED_APPS += (
	'debug_toolbar',
	'template_timings_panel',
	"template_profiler_panel",
	"haystack_panel",
	'profiler',
)

# See: https://github.com/django-debug-toolbar/django-debug-toolbar#installation
INTERNAL_IPS = ('127.0.0.1',)

HAYSTACK_CONNECTIONS = {
	'default': {
		'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
		'URL': 'http://127.0.0.1:9200',
		'INDEX_NAME': 'documents',
		'TIMEOUT': 60 * 5,
	},
}

DEBUG_TOOLBAR_CONFIG = {
	'JQUERY_URL':'',
}

DEBUG_TOOLBAR_PANELS = [
	'debug_toolbar.panels.versions.VersionsPanel',
    'debug_toolbar.panels.timer.TimerPanel',
    'debug_toolbar.panels.settings.SettingsPanel',
    'debug_toolbar.panels.headers.HeadersPanel',
    'debug_toolbar.panels.request.RequestPanel',
    'debug_toolbar.panels.sql.SQLPanel',
    'debug_toolbar.panels.staticfiles.StaticFilesPanel',
    'debug_toolbar.panels.templates.TemplatesPanel',
    'debug_toolbar.panels.cache.CachePanel',
    'debug_toolbar.panels.signals.SignalsPanel',
    'debug_toolbar.panels.logging.LoggingPanel',
    'debug_toolbar.panels.redirects.RedirectsPanel',
    'template_timings_panel.panels.TemplateTimings.TemplateTimings',
    "template_profiler_panel.panels.template.TemplateProfilerPanel",
    "haystack_panel.panel.HaystackDebugPanel"
]
########## END TOOLBAR CONFIGURATION

STATIC_URL = '/static/'


MIDDLEWARE_CLASSES = (
  'timelog.middleware.TimeLogMiddleware',
  'debug_toolbar.middleware.DebugToolbarMiddleware',

  'django.middleware.security.SecurityMiddleware',
  'django.middleware.cache.UpdateCacheMiddleware',
  'django.contrib.sessions.middleware.SessionMiddleware',
  'django.middleware.common.CommonMiddleware',

  'django_mobile.middleware.MobileDetectionMiddleware',
  'django_mobile.middleware.SetFlavourMiddleware',
  "django_mobile.cache.middleware.CacheFlavourMiddleware",

  'django.middleware.csrf.CsrfViewMiddleware',
  'django.contrib.auth.middleware.AuthenticationMiddleware',
  'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
  'defender.middleware.FailedLoginMiddleware',
  'django.contrib.messages.middleware.MessageMiddleware',

  'django.middleware.cache.FetchFromCacheMiddleware',
)

BROKER_URL = 'redis://localhost:6379/0'
CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'

DEFAULT_FROM_EMAIL = "teammythdoor@gmail.com"
SENDGRID_API_KEY = "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"

CACHE_MIDDLEWARE_SECONDS = 60*5

CSS_STATIC_URL_PATH_REPLACEMENT = {

}
