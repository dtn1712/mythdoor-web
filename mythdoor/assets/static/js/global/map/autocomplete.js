var autocomplete;

function populateData() {
	var place = autocomplete.getPlace();
	$("input[name='lat']").val(place.geometry.location.lat);
	$("input[name='lng']").val(place.geometry.location.lng);
	$("input[name='location']").val(place.formatted_address);
}

function initAutocomplete(element_id) {
  	autocomplete = new google.maps.places.Autocomplete((document.getElementById(element_id)),{types: ['geocode']});
  	autocomplete.addListener('place_changed', populateData);
}

$(function(){
	initAutocomplete("search_location");
})
