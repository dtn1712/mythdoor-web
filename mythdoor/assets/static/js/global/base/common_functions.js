function listFilter(items,query) {
    var rg = new RegExp(query,'i');
    $(items).each(function(){
        if($.trim($(this).text()).search(rg) == -1) {
        $(this).closest("li").css('display', 'none');
      }
      else {
        $(this).closest('li').css('display', '');
      }
    });
}

function showAlertMessage(message,alert_type,is_time_out) {
    if ($("#alert_message").is(":visible")){
      $("#alert_message").slideUp();
    }
    $("#alert_message").removeClass();
    $("#alert_message").attr("role","alert");
    if (alert_type) {
      $("#alert_message").attr("class","alert alert-" + alert_type)
    } else {
      $("#alert_message").attr("class","alert alert-normal alert-dismissible")
    }
    // if ($("#header").isInViewport() == false) {
    //   $("#alert_message").addClass("affix");
    // }
    $("#alert_message .message-content").html(message);
    $("#alert_message").slideDown();

    if (is_time_out) {
      setTimeout(function() {
        $("#alert_message").slideUp('slow');
      },8000)
    }
}

function hideAlertMessage() {
    $("#alert_message").slideUp();
}


function htmlUtf8Decode(input){
  var e = document.createElement('div');
  e.innerHTML = input;
  return e.childNodes[0].nodeValue;
}

function runTopProgressBar() {
    NProgress.set(Math.random());
    NProgress.start();
    NProgress.inc();
    setTimeout(function(){
      NProgress.done();
    }, 15000);
}

function checkLogin(redirect_link) {
  if (is_login == "False") {
    window.location.href = WEBSITE_URL + "/accounts/login/?next=" + redirect_link;
  }
}

function isTriggerLoadingScroll() {
    if ($(window).scrollTop() == $(document).height() - $(window).height()) {
      return true;
    }
    return false;
}

function clearModalError(modal_el) {
    var error_el = $(modal_el).find(".error");
    for (var i = 0; i < error_el.length; i++) {
        var el = error_el[i];
        $(el).removeClass("error");
    }
}

function getPageType() {
  return $("#page_type").val();
}

function getTemplateType() {
  return $("#template_type").val();
}

function triggerLazyLoadImage(image_class) {
  image_class = (typeof image_class === 'undefined') ? 'img.lazy' : image_class;
  $(image_class).lazyload({
    threshold : 200,
    effect : "fadeIn",
  });
}

function triggerHolderImage(image_class) {
  image_class = (typeof image_class === 'undefined') ? 'holder-image' : image_class;
  var images = document.getElementsByClassName("holder-image");
  Holder.run({
      images: images
  });
}

function getGridLayout() {
  var $grid = $('.grid').isotope({
      itemSelector: '.grid-item',
  });

  return $grid;
}

function layoutGridItem() {
  var $grid = getGridLayout();

  var $lazy_imgs = $("img.lazy");

  $grid.imagesLoaded(function() {
      $grid.isotope('layout');
      $lazy_imgs.load(function(){
        $grid.isotope('layout');
      })
  });
}

// function getCookie(name) {
//     var cookieValue = null;
//     if (document.cookie && document.cookie != '') {
//         var cookies = document.cookie.split(';');
//         for (var i = 0; i < cookies.length; i++) {
//             var cookie = jQuery.trim(cookies[i]);
//             // Does this cookie string begin with the name we want?
//             if (cookie.substring(0, name.length + 1) == (name + '=')) {
//                 cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
//                 break;
//             }
//         }
//     }
//     return cookieValue;
// }
// var csrftoken = getCookie('csrftoken');

// function csrfSafeMethod(method) {
//     // these HTTP methods do not require CSRF protection
//     return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
// }
// function sameOrigin(url) {
//     // test that a given url is a same-origin URL
//     // url could be relative or scheme relative or absolute
//     var host = document.location.host; // host + port
//     var protocol = document.location.protocol;
//     var sr_origin = '//' + host;
//     var origin = protocol + sr_origin;
//     // Allow absolute or scheme relative URLs to same origin
//     return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
//         (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
//         // or any other URL that isn't scheme relative or absolute i.e relative.
//         !(/^(\/\/|http:|https:).*/.test(url));
// }
// $.ajaxSetup({
//     beforeSend: function(xhr, settings) {
//         if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
//             // Send the token to same-origin, relative URLs only.
//             // Send the token only if the method warrants CSRF protection
//             // Using the CSRFToken value acquired earlier
//             xhr.setRequestHeader("X-CSRFToken", csrftoken);
//         }
//     }
// });


