function previewImage(input) {
  	if (input.files && input.files[0]) {
    	var reader = new FileReader();
    	reader.onload = function(e) {
      		$('#image_preview').attr('src', e.target.result);
    	}

    	reader.readAsDataURL(input.files[0]);
  	}
}

function handleDeletePhoto(unique_id) {
	ajaxPost("/ajax/photo/delete_upload_photo",{"unique_id": unique_id},function(content) {
		deleteUploadPhotoCallback(content);
	})
}

function handleUploadPhoto(files) {
	var form_data = false;
	if (window.FormData) {
  		form_data = new FormData();
	}
	var length = files.length;
	if (length == 1) {
		var file = files[0], file_size = file.size, file_type = file.type;
		if (file_size > VALID_FILE_SIZE) {
			showAlertMessage("Your upload picture is larger than 5Mb","danger")
		} else if (!file_type.match(/image.*/)) {
			showAlertMessage("Please upload correct image file","danger")
		} else if ($('#upload_photo_thumbnail_list').children().length >= 9) {
			showAlertMessage("You only allowed to upload 11 photos","danger");
		} else {
			if (form_data) {
				form_data.append("image", file);
				form_data.append("csrfmiddlewaretoken",$("input[name='csrfmiddlewaretoken']").val());
				form_data.append("rotation_angle",$("#upload_photo_rotation_angle").val());
				showAjaxLoadingIcon();
				$.ajax({
					url: "/photo/upload/",
					type: "POST",
					data: form_data,
					processData: false,
					contentType: false,
					dataType: "json",
					success: function (data) {
						hideAjaxLoadingIcon();
						if (data['success']) {
							var thumbnail_list = $('#upload_photo_thumbnail_list'), item;
							if (thumbnail_list.children().length == 0 ) {
								item = 	"<li class='photo main-photo' data-unique-id='" + data['unique_id'] + "'>" +
											"<img src='" + data['secure_url'] + "' width='80' height='80'>" +
											'<span class="delete-photo-btn" onclick="handleDeletePhoto(' + "'" + data['unique_id'] + "'" + ')">X</span>' +
										"</li>"
								$("#id_main_photo").val(data['unique_id']);
							} else {
								item = 	"<li class='photo additional-photo' data-unique-id='" + data['unique_id'] + "'>" +
											"<img src='" + data['secure_url'] + "' width='80' height='80'>" +
											'<span class="delete-photo-btn" onclick="handleDeletePhoto(' + "'" + data['unique_id'] + "'" + ')">X</span>' +
										"</li>"
							}
							thumbnail_list.append(item);
							var photos = $("#id_photos").val() + data['unique_id'] + ";";
							$("#id_photos").val(photos);
						} else {
							showAlertMessage("Failed to upload your picture. Please try again","danger");
						}
					},
					error : function(xhr,errmsg,err) {
						hideAjaxLoadingIcon();
						showAlertMessage("Failed to upload your picture. Please try again","danger");
					}
				});
			}
		}
	}
}


