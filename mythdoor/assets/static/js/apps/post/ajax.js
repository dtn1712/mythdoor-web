function deleteUploadPhotoCallback(data) {
	hideAjaxLoadingIcon();
	if (data['success']) {
		var el = $("#upload_photo_thumbnail_list").find("li.photo[data-unique-id=" + data['unique_id'] + "]");
		$(el).remove();
		var photos = $("#id_photos").val();
		if (photos.indexOf(data['unique_id'] + ";") > -1) {
			$("#id_photos").val(photos.replace(data['unique_id'] + ";",""));
		}
		var main_photo = $("#id_main_photo").val();
		if (main_photo.indexOf(data['unique_id']) > -1 ) {
			$("#id_main_photo").val("");
			var additional_photos = $("#upload_photo_thumbnail_list").find("li.additional-photo");
			if (additional_photos.length > 0) {
				$("#id_main_photo").val($(additional_photos[0]).attr("data-unique-id"));
				$(additional_photos[0]).attr("class","photo main-photo");
			}
		}
	} else {
		showAlertMessage("Failed to delete image. Please try again","danger");
	}
}

function checkCompanyValidCallback(data) {
	hideAjaxLoadingIcon();
	if (data['success']) {
		if (data['is_company_valid']) {
			var contact_by_companies = $("#id_contact_by_companies").val();
			$("#id_contact_by_companies").val(contact_by_companies + data['company_unique_id'] + ":" + data['company_name'] + ";");

			if (countOccurrences($("#id_contact_by_companies").val(),data['company_unique_id']) > 1) {
				$("#id_companies").tagsinput("remove",data['company_name']);
			}
		} else {
			$("#id_companies").tagsinput("remove",data['company_name']);
			showAlertMessage("Your company name is incorrect. Please choose the correct company","danger",true);
		}
	} else {
		showAlertMessage("Your company name is incorrect. Please choose the correct company","danger",true);
	}
}

function checkSchoolValidCallback(data) {
	hideAjaxLoadingIcon();
	if (data['success']) {
		if (data['is_school_valid']) {
			var contact_by_schools = $("#id_contact_by_schools").val();
			$("#id_contact_by_schools").val(contact_by_schools + data['school_unique_id'] + ":" + data['school_name'] + ";");

			if (countOccurrences($("#id_contact_by_schools").val(),data['school_unique_id']) > 1) {
				$("#id_schools").tagsinput("remove",data['school_name']);
			}
		} else {
			$("#id_schools").tagsinput("remove",data['school_name']);
			showAlertMessage("Your school name is incorrect. Please choose the correct school","danger",true);
		}
	} else {
		showAlertMessage("Your school name is incorrect. Please choose the correct school","danger",true);
	}
}

function watchPostCallback(data) {
	hideAjaxLoadingIcon();
	if (data['success']) {
		$("#handle_watch_post_btn").html('<span class="glyphicon glyphicon-ok mini-margin-right" aria-hidden="true"></span>Watching');
		$("#handle_watch_post_btn").attr("data-value","watching");
	} else {
		showAlertMessage("An error occured when processing your request. Please try again","danger",true);
	}
}

function unwatchPostCallback(data) {
	hideAjaxLoadingIcon();
	if (data['success']) {
		$("#handle_watch_post_btn").html('Watch this ad');
		$("#handle_watch_post_btn").attr("data-value","not_watching");
	} else {
		showAlertMessage("An error occured when processing your request. Please try again","danger",true);
	}
}

function createConversationCallback(data) {
	hideAjaxLoadingIcon();
	if (data['success']) {
		window.location.href = "/user/dashboard/message?section=buying&conversation=" + data['conversation_unique_id'];
	} else {
		showAlertMessage("An error occured when processing your request. Please try again","danger",true);
	}
}

function checkPostInterestBuyersCallback(data) {
	hideAjaxLoadingIcon();
	if (data['success']) {
		if (data['is_have_interest_buyers']) {
			$("#complete_post_modal").modal("show");
		} else {
			$("#complete_post_form").submit();
		}
	} else {
		showAlertMessage("An error occured when processing your request. Please try again","danger",true);
	}
}


function getTransactionFeedbackTemplateCallback(data) {
	hideAjaxLoadingIcon();
	if (data['success']) {
		$("#transaction_feedback_section").html(data['template_snippet']);

		var handleTransactionFeedbackInputChange = function(el,rating) {
			var parent = $(el).closest("li");
			var conversation_unique_id = $(parent).attr("data-unique-id");
			var comment = $($(parent).find("textarea")[0]).val();
			var data = {
				'conversation_unique_id': conversation_unique_id,
				"comment": comment,
				"rating": rating
			}
			ajaxPost("/ajax/post/create_transaction_feedback",data,function(content){})
		}

		$(".interest-buyer-rating").raty({
			path: $("#static_url").val() + "css/prod/images",
			click: function(score,evt) {
				handleTransactionFeedbackInputChange(this,score);
			}
		});
		$("#transaction_feedback_section textarea.feedback-comment").change(function() {
			var rating = $($(this.closest('li')).find("input[name='score']")[0]).val();
			if (stripWhitespace(rating).length != 0) {
				handleTransactionFeedbackInputChange(this,parseInt(rating));
			}
		})
	} else {
		$("#transaction_feedback_modal").modal("hide");
		showAlertMessage("An error occured when processing your request. Please try again","danger",true);
	}
}

function reportPostCallback(data) {
	hideAjaxLoadingIcon();
	if (data['success']) {
		showAlertMessage("Thanks for your report. We will investigate on the issue as soon as possible","success");
	} else {
		showAlertMessage("An error occured when processing your request. Please try again","danger",true);
	}
}
