var page_type = getPageType();

var next_parameter = getUrlParameter("next");

$(function() {
	
	if (page_type == 'account_main') {
		if (next_parameter != null) {
			$("form#login_form input[name='next']").val(next_parameter);
		}
	}

	if (page_type == "account_signup_complete") {
		$("input[name='check_company_email']").change(function() {
			$("input[name='is_company_email']").val($("input[name='check_company_email']:checked").val());
			$(".email-type-info-section").addClass("hidden");
			if (parseInt($(this).val())) {
				$("#company_email_type_section").removeClass("hidden");
			} else {
				$("#others_email_type_section").removeClass("hidden");
			}
		})


		$("#signup_complete_form").validate({
			ignore: ":hidden:not('#id_is_company_email')",
			rules: {
				company: {
					required: true,
				},
				is_company_email: {
					required: true
				},
				initial: {
					required:true,
					maxlength:2,
					lettersonly: true
				},
				password1: {
					required:true
				},
				password2: {
					required:true
				}
			}
		});
	}
})
