function loadMorePost(options) {
	var data = {
		"category": $("#selected_category").val(),
		"subcategory": $("#selected_subcategory").val(),
		'start_index': $("#load_post_start_index").val()
	}

	if (options === undefined) {
		options = {};
	}

	ajaxGet('/ajax/post/load_more_post', data, function(content){
        loadPostCallback(content);
    },options)

	showAjaxLoadingIcon();
}

function filterPost(options) {
	var data = {
		"category": $("#selected_category").val(),
		"subcategory": $("#selected_subcategory").val(),
	}

	if (options === undefined) {
		options = {};
	}
	
	ajaxGet('/ajax/post/filter_post', data, function(content){
        filterPostCallback(content);
    },options)

    showAjaxLoadingIcon();
}