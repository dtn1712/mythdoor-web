var options = { 
	onError: function(response) {
		hideAjaxLoadingIcon();
	}
}

$(function() {

	triggerLazyLoadImage("#homepage_carousel img");
	layoutGridItem();
})

$(function() {

	filterPost(options);

	$("#load_more_btn").click(function() {
		loadMorePost(options);
	})
})


$(function() {
	
	var $grid = getGridLayout();

	$("a.category-item-link").click(function() {
		var category_value = $(this).attr("data-value");
		$("#selected_category").val(category_value);
		$("#selected_subcategory").val("all");
		$("a.category-item-link").removeClass("active");
		$("a.subcategory-item-link").removeClass("active");
		$(this).addClass("active");
		if ( $("#" + category_value + "_subcategory_list").is(":visible")) {
			$(".subcategory-list").slideUp("fast",function() {
				$grid.isotope('layout');
			});
		} else {
			$(".subcategory-list").slideUp("fast");
			$("#" + category_value + "_subcategory_list").slideDown("fast",function() {
				$grid.isotope('layout');
			});
			filterPost(options);
		}
		$grid.isotope('layout');
	})

	$("a.subcategory-item-link").click(function() {
		var subcategory_value = $(this).attr("data-value");
		$("#selected_subcategory").val(subcategory_value);
		filterPost(options);
		$("a.category-item-link").removeClass("active");
		$("a.subcategory-item-link").removeClass("active");
		$(this).addClass("active");
	})

})

