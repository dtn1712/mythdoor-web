function loadMorePostCallback(data) {
	hideAjaxLoadingIcon();
	if (data['success']) {
		var $items = $(data['posts_snippet'])
		var $grid = getGridLayout();
		$grid.append($items).isotope('appended', $items );
		$("#load_post_start_index").val(data['start_index'])
		if (data['is_all_items_loaded']) {
			$("#load_more_btn_container").addClass("hidden");
		} else {
			$("#load_more_btn_container").removeClass("hidden");
		};
		triggerLazyLoadImage();
		triggerHolderImage();
		layoutGridItem();
	}
}

function filterPostCallback(data) {
	hideAjaxLoadingIcon();
	if (data['success']) {
		var $items = $(data['posts_snippet'])
		var $grid = getGridLayout();
		if (isExist("#no_post_item_section")) {
			$("#no_post_item_section").remove();
		}
		var grid_items = $(".grid-item");
		for (var i = 1; i < grid_items.length; i++) {
			var grid_item = grid_items[i];
			$grid.isotope( 'remove', grid_item );
		}

		$grid.append($items).isotope( 'appended', $items );
		$("#load_post_start_index").val(data['start_index']);
		if (data['is_all_items_loaded']) {
			$("#load_more_btn_container").addClass("hidden");
		} else {
			$("#load_more_btn_container").removeClass("hidden");
		}

		triggerLazyLoadImage();
		triggerHolderImage();
		layoutGridItem();
	}
}
