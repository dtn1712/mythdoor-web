var section_parameter = getUrlParameter("section");

$(function() {
	if (section_parameter != null) {

		if (page_type == 'member_message') {
			if (section_parameter == "buying") {
				var conversation_unique_id = getUrlParameter("conversation");
				if (conversation_unique_id != null) {
					ajaxGet("/ajax/message/load_conversation_history", {"conversation_unique_id": conversation_unique_id},function(content) {
						loadBuyingConversationHistoryCallback(content);
					})
					showAjaxLoadingIcon();
					$("#selected_buying_conversation_unique_id").val(conversation_unique_id);
					$("#dashboard_buying .conversation-item[data-unique-id='" + conversation_unique_id + "']").addClass("active");
				}
			} else if (section_parameter == "selling") {
				var conversation_unique_id = getUrlParameter("conversation");
				if (conversation_unique_id != null) {
					ajaxGet("/ajax/message/load_conversation_history", {"conversation_unique_id": conversation_unique_id},function(content) {
						loadSellingConversationHistoryCallback(content);
					})
					showAjaxLoadingIcon();
					var $conversation_item = $(".post-conversation-item[data-unique-id='" + conversation_unique_id + "']");
					$conversation_item.addClass("active");
					$conversation_item.closest("div.panel-collapse").collapse();
				}
			}
		}
	}
})

$(function() {
	$(".delete-post-btn").click(function() {
		var post_unique_id = $(this).attr("data-unique-id");
		$('#delete_post_form').attr("action","/post/" + post_unique_id + "/delete");
	})

	$('#delete_post_modal').on('hidden.bs.modal', function (e) {
  		$('#delete_post_form').attr("action","");
	})

})

$(function() {
	addDotToLongTextWithNumChar(".post-title",100);
})

$(function() {
	$("#dashboard_selling .post-conversation-panel").on("shown.bs.collapse",function() {
		var unique_id = $(this).attr("data-unique-id");
		$(".list-group-item[data-unique-id='" + unique_id + "']").addClass("active");
	});

	$("#dashboard_selling .post-conversation-panel").on("hidden.bs.collapse",function() {
		var unique_id = $(this).attr("data-unique-id");
		$(".list-group-item[data-unique-id='" + unique_id + "']").removeClass("active");
	});

	$("#dashboard_selling .post-conversation-item").click(function() {
		$("#dashboard_selling .post-conversation-item").removeClass("active");
		$(this).addClass("active");
		var conversation_unique_id = $(this).attr("data-unique-id");
		ajaxGet("/ajax/message/load_conversation_history", {"conversation_unique_id": conversation_unique_id},function(content) {
			loadSellingConversationHistoryCallback(content);
		})
		showAjaxLoadingIcon();
		$("#selected_selling_conversation_unique_id").val(conversation_unique_id);
	})
})

$(function() {
	$("#dashboard_buying .conversation-item").click(function() {
		$("#dashboard_buying .conversation-item").removeClass("active");
		$(this).addClass("active");
		var conversation_unique_id = $(this).attr("data-unique-id");
		ajaxGet("/ajax/message/load_conversation_history", {"conversation_unique_id": conversation_unique_id},function(content) {
			loadBuyingConversationHistoryCallback(content);
		})
		showAjaxLoadingIcon();
		$("#selected_buying_conversation_unique_id").val(conversation_unique_id);
	})
})

$(function() {
	$("#send_buying_post_message_btn").click(function() {
		var message_content = $("#buying_post_message_input").val();
		var conversation_unique_id = $("#selected_buying_conversation_unique_id").val();
		if (stripWhitespace(message_content).length != 0 ) {
			var data = {
				'message_content': message_content,
				'conversation_unique_id': conversation_unique_id
			}
			ajaxPost("/ajax/message/send_message",data,function(content) {
				sendBuyingPostMessageCallback(content);
			})
			showAjaxLoadingIcon();
			$("#buying_post_message_input").val("");
		}
	});

	$("#send_selling_post_message_btn").click(function() {
		var message_content = $("#selling_post_message_input").val();
		var conversation_unique_id = $("#selected_selling_conversation_unique_id").val();
		if (stripWhitespace(message_content).length != 0 ) {
			var data = {
				'message_content': message_content,
				'conversation_unique_id': conversation_unique_id
			};
			ajaxPost("/ajax/message/send_message",data,function(content) {
				sendSellingPostMessageCallback(content);
			})
			showAjaxLoadingIcon();
			$("#selling_post_message_input").val("");
		}
	});
})

$(function() {
	$("#delete_conversation_final_btn").click(function() {
		var conversation_type = $("#delete_conversation_type").val();
		if (conversation_type == "buying") {
			var data = {
				"conversation_unique_id": $("#selected_buying_conversation_unique_id").val()
			}
			ajaxPost("/ajax/message/delete_conversation_history",data,function(content) {
				deleteBuyingConversationHistoryCallback(content);
			})
			showAjaxLoadingIcon();
		} else if (conversation_type == "selling") {
			var data = {
				"conversation_unique_id": $("#selected_selling_conversation_unique_id").val()
			}
			ajaxPost("/ajax/message/delete_conversation_history",data,function(content) {
				deleteSellingConversationHistoryCallback(content);
			})
			showAjaxLoadingIcon();
		}
	})
})

$(function() {
	$("#clear_all_notification_btn").click(function() {
		var new_notifications_count = $("#new_notifications_count").val();
		if (new_notifications_count > 0 ) {
			ajaxPost("/ajax/notification/clear_all_notifications",function(content){
				clearAllNotificationsCallback(content);
			})
			showAjaxLoadingIcon();
		}
	})
})

$(function() {
	$("#post_manage_list .list-group-item").click(function() {
		if ($(this).hasClass('active') == false) {
			var value = $(this).attr("data-value");
			$(".post-manage-content").addClass("hidden");
			$("#" + value + "_section").removeClass("hidden");
			$("#post_manage_list .list-group-item").removeClass("active");
			$(this).addClass("active");
		}
	})
})

$(function() {
	$("#account_credential_list .list-group-item").click(function() {
		if ($(this).hasClass('active') == false) {
			var value = $(this).attr("data-value");
			$(".account-credential-content").addClass("hidden");
			$("#" + value + "_section").removeClass("hidden");
			$("#account_credential_list .list-group-item").removeClass("active");
			$(this).addClass("active");
		}
	})
})

$(function() {
	$(".message-options .btn").click(function() {
		if ($(this).hasClass('active') == false) {
			var value = $(this).attr("data-value");
			$(".post-conversation-content").addClass("hidden");
			$("#dashboard_" + value).removeClass("hidden");
			$(".message-options .btn").removeClass("active");
			$(this).addClass("active");
		}
	})
})

$(function() {
	$("#dashboard_settings input").change(function() {
		validateSettings();
	})
})

$(function(){
	$("#report_conversation_input").on("change keyup paste",function(){
		var value = $(this).val();
		if (value.trim().length > 0) {
			$("#report_conversation_final_btn").removeAttr("disabled");
		} else {
			$("#report_conversation_final_btn").attr("disabled","disabled");
		}
	})

	$("#report_conversation_final_btn").click(function() {
		var report_content = $("#report_conversation_input").val();
		if (report_content.trim().length > 0) {
			var conversation_unique_id = $("#report_conversation_unique_id").val();
			showAjaxLoadingIcon();
			ajaxPost("/ajax/message/report_conversation",{"conversation_unique_id": conversation_unique_id, "report_content": report_content},function(content){
				reportConversationCallback(content);
			})
		} else {
			$("#report_conversation_final_btn").attr("disabled","disabled");
		}
	})
})
