function validateSettings(){
	var preferred_email = $("#preferred_email_input").val();
	var initial = $("#initial_input").val();

	hideAlertMessage();
	$("#dashboard_settings input.error").removeClass("error");
	$("#dashboard_settings label.error").addClass("hidden");

	is_valid = true;
	if (initial.length == 0) {
		is_valid = false
		$("#initial_empty_error").removeClass("hidden");
		$("#initial_input").addClass("error");
	} else if (initial.length > 2) {
		is_valid = false
		$("#initial_maxlength_error").removeClass("hidden");
		$("#initial_input").addClass("error");
	} else if (preferred_email.length != 0) {
		if (isValidEmail(preferred_email) == false) {
			is_valid = false
			$("#preferred_email_invalid_error").removeClass("hidden");
			$("#preferred_email_input").addClass("error");
		}
	}
	return is_valid;
}

function updateSettingsHandler() {
	var preferred_email = $("#preferred_email_input").val().trim();
	var initial = $("#initial_input").val();

	is_valid = validateSettings();
	if (is_valid) {
		showAjaxLoadingIcon();
		console.log(preferred_email)
		if (preferred_email.length != 0) {
			console.log("yes");
			ajaxGet("/ajax/member/check_exist_email",{"email": preferred_email},function(content){
		    	if (content['success']) {
		    		if (content['is_exist']) {
		    			$("#preferred_email_exist_error").removeClass("hidden");
		    			$("#preferred_email_input").addClass("error");
		    		} else {
		    			ajaxPost("/ajax/member/update_settings",{"preferred_email": preferred_email, "initial":initial}, function(content) {
		    				updateSettingsCallback(content);
		    			})
		    		}
		    	} else {
		    		hideAjaxLoadingIcon();
		    		showAlertMessage("Failed to validate your preferred email. Please try again","danger",true);
		    	}
		    })
		} else {
			ajaxPost("/ajax/member/update_settings",{"initial":initial}, function(content) {
				updateSettingsCallback(content);
			})
		}
		
	}
}

function sendConnectionRequest() {
	var connection_email = $("#connection_email_input").val();
	var connection_relationship = $("#connection_relationship_input").val();
	var connection_request_message = $("#connection_request_message_input").val();
	console.log(connection_request_message);
	$("#dashboard_account_credential .error").addClass("hidden");
	if ( stripWhitespace(connection_email).length == 0 ) {
		$("#connection_email_error_empty").removeClass("hidden");
	} else if (stripWhitespace(connection_relationship).length == 0 ) {
		$("#connection_relationship_error_empty").removeClass("hidden");
	} else {
		if (!isValidEmail(connection_email)) {
			$("#connection_email_error_invalid").removeClass("hidden");
		} else {
			var data = {
				"connection_email": connection_email,
				"connection_relationship": connection_relationship,
				"connection_request_message": connection_request_message,
			}
			ajaxPost("/ajax/member/send_connection_request",data,function(content) {
				sendConnectionRequestCallback(content);
			})
			showAjaxLoadingIcon();
		}
	}
}

function approveConnectionRequest(connection_request_unique_id) {
	ajaxPost("/ajax/member/approve_connection_request",{"connection_request_unique_id": connection_request_unique_id},function(content) {
		approveConnectionRequestActionCallback(content);
	})
	showAjaxLoadingIcon();
}

function rejectConnectionRequest(connection_request_unique_id) {
	ajaxPost("/ajax/member/reject_connection_request",{"connection_request_unique_id": connection_request_unique_id},function(content) {
		rejectConnectionRequestActionCallback(content);
	})
	showAjaxLoadingIcon();
}

function handleUpdateOccupation() {
	var occupation = $("#occupation_input").val();
	if ( stripWhitespace(occupation).length != 0 ) {
		ajaxPost("/ajax/member/update_occupation",{"occupation": occupation},function(content){
			updateOccupationCallback(content);
		})
		showAjaxLoadingIcon();
	}
}

function filterPostByStatus(status) {
	ajaxGet("/ajax/member/get_user_posts_by_status",{"status": status},function(content) {
		getUserPostsByStatusCallback(content);
	})
	showAjaxLoadingIcon();
}

function clearNotification(notification_unique_id) {
	ajaxPost("/ajax/notification/clear_notification",{"notification_unique_id": notification_unique_id},function(content){
		clearNotificationCallback(content);
	});
	showAjaxLoadingIcon();
}
