from django.http import HttpResponse, HttpResponseRedirect, HttpResponseServerError, Http404
from django.template import RequestContext
from django.shortcuts import render_to_response, get_object_or_404, get_list_or_404
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User, AnonymousUser
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic.base import TemplateResponseMixin
from django.views.generic import DetailView
from django.views.generic.edit import FormView

from mythdoor.apps.app_views import AppBaseView
from mythdoor.apps.app_helper import get_user_login_object

import logging

logger = logging.getLogger(__name__)

APP_NAME = "member"

class BaseDashboardView(AppBaseView,DetailView):
	app_name = APP_NAME
	model = User
	queryset = None

	@method_decorator(login_required)
	def dispatch(self, *args, **kwargs):
		return super(BaseDashboardView, self).dispatch(*args, **kwargs)

	def get_object(self, queryset=None):
		return get_user_login_object(self.request)


class PostDashboardView(BaseDashboardView):
	template_name = "post"


class MessageDashboardView(BaseDashboardView):
	template_name = "message"


class AccountCredentialDashboardView(BaseDashboardView):
	template_name = "account_credential"


class SettingsDashboardView(BaseDashboardView):
	template_name = "settings"


class NotificationDashboardView(BaseDashboardView):
	template_name = "notification"

