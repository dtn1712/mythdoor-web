from django.conf.urls import *

from mythdoor.apps.member.views import PostDashboardView, MessageDashboardView
from mythdoor.apps.member.views import AccountCredentialDashboardView, SettingsDashboardView
from mythdoor.apps.member.views import NotificationDashboardView

urlpatterns = [
	url(r"^dashboard/post$", PostDashboardView.as_view(), name='post_dashboard'),
	url(r"^dashboard/message$", MessageDashboardView.as_view(), name='message_dashboard'),
	url(r"^dashboard/account_credential$", AccountCredentialDashboardView.as_view(), name='account_credential_dashboard'),
	url(r"^dashboard/settings$", SettingsDashboardView.as_view(), name='settings_dashboard'),
	url(r"^dashboard/notification$", NotificationDashboardView.as_view(), name='notification_dashboard'),
]
