from django.views.generic import TemplateView

from mythdoor.apps.app_views import AppBaseView

import logging

logger = logging.getLogger(__name__)

APP_NAME = "about"


class BaseAboutView(AppBaseView,TemplateView):
	app_name = APP_NAME


class HelpView(BaseAboutView):
	template_name = "help"

class PrivacyView(BaseAboutView):
	template_name = "privacy"

class TermView(BaseAboutView):
	template_name = "term"

class AboutView(BaseAboutView):
	template_name = "about"

class SafetyView(BaseAboutView):
	template_name = "safety"