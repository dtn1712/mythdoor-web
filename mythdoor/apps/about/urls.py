from django.conf.urls import *

from mythdoor.apps.about.views import HelpView, PrivacyView, SafetyView
from mythdoor.apps.about.views import TermView, AboutView

urlpatterns = [
	url(r"^about$", AboutView.as_view(), name="about"),
	url(r"^help$", HelpView.as_view(), name="help"),
	url(r"^privacy$", PrivacyView.as_view(), name="privacy"),
	url(r"^term$",TermView.as_view(),name='term'),
	url(r"^safety$",SafetyView.as_view(),name='safety')
]
