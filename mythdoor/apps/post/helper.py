from django.db.models import Count
from django.core.cache import cache

from mythdoor.apps.main.constants import CURRENCY_ICON_MAP, ACTIVE_STATUS, DEFAULT_PAGE_SIZE, CACHE_KEY_SPECIAL_SEPARATOR
from mythdoor.apps.main.models import Post
from mythdoor.settings import CACHE_OBJECT_LIST_TIMEOUT

import json, logging

logger = logging.getLogger(__name__)

def get_post_common_info(post):
	try:
		data = {
			"unique_id": post.unique_id,
			"user_create": post.user_create,
			"title": post.title,
			"content": post.content,
			"postal_code": post.location.postal_code,
			"city": post.location.city,
			"state": post.location.state,
			"user_create_display_credential":post.user_create.userprofile.credential.get_display_credential(),
			"display_category": post.get_display_category(),
			"display_subcategory": post.get_display_subcategory(),
			"post_status": post.post_status
		}

		if post.main_photo != None:
			data['main_photo'] = post.main_photo
		else:
			data['main_photo'] = None

		if post.price != None:
			data["price"] = CURRENCY_ICON_MAP[str(post.price.currency)] + str(post.price.amount)
		else:
			data['price'] = None

		return data
	except Exception as e:
		logger.exception(e)
	return None

def get_post_common_info_json(post):
	data = get_post_common_info(post)
	if data == None:
		return json.dumps({})
	else:
		return json.dumps(data)

def get_post_list_common_info(posts):
	results = []
	for post in posts:
		post_data = get_post_common_info(post)
		if post_data != None:
			results.append(post_data)
	return results


def generate_post_list_cache_key(category,subcategory,start_index):
	return "post" + CACHE_KEY_SPECIAL_SEPARATOR + category + CACHE_KEY_SPECIAL_SEPARATOR + subcategory + CACHE_KEY_SPECIAL_SEPARATOR + str(start_index)


def generate_post_list_template_cache_key(category,subcategory,start_index):
	return "post" + CACHE_KEY_SPECIAL_SEPARATOR + category + CACHE_KEY_SPECIAL_SEPARATOR + subcategory + CACHE_KEY_SPECIAL_SEPARATOR + str(start_index) + CACHE_KEY_SPECIAL_SEPARATOR + "template"


def get_filter_post(category,subcategory,start_index):
	posts = []
	key = generate_post_list_cache_key(category,subcategory,start_index)
	results = cache.get(key)
	if results == None:
		if category == "all":
			posts = Post.objects.annotate(null_photo=Count('main_photo')).filter(post_status=ACTIVE_STATUS,is_deleted=False).order_by("-create_time","-null_photo").select_related()[start_index:start_index + DEFAULT_PAGE_SIZE]
		else:
			if subcategory == "all":
				posts = Post.objects.annotate(null_photo=Count('main_photo')).filter(post_status=ACTIVE_STATUS,category=category,is_deleted=False).order_by("-create_time","-null_photo").select_related()[start_index:start_index + DEFAULT_PAGE_SIZE]
			else:
				posts = Post.objects.annotate(null_photo=Count('main_photo')).filter(post_status=ACTIVE_STATUS,category=category,subcategory=subcategory,is_deleted=False).order_by("-create_time","-null_photo").select_related()[start_index:start_index + DEFAULT_PAGE_SIZE]

		results = get_post_list_common_info(posts)
		cache.set(key,results,timeout=CACHE_OBJECT_LIST_TIMEOUT + 30)

	return results




