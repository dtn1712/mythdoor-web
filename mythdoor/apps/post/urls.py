from django.conf.urls import *

from django.views.generic import RedirectView

from mythdoor.apps.post.views import CreatePostView, ListPostView, CompletePostView
from mythdoor.apps.post.views import TransactionFeedbackView, PublishPostView, DeletePostView
from mythdoor.apps.post.views import UpdatePostView, DetailPostView, RepublishExpiredPostView

urlpatterns = [
   url(r"^create$", CreatePostView.as_view(), name="create_post"),
   url(r"^create/$",RedirectView.as_view(url='/post/create',permanent=False)),

   url(r"^list$", ListPostView.as_view(), name="list_post"),
   url(r"^list/$",RedirectView.as_view(url='/post/list',permanent=False)),

   url(r"^complete$", CompletePostView.as_view(), name="complete_post"),
   url(r"^complete/$",RedirectView.as_view(url='/post/complete',permanent=False)),

   url(r"^transaction/feedback$", TransactionFeedbackView.as_view(), name="transaction_feedback"),
   url(r"^transaction/feedback/$",RedirectView.as_view(url='/post/transaction/feedback',permanent=False)),

   url(r"^(?P<post_unique_id>\w+)/repost$", RepublishExpiredPostView.as_view(), name="republish_expired_post"),
   url(r"^(?P<post_unique_id>\w+)/publish$", PublishPostView.as_view(), name="publish_post"),
   url(r"^(?P<post_unique_id>\w+)/delete$", DeletePostView.as_view() ,name="delete_post"),
   url(r"^(?P<post_unique_id>\w+)/edit$", UpdatePostView.as_view() ,name="edit_post"),
   url(r"^(?P<post_unique_id>\w+)$", DetailPostView.as_view(),name="post_detail"),
]
