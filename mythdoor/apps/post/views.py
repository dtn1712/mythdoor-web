from django.http import HttpResponse, HttpResponseRedirect, HttpResponseServerError, Http404
from django.template import RequestContext
from django.shortcuts import render_to_response, get_object_or_404, get_list_or_404
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User, AnonymousUser
from django.utils import timezone
from django.core import serializers
from django.core.mail import send_mail
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.core.urlresolvers import reverse
from django.db.models import Q

from django.views.generic.edit import FormView, UpdateView
from django.views.generic import DetailView, ListView, DeleteView, TemplateView
from django.views.generic import View
from django import db
from django.core.urlresolvers import reverse_lazy

from mythdoor.apps.app_helper import generate_unique_id, get_template_path, is_user_eligible_to_contact
from mythdoor.apps.app_helper import get_user_login_object, is_user_eligible_to_post
from mythdoor.apps.main.models import Post, Conversation
from mythdoor.apps.post.forms import PostCreateForm, PostUpdateForm, PostAutoDetailForm, PostHousingDetailForm
from mythdoor.apps.main.constants import POST_SUBCATEGORY_WITH_BLANK_CHOICE_MAP, ACTIVE_STATUS
from mythdoor.apps.app_settings import GROUP_SPECIFIED_PROFESSIONAL
from mythdoor.apps.app_views import AppBaseView

from moneyed import Money

import json, logging, datetime

logger = logging.getLogger(__name__)

APP_NAME = "post"

class PostBaseView(AppBaseView):
	app_name = APP_NAME

	def is_user_login_create_post(self,post):
		try:
			user_login = get_user_login_object(self.request)
			if post.user_create == user_login:
				return True
		except Exception as e:
			logger.exception(e)
		return False


class DetailPostView(PostBaseView,DetailView):
	template_name = "detail"
	model = Post
	slug_field = 'unique_id'
	slug_url_kwarg = 'post_unique_id'
	obj = None

	def dispatch(self, *args, **kwargs):
		post = self.get_object()
		if post.is_deleted:
			return HttpResponseRedirect("/")

		if post.is_expired():
			self.template_name = "expired"
		else:
			if self.is_user_login_create_post(post) == False:
				if post.post_status == 'sold':
					self.template_name = 'sold'
				elif post.post_status == 'pending':
					return HttpResponseRedirect("/")

		return super(DetailPostView, self).dispatch(*args, **kwargs)

	def get_object(self, queryset=None):
		if self.obj != None: return self.obj

		slug = self.kwargs.get(self.slug_url_kwarg, None)
		try:
			post = Post.objects.select_related('user_create',"main_photo","contact_permission","auto_detail","housing_detail") \
								.prefetch_related("photos","contact_permission__companies") \
								.get(unique_id=slug)
			self.obj = post
			return self.obj
		except Post.DoesNotExist:
			raise Http404

	def get_context_data(self, **kwargs):
		context = super(DetailPostView, self).get_context_data(**kwargs)
		user_login = get_user_login_object(self.request)
		post = self.get_object()
		context['is_user_eligible_to_contact'] = is_user_eligible_to_contact(user_login,post)

		context['is_watching_post'] = False
		if user_login != None:
			context['is_watching_post'] = user_login.userprofile.is_watching_post(post)

		context['is_already_contacted'] = False
		if context['is_user_eligible_to_contact']:
			conversations = Conversation.objects.filter(Q(user1=user_login)|Q(user2=user_login),post=post)
			if len(conversations) != 0:
				conversation = conversations[0]
				if (conversation.user1 == user_login and conversation.user1_messages.count() > 0) or (conversation.user2 == user_login and conversation.user2_messages.count() > 0):
					context['is_already_contacted'] = True
					context['conversation'] = conversation
		return context


class CreatePostView(PostBaseView,FormView):
	template_name = "create"
	form_class = PostCreateForm
	success_url = None

	@method_decorator(login_required)
	def dispatch(self, *args, **kwargs):
		user_login = get_user_login_object(self.request)
		if is_user_eligible_to_post(user_login) == False:
			return HttpResponseRedirect("/user/dashboard/account_credential?action=create_post&result=not_eligible&session_id=" + self.request.session.session_key)
		return super(CreatePostView, self).dispatch(*args, **kwargs)

	def post(self, request, *args, **kwargs):
		post_create_form = PostCreateForm(request.POST)
		if post_create_form.is_valid():
			try:
				data = post_create_form.cleaned_data
				post = post_create_form.save(request)
				if data['category'] == "auto" and data['subcategory'] == "car":
					auto_detail_form = PostAutoDetailForm(request.POST)
					if auto_detail_form.is_valid():
						post_auto_detail = auto_detail_form.save()
						post.auto_detail = post_auto_detail
				if data['category'] == "housing":
					housing_detail_form = PostHousingDetailForm(request.POST)
					if housing_detail_form.is_valid():
						post_housing_detail = housing_detail_form.save()
						post.housing_detail = post_housing_detail

				post.save()
				return HttpResponseRedirect("/post/" + post.unique_id + "?action=create_post&result=success&session_id=" + request.session.session_key)
			except Exception as e:
				logger.exception(e)
				return HttpResponseRedirect("/post/create?action=create_post&result=fail&session_id=" + request.session.session_key)
		else:
			return self.form_invalid(post_create_form)

	def form_invalid(self, form):
		return self.render_to_response(self.get_context_data(form=form,is_form_invalid=True))

	def get_context_data(self, **kwargs):
		context = super(CreatePostView, self).get_context_data(**kwargs)
		context['post_subcategory_map'] = POST_SUBCATEGORY_WITH_BLANK_CHOICE_MAP
		if "auto_detail_form" not in context: context['auto_detail_form'] = PostAutoDetailForm()
		if "housing_detail_form" not in context: context['housing_detail_form'] = PostHousingDetailForm()
		return context


class PublishPostView(PostBaseView,UpdateView):
	model = Post
	slug_field = 'unique_id'
	slug_url_kwarg = 'post_unique_id'

	@method_decorator(login_required)
	def dispatch(self, *args, **kwargs):
		if self.is_user_login_create_post(self.get_object()) == False:
			return HttpResponseRedirect("/")
		return super(PublishPostView, self).dispatch(*args, **kwargs)

	def get(self, request, *args, **kwargs):
		return HttpResponseRedirect("/")

	def post(self, request, *args, **kwargs):
		try:
			post = self.get_object()
			post.post_status = ACTIVE_STATUS
			post.create_time = timezone.now()
			post.save()
			return HttpResponseRedirect("/post/" + kwargs['post_unique_id'] + "?action=publish_post&result=success&session_id=" + request.session.session_key)
		except Exception as e:
			logger.exception(e)
			return HttpResponseRedirect("/post/" + kwargs['post_unique_id'] + "/publish?action=publish_post&result=fail&session_id=" + request.session.session_key)


class ListPostView(PostBaseView,ListView):
	template_name = "list"
	model = Post
	paginate_by = 10


class UpdatePostView(PostBaseView,UpdateView):
	template_name = "update"
	form_class = PostUpdateForm
	model = Post
	slug_field = 'unique_id'
	slug_url_kwarg = 'post_unique_id'

	@method_decorator(login_required)
	def dispatch(self, request, *args, **kwargs):
		if self.is_user_login_create_post(self.get_object()) == False:
			return HttpResponseRedirect("/")
		return super(UpdatePostView, self).dispatch(request,*args, **kwargs)

	def get_success_url(self):
		return "/post/" + self.get_object().unique_id + "?action=edit_post&result=success&session_id=" + self.request.session.session_key

	def get_fail_url(self):
		return "/post/" + self.get_object().unique_id + "?action=edit_post&result=fail&session_id=" + self.request.session.session_key

	def form_valid(self,form):
		try:
			data = form.cleaned_data
			post = form.save(self.request,self.get_object())
			if data['category'] == "auto" and data['subcategory'] == "car":
				auto_detail_form = PostAutoDetailForm(self.request.POST)
				if auto_detail_form.is_valid():
					post_auto_detail = auto_detail_form.save(post.auto_detail)
					post.auto_detail = post_auto_detail
					post.housing_detail = None
			if data['category'] == "housing":
				housing_detail_form = PostHousingDetailForm(self.request.POST)
				if housing_detail_form.is_valid():
					post_housing_detail = housing_detail_form.save(post.housing_detail)
					post.housing_detail = post_housing_detail
					post.auto_detail = None

			post.save()
			return HttpResponseRedirect(self.get_success_url())
		except Exception as e:
			logger.exception(e)
			return HttpResponseRedirect(self.get_fail_url())

	def get_context_data(self, **kwargs):
		context = super(UpdatePostView, self).get_context_data(**kwargs)
		context['post_subcategory_map'] = POST_SUBCATEGORY_WITH_BLANK_CHOICE_MAP
		instance = self.get_object()
		if "auto_detail_form" not in context:
			auto_detail_form = PostAutoDetailForm()
			if instance.category == "auto" and instance.subcategory == "car":
				auto_detail_form = PostAutoDetailForm(instance=instance)
			context['auto_detail_form'] = auto_detail_form

		if "housing_detail_form" not in context:
			housing_detail_form = PostHousingDetailForm()
			if instance.category == "housing":
				housing_detail_form = PostHousingDetailForm(instance=instance)
			context['housing_detail_form'] = housing_detail_form

		if instance.contact_permission.group_permission_type == GROUP_SPECIFIED_PROFESSIONAL:
			contact_by_companies = ""
			for company in instance.contact_permission.companies.all():
				contact_by_companies += company.name + ","
			context['contact_by_companies'] = contact_by_companies[:len(contact_by_companies)-1]

		return context


class DeletePostView(PostBaseView, DeleteView):
	model = Post
	slug_field = 'unique_id'
	slug_url_kwarg = 'post_unique_id'

	@method_decorator(login_required)
	def dispatch(self, *args, **kwargs):
		if self.is_user_login_create_post(self.get_object()) == False:
			return HttpResponseRedirect("/")
		return super(DeletePostView, self).dispatch(*args, **kwargs)

	def get(self, request, *args, **kwargs):
		return HttpResponseRedirect("/")

	def post(self, request, *args, **kwargs):
		try:
			post = self.get_object()
			#post.delete()
			post.is_deleted = True
			post.save()
			return HttpResponseRedirect("/user/dashboard/post?action=delete_post&result=success")
		except Exception as e:
			logger.exception(e)
			if self.get_object() != None:
				return HttpResponseRedirect("/post/" + self.get_object().unique_id + "?action=delete_post&result=fail&session_id=" + request.session.session_key)
			else:
				return HttpResponseRedirect("/user/dashboard/post?action=delete_post&result=fail&session_id=" + request.session.session_key)


class CompletePostView(AppBaseView,FormView):

	@method_decorator(login_required)
	def dispatch(self, *args, **kwargs):
		return super(CompletePostView, self).dispatch(*args, **kwargs)

	def get(self, request, *args, **kwargs):
		return HttpResponseRedirect("/")

	def post(self, request, *args, **kwargs):
		post_unique_id = request.POST['post_unique_id']
		try:
			post = Post.objects.get(unique_id=post_unique_id)
			post.post_status = "sold"
			post.save()
			return HttpResponseRedirect(self.get_success_url())
		except Exception as e:
			logger.exception(e)
			return HttpResponseRedirect("/user/dashboard/post?action=complete_post&result=fail&session_id=" + request.session.session_key)

	def get_success_url(self):
		return "/user/dashboard/post?action=complete_post&result=success&session_id=" + self.request.session.session_key


class RepublishExpiredPostView(PostBaseView,UpdateView):
	model = Post
	slug_field = 'unique_id'
	slug_url_kwarg = 'post_unique_id'

	@method_decorator(login_required)
	def dispatch(self, *args, **kwargs):
		if self.is_user_login_create_post(self.get_object()) == False:
			return HttpResponseRedirect("/")
		return super(RepublishExpiredPostView, self).dispatch(*args, **kwargs)

	def get(self, request, *args, **kwargs):
		return HttpResponseRedirect("/")

	def post(self, request, *args, **kwargs):
		try:
			post = self.get_object()
			post.post_status = ACTIVE_STATUS
			post.last_repost_time = timezone.now()
			post.save()
			return HttpResponseRedirect("/post/" + kwargs['post_unique_id'] + "?action=publish_post&result=success&session_id=" + request.session.session_key)
		except Exception as e:
			logger.exception(e)
			return HttpResponseRedirect("/post/" + kwargs['post_unique_id'] + "/publish?action=publish_post&result=fail&session_id=" + request.session.session_key)


class TransactionFeedbackView(AppBaseView,FormView):

	@method_decorator(login_required)
	def dispatch(self, *args, **kwargs):
		return super(TransactionFeedbackView, self).dispatch(*args, **kwargs)

	def get_success_url(self):
		return "/user/dashboard/post?action=complete_post&result=success&session_id=" + self.request.session.session_key


