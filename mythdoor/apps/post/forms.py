from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User

from mythdoor.apps.main.models import Post, Photo, PostContactPermission, Company, PostLocation
from mythdoor.apps.main.models import PostAutoDetail, PostHousingDetail
from mythdoor.apps.app_settings import GROUP_SPECIFIED_PROFESSIONAL
from mythdoor.apps.main.constants import POST_CATEGORY_WITH_BLANK_CHOICE
from mythdoor.apps.main.constants import YES_NO_WITH_BLANK_CHOICE
from mythdoor.apps.main.constants import ITEM_CONDITION_WITH_BLANK_CHOICE
from mythdoor.apps.main.constants import COMMON_SEPARATOR as SEPARATOR
from mythdoor.apps.app_helper import get_user_login_object, set_null_if_empty, convert_bool_to_binary
from mythdoor.settings import zcdb

from moneyed import Money, USD
from dateutil import parser

import datetime


class PostForm(forms.Form):
	title = forms.CharField(widget=forms.TextInput(attrs={"class": "form-control","required":"required"}))
	content = forms.CharField(widget=forms.Textarea(attrs={"class": "form-control","required":"required"}))
	postal_code = forms.CharField(widget=forms.TextInput(attrs={"class": "form-control","required":"required"}))
	contact_permission = forms.CharField(required=False)
	contact_by_companies = forms.CharField(required=False,widget=forms.HiddenInput())
	category = forms.ChoiceField(choices=POST_CATEGORY_WITH_BLANK_CHOICE,widget=forms.Select(attrs={"class":"form-control","required":"required"}))
	subcategory = forms.CharField(widget=forms.HiddenInput())
	item_condition = forms.ChoiceField(required=False,choices=ITEM_CONDITION_WITH_BLANK_CHOICE,widget=forms.Select(attrs={"class":"form-control"}))
	price = forms.DecimalField(required=False,min_value=0,max_digits=10,decimal_places=2,widget=forms.TextInput(attrs={"class": "form-control"}))
	main_photo = forms.CharField(required=False)
	photos = forms.CharField(required=False)

	def save(self, request, obj=None):
		data = self.cleaned_data
		post_contact_permission = None
		if obj == None:
			post_contact_permission = PostContactPermission.objects.create(group_permission_type=data['contact_permission'])
		else:
			post_contact_permission = obj.contact_permission
			post_contact_permission.group_permission_type = data['contact_permission']

		if post_contact_permission.group_permission_type == GROUP_SPECIFIED_PROFESSIONAL:
			contact_by_companies = data['contact_by_companies']
			companies = Company.objects.filter(unique_id__in=contact_by_companies.replace(":",SEPARATOR).split(SEPARATOR))
			for company in companies:
				post_contact_permission.companies.add(company)

		post_contact_permission.save()
		post = obj if obj != None else Post()
		post.contact_permission = post_contact_permission
		post.title = data['title']
		post.content = data['content']

		post.category = data['category']
		post.subcategory = data['subcategory']
		post.item_condition = data.get("item_condition","")
		post.user_create = get_user_login_object(request)

		postal_code = data['postal_code']
		zipcode = zcdb[postal_code]
		location = PostLocation.objects.create(postal_code=postal_code,city=zipcode.city,state=zipcode.state,latitude=float(zipcode.latitude),longitude=float(zipcode.longitude))
		post.location = location

		try:
			post.price = Money(data.get('price',""),USD)
		except:
			post.price = None

		post.save()

		main_photo_unique_id = None
		if len(data['main_photo']) > 0:
			try:
				main_photo = Photo.objects.get(unique_id=data['main_photo'])
				post.main_photo = main_photo
				main_photo_unique_id = main_photo.unique_id
			except Photo.DoesNotExist:
				pass

		if len(data['photos']) > 0:
			photos = Photo.objects.filter(unique_id__in=data['photos'].split(SEPARATOR)).exclude(unique_id=main_photo_unique_id)
			for photo in photos:
				post.photos.add(photo)

		return post


class PostAutoDetailForm(forms.Form):
	vin = forms.CharField(required=False,widget=forms.TextInput(attrs={"class": "form-control"}))
	make = forms.CharField(required=False,widget=forms.TextInput(attrs={"class": "form-control"}))
	model = forms.CharField(required=False,widget=forms.TextInput(attrs={"class": "form-control"}))
	year = forms.CharField(required=False,widget=forms.TextInput(attrs={"class": "form-control"}))
	mileage = forms.CharField(required=False,widget=forms.TextInput(attrs={"class": "form-control"}))
	color = forms.CharField(required=False,widget=forms.Select(attrs={"class":"form-control"}))
	title_status = forms.CharField(required=False,widget=forms.Select(attrs={"class":"form-control"}))
	body_type = forms.CharField(required=False,widget=forms.Select(attrs={"class":"form-control"}))
	transmission = forms.CharField(required=False,widget=forms.Select(attrs={"class":"form-control"}))
	cylinder = forms.CharField(required=False,widget=forms.Select(attrs={"class":"form-control"}))
	fuel = forms.CharField(required=False,widget=forms.Select(attrs={"class":"form-control"}))

	def __init__(self, *args, **kwargs):
		if "instance" in kwargs:
			instance = kwargs.pop('instance')
			if instance.auto_detail != None:
				initial = {
					"vin": instance.auto_detail.vin,
					"make": instance.auto_detail.make,
					"model": instance.auto_detail.model,
					"year": instance.auto_detail.year,
					"mileage": instance.auto_detail.mileage,
					"color": instance.auto_detail.color,
					"title_status": instance.auto_detail.title_status,
					"body_type": instance.auto_detail.body_type,
					"transmission": instance.auto_detail.transmission,
					"cylinder": instance.auto_detail.cylinder,
					"fuel": instance.auto_detail.fuel
				}
				kwargs['initial'] = initial
		super(PostAutoDetailForm, self).__init__(*args, **kwargs)

	def clean_year(self):
		value = self.cleaned_data["year"]
		if value != None and len(value) != 0:
			result = None
			try:
				result = int(value)
			except:
				raise forms.ValidationError(_("Please enter input as number"))

			if result < 1950 or result > datetime.datetime.now().year:
				raise forms.ValidationError(_("Please enter year in range 1950 to now"))

		return value

	def clean_mileage(self):
		value = self.cleaned_data['mileage']
		if value != None and len(value) != 0:
			result = None
			try:
				result = long(value)
			except:
				raise forms.ValidationError(_("Please enter input as number"))

			if result < 0:
				raise forms.ValidationError(_("Please enter mileage as positive number"))

		return value


	def save(self,obj=None):
		data = self.cleaned_data
		auto_detail = obj if obj != None else PostAutoDetail()
		auto_detail.vin = data.get('vin',"")
		auto_detail.make = data.get('make',"")
		auto_detail.model = data.get('model',"")
		auto_detail.year = data.get('year',"")
		auto_detail.mileage = data.get('mileage',"")
		auto_detail.color = data.get("color","")
		auto_detail.title_status = data.get("title_status","")
		auto_detail.body_type = data.get("body_type","")
		auto_detail.transmission = data.get("transmission","")
		auto_detail.cylinder = data.get("cylinder","")
		auto_detail.fuel = data.get("fuel","")
		auto_detail.save()
		return auto_detail


class PostHousingDetailForm(forms.Form):
	address = forms.CharField(required=False,widget=forms.TextInput(attrs={"class": "form-control"}))
	total_area = forms.IntegerField(required=False,widget=forms.TextInput(attrs={"class": "form-control"}))
	num_bedroom = forms.CharField(required=False,widget=forms.Select(attrs={"class":"form-control"}))
	num_bathroom = forms.CharField(required=False,widget=forms.Select(attrs={"class":"form-control"}))
	is_allow_cat = forms.CharField(required=False,widget=forms.Select(attrs={"class":"form-control"}))
	is_allow_dog = forms.CharField(required=False,widget=forms.Select(attrs={"class":"form-control"}))
	housing_type = forms.CharField(required=False,widget=forms.Select(attrs={"class":"form-control"}))
	available_on = forms.CharField(required=False,widget=forms.TextInput(attrs={"class": "form-control"}))

	def __init__(self, *args, **kwargs):
		if "instance" in kwargs:
			instance = kwargs.pop('instance')
			if instance.housing_detail != None:
				initial = {
					"address": instance.housing_detail.address,
					"total_area": instance.housing_detail.total_area,
					"num_bedroom": instance.housing_detail.num_bedroom,
					"num_bathroom": instance.housing_detail.num_bathroom,
					"is_allow_cat": convert_bool_to_binary(instance.housing_detail.is_allow_cat),
					"is_allow_dog": convert_bool_to_binary(instance.housing_detail.is_allow_dog),
					"housing_type": instance.housing_detail.housing_type,
				}
				if instance.housing_detail.available_on != None:
					initial['available_on'] = instance.housing_detail.available_on.strftime("%m/%d/%Y")
				kwargs['initial'] = initial
		super(PostHousingDetailForm, self).__init__(*args, **kwargs)


	def save(self,obj=None):
		data = self.cleaned_data
		housing_detail = obj if obj != None else PostHousingDetail()
		housing_detail.address = data.get("address","")

		housing_detail.housing_type = data.get("housing_type","")
		housing_detail.num_bedroom = set_null_if_empty(data.get("num_bedroom",""))
		housing_detail.num_bathroom = set_null_if_empty(data.get("num_bathroom",""))

		is_allow_cat = set_null_if_empty(data.get("is_allow_cat",""))
		if is_allow_cat != None:
			try:
				housing_detail.is_allow_cat = bool(int(is_allow_cat))
			except:
				pass
		else:
			housing_detail.is_allow_cat = None

		is_allow_dog = set_null_if_empty(data.get("is_allow_dog",""))
		if is_allow_dog != None:
			try:
				housing_detail.is_allow_dog = bool(int(is_allow_dog))
			except:
				pass
		else:
			housing_detail.is_allow_dog = None

		total_area = set_null_if_empty(data.get("total_area",""))
		if total_area != None:
			try:
				housing_detail.total_area = int(total_area)
			except:
				pass
		else:
			housing_detail.total_area = None

		available_on = set_null_if_empty(data.get("available_on",""))
		if available_on != None:
			try:
				housing_detail.available_on = parser.parse(available_on)
			except:
				pass
		else:
			housing_detail.available_on = None

		housing_detail.save()
		return housing_detail


class PostCreateForm(PostForm):
	def __init__(self, *args, **kwargs):
		super(PostCreateForm, self).__init__(*args, **kwargs)

class PostUpdateForm(PostForm):

	def __init__(self, *args, **kwargs):
		instance = kwargs.pop('instance')
		initial = {
			"title": instance.title,
			"category": instance.category,
			"subcategory": instance.subcategory,
			"postal_code": instance.location.postal_code,
			"content": instance.content,
			"item_condition": instance.item_condition,
			"contact_permission": instance.contact_permission.group_permission_type,
			"main_photo": instance.main_photo,
			"photos": instance.photos,
		}

		if instance.price != None:
			initial['price'] = float(instance.price.amount)

		if instance.contact_permission.group_permission_type == GROUP_SPECIFIED_PROFESSIONAL:
			contact_by_companies = ""
			for company in instance.contact_permission.companies.all():
				contact_by_companies += company.unique_id + ":" + company.name + ";"
			initial['contact_by_companies'] = contact_by_companies

		kwargs['initial'] = initial
		super(PostUpdateForm, self).__init__(*args, **kwargs)

