from django.conf import settings

from mythdoor.settings import SITE_NAME, DEFAULT_TEMPLATE_FILE_EXTENSION

import datetime


LOGIN_REDIRECT_URL = getattr(settings, 'LOGIN_REDIRECT_URL', '/')
USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')

EMAIL_TYPE_COMPANY = "company"
EMAIL_TYPE_COMPANY_PENDING = "company_pending"
EMAIL_TYPE_OTHERS = "others"

CREDENTIAL_PROFESSIONAL = "professional"
CREDENTIAL_PROFESSIONAL_CONNECTION = "professional_connection"
CREDENTIAL_OTHERS = "others"

GROUP_EVERYONE = "everyone"
GROUP_PROFESSIONAL = "professional"
GROUP_SPECIFIED_PROFESSIONAL = "specified_professional"

LOGO_ICON_URL = "https://mythdoor.s3.amazonaws.com/img/ico/favicon.ico"

PUBLIC_CONSUMER_EMAIL_DOMAIN = [
  "yahoo.com",'gmail.com','hotmail.com',"aol.com",
  "comcast.net","msn.com","live.com","sbcglobal.net"
  "sbc.global.net","outlook.com","verizon.net"]

SITE_NAME_INITIAL = SITE_NAME[0].upper()

DEFAULT_LATITUDE = 47.616000
DEFAULT_LONGITUDE = -122.322464
DEFAULT_CITY = "Seattle"
DEFAULT_COUNTRY = "United States"
DEFAULT_COUNTRY_CODE = "US"
DEFAULT_TIMEZONE = "America/Los_Angeles"
DEFAULT_POSTAL_CODE = 98122
DEFAULT_DISTANCE_RANGE = 50

SECONDS_PER_DAY = 86400
SECONDS_PER_HOUR = 3600
SECONDS_PER_MINUTE = 60

YES_NO_BOOLEAN_MAP = {
  True: "Yes",
  False: "No"
}

SITE_DATA = {
  "SITE_NAME" : SITE_NAME,
  "SITE_NAME_INITIAL": SITE_NAME_INITIAL,
  "SITE_DESCRIPTION": SITE_NAME[0].upper() + SITE_NAME[1:].lower() + " is a web application that allow local people to buy and sell safely with other professionals",
  "SITE_KEYWORDS": "mythdoor, buy sell local, marketplace",
  "DEFAULT_TIMEZONE": DEFAULT_TIMEZONE,
  "DEFAULT_DISTANCE_RANGE": DEFAULT_DISTANCE_RANGE,
  "CREDENTIAL_PROFESSIONAL" : CREDENTIAL_PROFESSIONAL,
  "CREDENTIAL_PROFESSIONAL_CONNECTION" : CREDENTIAL_PROFESSIONAL_CONNECTION,
  "CREDENTIAL_OTHERS" : CREDENTIAL_OTHERS,
  "EMAIL_TYPE_COMPANY_PENDING": EMAIL_TYPE_COMPANY_PENDING,
  "EMAIL_TYPE_COMPANY": EMAIL_TYPE_COMPANY,
  "EMAIL_TYPE_OTHERS": EMAIL_TYPE_OTHERS,
  "TEMPLATE_FILE_EXTENSION": DEFAULT_TEMPLATE_FILE_EXTENSION
}

KEYWORDS_URL = [
	'admin','signup','login','password',"accounts"
	'logout','confirm_email','search','settings',
	'buzz','messages',"about",'api','asset','photo',
	'feeds','friends'
]

MESSAGE_SNIPPET_TEMPLATE =  {
  # Auth app
  "signup_success": "messages/apps/account/signup_success.html",
  "confirm_email_success": "messages/apps/account/confirm_email_success.html",
  "confirm_email_asking": "messages/apps/account/confirm_email_asking.html",
  "resend_confirm_email_success": "messages/apps/account/resend_confirm_email_success.html",
  "resend_confirm_email_error": "messages/apps/account/resend_confirm_email_error.html",
  "change_password_success": "messages/apps/account/change_password_success.html",
  "social_login_error": "messages/apps/account/social_login_error.html",
  "password_reset_success": "messages/apps/account/password_reset_success.html",
  "create_post_success": "messages/apps/post/create_post_success.html",
  "create_post_fail": "messages/apps/post/create_post_fail.html",
  "create_post_not_eligible": "messages/apps/post/create_post_not_eligible.html",
  "edit_post_success": "messages/apps/post/edit_post_success.html",
  "edit_post_fail": "messages/apps/post/edit_post_fail.html",
  "publish_post_success": "messages/apps/post/publish_post_success.html",
  "publish_post_fail": "messages/apps/post/publish_post_fail.html",
  "delete_post_success": "messages/apps/post/delete_post_success.html",
  "delete_post_fail": "messages/apps/post/delete_post_fail.html",
  "complete_post_success": "messages/apps/post/complete_post_success.html",
  "complete_post_fail": "messages/apps/post/complete_post_fail.html",
}

HTML_SNIPPET_TEMPLATE = {
  "post_auto_detail_form": "sites/apps/post/snippet/auto_detail_form.jinja.html",
  "post_housing_detail_form": "sites/apps/post/snippet/housing_detail_form.jinja.html",
  "post_list": "sites/apps/post/snippet/post_list.jinja.html",
  "empty_post_list": "sites/apps/post/snippet/empty_post_list.jinja.html",
  "user_settings": "sites/apps/member/snippet/user_settings.jinja.html",
  "professional_account_info": "sites/apps/member/snippet/professional_account_info.jinja.html",
  "professional_connection_requests": "sites/apps/member/snippet/professional_connection_requests.jinja.html",
  "user_posts": "sites/apps/member/snippet/user_posts.jinja.html",
  "message_item": "sites/apps/member/snippet/message_item.jinja.html",
  "conversation_history": "sites/apps/member/snippet/conversation_history.jinja.html",
  "transaction_feedback_template": "sites/apps/post/snippet/transaction_feedback_template.jinja.html"
}


EMAIL_SUBJECT_SNIPPET_TEMPLATE = {
  "signup_request_activation": "emails/apps/account/signup_request_activation_subject.txt",
  'signup_request_incorrect_email': "emails/apps/account/signup_request_incorrect_email_subject.txt",
  "send_connection_request": "emails/apps/member/send_connection_request_subject.txt",
  "connection_request_approved": "emails/apps/member/connection_request_approved_subject.txt",
  "password_reset": "emails/apps/account/password_reset_subject.txt",
  "new_message": "emails/apps/message/new_message_subject.txt"
}

EMAIL_CONTENT_HTML_SNIPPET_TEMPLATE = {
  "signup_request_activation": "emails/apps/account/signup_request_activation_content.html",
  'signup_request_incorrect_email': "emails/apps/account/signup_request_incorrect_email_content.html",
  "send_connection_request": "emails/apps/member/send_connection_request_content.html",
  "connection_request_approved": "emails/apps/member/connection_request_approved_content.html",
  "password_reset": "emails/apps/account/password_reset_content.html",
  "new_message": "emails/apps/message/new_message_content.html"
}

EMAIL_CONTENT_TEXT_SNIPPET_TEMPLATE = {
  "signup_request_activation": "emails/apps/account/signup_request_activation_content.txt",
  'signup_request_incorrect_email': "emails/apps/account/signup_request_incorrect_email_content.txt",
  "send_connection_request": "emails/apps/member/send_connection_request_content.txt",
  "connection_request_approved": "emails/apps/member/connection_request_approved_content.txt",
  "password_reset": "emails/apps/account/password_reset_content.txt",
  "new_message": "emails/apps/message/new_message_content.txt"
}

NOTIFICATION_SNIPPET_TEMPLATE = {
  "new_connection_request": "sites/apps/notification/snippet/new_connection_request.txt",
  "approve_connection_request": "sites/apps/notification/snippet/approve_connection_request.txt",
  "new_message": "sites/apps/notification/snippet/new_message.txt"
}




