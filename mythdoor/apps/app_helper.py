from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.gis.geoip import GeoIP
from django.db.models import Q
from django.core import serializers
from django.core.cache import cache
from django.shortcuts import render_to_response, get_object_or_404
from django.template.loader import render_to_string
from django.template import RequestContext, engines
from django.contrib.gis.measure import D

from django.conf import settings as django_settings
from django.utils.html import strip_tags

from mythdoor.apps.app_settings import MESSAGE_SNIPPET_TEMPLATE, YES_NO_BOOLEAN_MAP
# from mythdoor.apps.app_settings import HTML_SNIPPET_TEMPLATE_RESPONSIVE, HTML_SNIPPET_TEMPLATE_NON_RESPONSIVE
from mythdoor.apps.app_settings import HTML_SNIPPET_TEMPLATE
from mythdoor.apps.app_settings import GROUP_EVERYONE, GROUP_PROFESSIONAL, GROUP_SPECIFIED_PROFESSIONAL
from mythdoor.apps.app_settings import CREDENTIAL_OTHERS, CREDENTIAL_PROFESSIONAL, CREDENTIAL_PROFESSIONAL_CONNECTION
from mythdoor.apps.app_settings import SECONDS_PER_DAY, SECONDS_PER_HOUR, SECONDS_PER_MINUTE
from mythdoor.apps.app_settings import DEFAULT_POSTAL_CODE

from mythdoor.settings import SITE_NAME, SECRET_KEY, OW_LY_API_KEY, ROOT_PATH, DEFAULT_TEMPLATE_FILE_EXTENSION
from mythdoor.settings import geoip

from firebase_token_generator import create_token

from sys import path

import collections, datetime, decimal, json, os, logging
import shortuuid, random, math, pycountry, pytz, requests

logger = logging.getLogger(__name__)

CATALOGUE = "CATALOGUE"
COMMENT_CHARACTER = "<!--"

SECONDS_PER_DAY = 86400
SECONDS_PER_HOUR = 3600
SECONDS_PER_MINUTE = 60

def read_catalogue(list_file,path,catalogue_type):
	catalogue_path = path
	catalogue_file = CATALOGUE
	if catalogue_type != None:
		catalogue_file = catalogue_type[0].upper() + "_" + CATALOGUE
	if catalogue_file not in path:
		catalogue_path = path + catalogue_file if path[len(path)-1] == "/" else path + "/" + catalogue_file
	f = open(catalogue_path,"r")
	for filename in f:
		if len(filename.replace("\n","")) != 0 and filename.startswith(COMMENT_CHARACTER) == False:
			list_file.append(filename.replace("\n","").strip())


# This function to get current time-zone of client
def get_current_time_zone(request):
	g = GeoIP()
	client_ip = get_client_ip(request)
	data = g.city(client_ip)
	db = GeoTimeZoneReader()
	timezone = "America/New_York"
	try:
		timezone = db.get_timezone(data['country_code'], data['region'])
	except Exception as e:
		logger.exception(e)
	return timezone


# This function to get current time based on the time_zone
def get_current_time_at_client_time_zone(request):
	time_zone = get_current_time_zone(request)
	return datetime.datetime.now(pytz.timezone(time_zone))

def get_request_geodata(request):
	client_ip = get_client_ip(request)
	return geoip.city(client_ip)

def get_current_postal_code(request):
	client_ip = get_client_ip(request)
	current_postal_code = DEFAULT_POSTAL_CODE
	if geoip.city(client_ip) != None:
		current_postal_code = geoip.city(client_ip)['postal_code']
	return current_postal_code

def convert_list_to_json(data):
	d = []
	for i in range(0,len(data)):
		d.append(data[i]['fields'])
	return json.dumps(d)


def data_backup():
	path.append(ROOT_PATH)
	try:
		cmd = "python manage.py dumpdata > " + SITE_NAME + "/db/fixtures/"
		os.system(cmd)
	except Exception as e:
		logger.exception(e)


def make_two_numbers(original):
	if len(original)==1:
		return "0"+original
	else:
		return original


def capitalize_first_letter(s):
	if s == None: return
	if len(s) == 0: return
	return s[0].upper() + s[1:].lower()


def json_encode_decimal(obj):
	if isinstance(obj, decimal.Decimal):
		return str(obj)
	raise TypeError(repr(obj) + " is not JSON serializable")


def convert_24hr_to_ampm(time):
	hour = time.hour
	minute = time.minute
	if int(hour) >= 12:
		final_hour = int(hour) + 1 - 12
		return str("%02d" % final_hour) + ":" + str("%02d" % minute) + "pm"
	else:
		return str("%02d" % hour) + ":" + str("%02d" % minute) + "am"


def convert_ampm_to_24hr(time,ampm):
	hour = time.hour
	result = ""
	if ampm.lower() == 'am':
		if int(hour) == 12:
			result = "00:"
		else:
			result = make_two_numbers(hour) + ":"
	if ampm.lower() == 'pm':
		if int(hour) == 12:
			result = "12:"
		else:
			result = str(12 + hour) + ":"
	return result + str(time.minute) + " " + ampm


def get_elapse_time_text(value):
   # Get total elapse seconds
   elapse_time = datetime.datetime.utcnow() - datetime.datetime(value.year,value.month,value.day,value.hour,value.minute,value.second)
   total_seconds = elapse_time.total_seconds()

   days = int(total_seconds / SECONDS_PER_DAY)
   hours = int((total_seconds % SECONDS_PER_DAY) / SECONDS_PER_HOUR)
   minutes = int(((total_seconds % SECONDS_PER_DAY) % SECONDS_PER_HOUR) / SECONDS_PER_MINUTE)

   if days == 0 and hours == 0 and minutes == 0:
      return "Just now"

   min_value, hour_value, day_value = ' minute', ' hour', ' day'
   if minutes != 1: min_value += "s"
   if hours != 1: hour_value += "s"
   if days != 1: day_value += "s"

   if days == 0 and hours == 0:
   	return str(minutes) + min_value + " ago"
   if days == 0:
   	return str(hours) + hour_value + " ago"
   return str(days) + day_value + " ago"


def is_empty(s):
   if s == None: return True
   if len(s.strip()) == 0: return True
   return False


def get_duplicate_object(l):
	l2 = collections.Counter(l)
	return [i for i in l2 if l2[i]>1]


def remove_duplicate_object(l):
	return list(set(l))


def set_fixed_string(s,s_len):
	if s_len >= len(s):
		return s
	return s[:s_len] + '...'


# Get current login user object
def get_user_login_object(request):
	if hasattr(request,"user") == False: return None
	try:
		user_login = request.user
		if not user_login.is_anonymous():
			return user_login
	except:
		pass
	return None


# Get the current ip from client to see their zip code and
# return appropriate location. Currently, this is not work with localhost
def get_client_ip(request):
	x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
	if x_forwarded_for:
		ip = x_forwarded_for.split(',')[-1].strip()
	else:
		ip = request.META.get('REMOTE_ADDR')
	return ip


# Setup the constant month tuple using for the form
def setup_constant_month():
	month_value = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
	l = []
	for i in range(1,13):
		l.append([i,month_value[i-1]])
	return tuple(tuple(x) for x in l)


# Setup the constant day tuple
def setup_constant_day():
	l = []
	for i in range(1,32):
		l.append([i,i])
	return tuple(tuple(x) for x in l)


# Setup the constant year tuple
def setup_constant_year(start_year=1920,is_string=False):
	l = []
	current_year = datetime.datetime.now().year
	for i in range(start_year,int(current_year) - 13):
		if is_string:
			l.append([str(i),str(i)])
		else:
			l.append([i,i])
	return tuple(tuple(x) for x in l)


# Setup the constant countries in alpha2 code
def setup_constant_countries_alpha2():
	output = []
	for country in list(pycountry.countries):
		data = (str(country.alpha2),country.name.encode("utf-8"))
		output.append(data)
	return tuple(output)


# Setup the constant countries in alpha3 code
def setup_constant_countries_alpha3():
	output = []
	for country in list(pycountry.countries):
		data = (str(country.alpha3),country.name.encode("utf-8"))
		output.append(data)
	return tuple(output)


def generate_html_snippet(request,snippet,data,template_engine='jinja'):
	# is_responsive = False
	# if "flavour" in request and request['flavour'] != None and request['flavour'] != "full":
	# 	is_responsive = True

	# template_name = HTML_SNIPPET_TEMPLATE_NON_RESPONSIVE[snippet] if not is_responsive else HTML_SNIPPET_TEMPLATE_RESPONSIVE[snippet]
	
	template_name = HTML_SNIPPET_TEMPLATE[snippet]
	if template_engine == "jinja":
		jinja_engine = engines['jinja']
		template = jinja_engine.get_template(template_name)
		return template.render(context=data,request=request)
	else:
		return render_to_string(template_name,data,context_instance=RequestContext(request))


def generate_message(action,result,data):
	try:
		template_name = action
		if result != None:
			template_name = action + "_" + result
		message = render_to_string(MESSAGE_SNIPPET_TEMPLATE[template_name],data)
		return message
	except Exception as e:
		logger.exception(e)
	return None


def handle_request_get_message(request,data):
	if "action" in request.GET and "result" in request.GET:
		if ("session_id" in request.GET and request.GET["session_id"] == request.session.session_key) or \
			("show" in request.GET and request.GET['show'] == "1"):	
			return generate_message(request.GET['action'],request.GET['result'],data)
	return None

def get_any_admin_object():
	admins = User.objects.filter(is_superuser=True)
	if len(admins) == 0:
		return None
	else:
		return admins[0]


def generate_token(custom_data,options):
	return create_token(SECRET_KEY,custom_data,options)


def get_short_url(request):
	long_url = str(request.build_absolute_uri())
	try:
		r = requests.get("http://ow.ly/api/1.1/url/shorten?apiKey=" + OW_LY_API_KEY + "&longUrl=" + long_url)
		data = r.json()
		return data['results']['shortUrl']
	except:
		pass
	return long_url


def get_different_hours_from_timezones(first,second):
	try:
		if (TIMEZONE_TO_UTC[str(second)] is not None ) and (TIMEZONE_TO_UTC[str(first)] is not None):
			return TIMEZONE_TO_UTC[str(second)]-TIMEZONE_TO_UTC[str(first)]
	except:
		pass
	return 0


def generate_unique_id():
	return shortuuid.uuid()[:11] + shortuuid.uuid()[:11] + str("%03d" % random.randint(1,999))


def is_str_unique_id(s):
	if len(s) != 27 or s[24:27].isdigit() == False or s[0:2].upper() not in MODEL_KEY_LIST:
		return False
	return True


# def get_base_template_path(flavour,app_name=None):
# 	prefix = "non_responsive"
# 	if flavour != None:
# 		prefix = "non_responsive" if flavour == "full" else "responsive"

# 	if app_name == None:
# 		return "sites/" + prefix + "/base" + DEFAULT_TEMPLATE_FILE_EXTENSION
#  	else:
#  		return "sites/" + prefix + "/apps/" + app_name + "/" + app_name + "_base" + DEFAULT_TEMPLATE_FILE_EXTENSION

# def get_template_path(app_name,template_name,flavour,sub_path='/page/'):
# 	prefix = "non_responsive"
# 	if flavour != None:
# 		prefix = "non_responsive" if flavour == "full" else "responsive"
# 	return "sites/" + prefix + "/apps/" + app_name + sub_path + template_name + DEFAULT_TEMPLATE_FILE_EXTENSION


def get_base_template_path(app_name=None):
	if app_name == None:
		return "sites/base" + DEFAULT_TEMPLATE_FILE_EXTENSION
 	else:
 		return "sites/apps/" + app_name + "/" + app_name + "_base" + DEFAULT_TEMPLATE_FILE_EXTENSION

def get_template_path(app_name,template_name,sub_path='/page/'):
	return "sites/apps/" + app_name + sub_path + template_name + DEFAULT_TEMPLATE_FILE_EXTENSION


def check_key_not_blank(data,key):
	if key in data and data[key] != None and len(data[key]) != 0:
		return True
	return False


def set_null_if_empty(s):
	try:
		if len(s.strip()) > 0:
			return s
	except:
		pass
	return None


def is_user_eligible_to_post(user):
	is_eligible_user = True
	credential = user.userprofile.credential.get_credential()
	if credential == CREDENTIAL_OTHERS or credential == None or len(credential) == 0:
		is_eligible_user = False
	return is_eligible_user


def is_professional_and_can_contact(group,post,credential):
	credential_value = credential.get_credential()
	group_condition = group == GROUP_PROFESSIONAL
	credential_condition = credential_value == CREDENTIAL_PROFESSIONAL or credential_value == CREDENTIAL_PROFESSIONAL_CONNECTION
	if group_condition and credential_condition: return True

	if group == GROUP_SPECIFIED_PROFESSIONAL:

		if credential_value == CREDENTIAL_PROFESSIONAL:
			company_count = post.contact_permission.companies.filter(unique_id=credntial.company.unique_id).count()
			if company_count == 1: return True

		if credential_value == CREDENTIAL_PROFESSIONAL_CONNECTION:
			if credential.connection != None and credential.connection.status == APPROVE_STATUS:
				connection_credential = credential.connection.approved_user.userprofile.credential
				if connection_credential.get_credential() == CREDENTIAL_PROFESSIONAL:
					company_count = post.contact_permission.companies.filter(unique_id=connection_credential.company.unique_id).count()
					if company_count == 1: return True

	return False


def is_user_eligible_to_contact(user,post):
	group = post.contact_permission.group_permission_type

	if group == GROUP_EVERYONE: return True
	if user == None: return False
	if post.user_create == user: return True

	credential = user.userprofile.credential

	if is_professional_and_can_contact(group,post,credential): return True

	return False


def get_model_value_fields(model,fields):
	result = {}
	for field in fields:
		value = getattr(model,field)
		key = fields[field][0]
		key_type = fields[field][1]
		if key_type == "char":
			if len(value) != 0: result[key] = value
		elif key_type == "int":
			if value != None: result[key] = value
		elif key_type == "boolean":
			if value != None: result[key] = YES_NO_BOOLEAN_MAP[bool(value)]
		elif key_type == "date":
			if value != None: result[key] = value
	return result


def convert_bool_to_binary(value):
	if value == None: return None
	if value == True: return "1"
	return "0"


