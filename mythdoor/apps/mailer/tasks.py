from celery import shared_task

from mythdoor.apps.mailer.service import MailerService

import logging

logger = logging.getLogger(__name__)

@shared_task
def send_email(unique_id):
	try:
		mailer_service = MailerService()
		mailer_service.send_email(unique_id)
		logger.debug("Send email successfully")
	except Exception as e:
		logger.exception(e)


@shared_task
def send_mass_emails(list_email_unique_ids):
	try:
		mailer_service = MailerService()
		mailer_service.send_mass_emails(list_email_unique_ids)
		logger.debug("Send email successfully")
	except Exception as e:
		logger.exception(e)
