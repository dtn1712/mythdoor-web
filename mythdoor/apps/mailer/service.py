from django.core.mail import get_connection, EmailMultiAlternatives
from django.core.mail.backends.smtp import EmailBackend as DjangoSmtpEmailBackend
from django.template.loader import render_to_string

from mythdoor.apps.main.models import Email
from mythdoor.apps.app_settings import EMAIL_SUBJECT_SNIPPET_TEMPLATE
from mythdoor.apps.app_settings import EMAIL_CONTENT_TEXT_SNIPPET_TEMPLATE
from mythdoor.apps.app_settings import EMAIL_CONTENT_HTML_SNIPPET_TEMPLATE
from mythdoor.settings import SITE_NAME, DEFAULT_FROM_EMAIL

import ast, datetime, logging

logger = logging.getLogger(__name__)

class MailerService():

	def generate_email(self,data):
		context = ast.literal_eval(data.context_data)
		from_email = SITE_NAME[0].upper() + SITE_NAME[1:].lower() + " <" + DEFAULT_FROM_EMAIL + ">"
		subject = render_to_string(EMAIL_SUBJECT_SNIPPET_TEMPLATE[data.email_template],context)
		text_content = render_to_string(EMAIL_CONTENT_TEXT_SNIPPET_TEMPLATE[data.email_template],context)
		html_content = render_to_string(EMAIL_CONTENT_HTML_SNIPPET_TEMPLATE[data.email_template],context)
		email = EmailMultiAlternatives(subject=subject,body=text_content,from_email=from_email,to=data.to_emails.split(","))
		email.attach_alternative(html_content, "text/html")
		email.content_subtype = "html"
		data.subject = subject
		data.text_content = text_content
		data.save()
		return email

	def send_email(self,unique_id):
		try:
			data = Email.objects.get(unique_id=unique_id)
			email = self.generate_email(data)
			email.send()
			data.status = "success"
			data.send_time = datetime.datetime.now()
			data.save()
		except Exception as e:
			logger.exception(e)
			data.status = "fail"
			data.save()
			raise

	def send_mass_emails(self,list_email_unique_ids):
		emails = Email.objects.filter(unique_id__in=list_email_unique_ids)
		list_send_emails = []
		for email in emails:
			list_send_emails.append(email.generate_email())
		try:
			connection = get_connection()
			connection.open()
			connection.send_messages(list_send_emails)
			connection.close()
			Email.objects.filter(unique_id__in=list_email_unique_ids).update(status='success',send_time=datetime.datetime.now())
		except Exception as e:
			logger.exception(e)
			Email.objects.filter(unique_id__in=list_email_unique_ids).update(status='fail')
