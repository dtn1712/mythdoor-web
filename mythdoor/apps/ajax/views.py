from django.db.models import Count, Q
from django.contrib.gis.measure import D
from django.contrib.auth.models import User
from django.core.cache import cache
from django.template.loader import render_to_string
from django.contrib.auth.decorators import login_required

from django_ajax.decorators import ajax

from mythdoor.apps.ajax.helper import get_request_params, validate_request
from mythdoor.apps.message.tasks import clear_message_status
from mythdoor.apps.main.models import Post, Conversation, TransactionFeedback, Photo, Company, ConversationReport
from mythdoor.apps.main.models import UserConnectionRequest, Email, Notification, Message, PostReport
from mythdoor.apps.post.helper import get_post_common_info, get_filter_post, get_post_list_common_info
from mythdoor.apps.post.helper import generate_post_list_template_cache_key
from mythdoor.apps.app_helper import generate_html_snippet, get_user_login_object
from mythdoor.apps.member.helper import get_user_common_info
from mythdoor.apps.main.constants import ACTIVE_STATUS, DEFAULT_PAGE_SIZE, OLD, EMPTY_STRING
from mythdoor.apps.main.constants import CREDENTIAL_PROFESSIONAL, APPROVED, CREDENTIAL_PROFESSIONAL_CONNECTION
from mythdoor.apps.main.constants import REJECTED, CREDENTIAL_OTHERS, POST_STATUS_MAP, NEW
from mythdoor.apps.main.constants import CACHE_KEY_SPECIAL_SEPARATOR
from mythdoor.apps.app_settings import NOTIFICATION_SNIPPET_TEMPLATE
from mythdoor.apps.mailer.tasks import send_email
from mythdoor.settings import WEBSITE_URL, CACHE_OBJECT_LIST_TIMEOUT

from validate_email import validate_email

import logging

logger = logging.getLogger(__name__)

@ajax
def load_more_post(request):
	result = {}
	try:
		validate_request(request,['GET'])
		data = get_request_params(request,['category','subcategory','start_index'])
		category = data['category']
		subcategory = data['subcategory']
		start_index = data['start_index']
		posts = get_filter_post(category,subcategory,int(start_index))
		snippet_key = generate_post_list_template_cache_key(category,subcategory,start_index)
		posts_snippet_cache = cache.get(snippet_key)
		if posts_snippet_cache != None:
			result['posts_snippet'] = posts_snippet_cache
		else:
			if len(posts) == 0:
				result['posts_snippet'] = generate_html_snippet(request,"empty_post_list",{})
			else:
				result['posts_snippet'] = generate_html_snippet(request,"post_list",{"posts":posts})
			cache.set(snippet_key,result['posts_snippet'],timeout=CACHE_OBJECT_LIST_TIMEOUT + 30)
		result['start_index'] = int(start_index) + len(posts)
		result['is_all_items_loaded'] = False
		if len(posts) < DEFAULT_PAGE_SIZE:
			result['is_all_items_loaded'] = True
		result['success'] = True
	except Exception as e:
		logger.exception(e)
		result['success'] = False
		result['error_message'] = str(e)
	return result


@ajax
def filter_post(request):
	result = {}
	try:
		validate_request(request,['GET'])
		data = get_request_params(request,['category','subcategory'])
		category = data['category']
		subcategory = data['subcategory']
		posts = get_filter_post(category,subcategory,0)
		snippet_key = generate_post_list_template_cache_key(category,subcategory,0)
		posts_snippet_cache = cache.get(snippet_key)
		if posts_snippet_cache != None:
			result['posts_snippet'] = posts_snippet_cache
		else:
			if len(posts) == 0:
				result['posts_snippet'] = generate_html_snippet(request,"empty_post_list",{})
			else:
				result['posts_snippet'] = generate_html_snippet(request,"post_list",{"posts":posts})
			cache.set(snippet_key,result['posts_snippet'],timeout=CACHE_OBJECT_LIST_TIMEOUT + 30)
		result['start_index'] = DEFAULT_PAGE_SIZE
		result['is_all_items_loaded'] = False
		if len(posts) < DEFAULT_PAGE_SIZE:
			result['is_all_items_loaded'] = True
		result['success'] = True
	except Exception as e:
		logger.exception(e)
		result['success'] = False
		result['error_message'] = str(e)
	return result

@ajax
@login_required
def watch_post(request):
	result = {}
	try:
		validate_request(request,['POST'])
		data = get_request_params(request,['post_unique_id'])
		post_unique_id = data['post_unique_id']
		user_login = get_user_login_object(request)
		post = Post.objects.get(unique_id=post_unique_id)
		user_login.userprofile.watching_posts.add(post)
		user_login.userprofile.save()
		result['success'] = True
	except Exception as e:
		logger.exception(e)
		result['success'] = False
		result['error_message'] = str(e)
	return result


@ajax
@login_required
def unwatch_post(request):
	result = {}
	try:
		validate_request(request,['POST'])
		data = get_request_params(request,['post_unique_id'])
		post_unique_id = data['post_unique_id']
		user_login = get_user_login_object(request)
		post = Post.objects.get(unique_id=post_unique_id)
		user_login.userprofile.watching_posts.remove(post)
		user_login.userprofile.save()
		result['post_unique_id'] = post_unique_id
		result['success'] = True
	except Exception as e:
		logger.exception(e)
		result['success'] = False
		result['error_message'] = str(e)
	return result


@ajax
@login_required
def report_post(request):
	result = {}
	try:
		validate_request(request,['POST'])
		data = get_request_params(request,['post_unique_id','report_content'])
		user_login = get_user_login_object(request)
		post = Post.objects.get(unique_id=data['post_unique_id'])
		PostReport.objects.create(report_post=post,content=data['report_content'],user_create=user_login)
		result['success'] = True
	except Exception as e:
		logger.exception(e)
		result['success'] = False
		result['error_message'] = str(e)
	return result


@ajax
@login_required
def check_post_interest_buyers(request):
	result = {}
	try:
		validate_request(request,['POST'])
		data = get_request_params(request,['post_unique_id'])
		post_unique_id = data['post_unique_id']
		user_login = get_user_login_object(request)
		post = Post.objects.get(unique_id=post_unique_id)
		conversations = Conversation.objects.filter(Q(user1=user_login)|Q(user2=user_login),post=post)
		if len(conversations) == 0:
			result['is_have_interest_buyers'] = False
		else:
			result['is_have_interest_buyers'] = True
		result['success'] = True
	except Exception as e:
		logger.exception(e)
		result['success'] = False
		result['error_message'] = str(e)
	return result


@ajax
@login_required
def get_transaction_feedback_template(request):
	result = {}
	try:
		validate_request(request,['GET'])
		data = get_request_params(request,['post_unique_id'])
		post_unique_id = data['post_unique_id']
		user_login = get_user_login_object(request)
		post = Post.objects.get(unique_id=post_unique_id)
		conversations = Conversation.objects.filter(Q(user1=user_login)|Q(user2=user_login),post=post)
		result['template_snippet'] = generate_html_snippet(request,"transaction_feedback_template",{"conversations":conversations})
		result['post_unique_id'] = post_unique_id
		result['success'] = True
	except Exception as e:
		logger.exception(e)
		result['success'] = False
		result['error_message'] = str(e)
	return result


@ajax
@login_required
def create_transaction_feedback(request):
	result = {}
	try:
		validate_request(request,['POST'])
		data = get_request_params(request,['conversation_unique_id','rating','comment'])
		rating = data['rating']
		if rating:
			user_login = get_user_login_object(request)
			conversation = Conversation.objects.get(unique_id=data['conversation_unique_id'])
			interest_buyer = conversation.user1
			if user_login == conversation.user1:
				interest_buyer = conversation.user2
			TransactionFeedback.objects.filter(post=conversation.post,interest_buyer=interest_buyer).delete()
			TransactionFeedback.objects.create(post=conversation.post,interest_buyer=interest_buyer,rating=int(rating),comment=data['comment'])
		result['success'] = True
	except Exception as e:
		logger.exception(e)
		result['success'] = False
		result['error_message'] = str(e)
	return result


@ajax
@login_required
def delete_upload_photo(request):
	result = {}
	try:
		validate_request(request,['POST'])
		data = get_request_params(request,['unique_id'])
		photo = Photo.objects.get(unique_id=data['unique_id'])
		photo.delete()
		result['success'] = True
		result['unique_id'] = data['unique_id']
	except Exception as e:
		logger.exception(e)
		result['success'] = False
		result['error_message'] = str(e)
	return result

@ajax
@login_required
def clear_notification(request):
	result = {}
	try:
		validate_request(request,['POST'])
		data = get_request_params(request,['notification_unique_id'])
		notification = Notification.objects.get(unique_id=data['notification_unique_id'])
		notification.status = OLD
		notification.save()
		result['notification_unique_id'] = data['notification_unique_id']
		result['related_link'] = notification.related_link
		result['success'] = True
	except Exception as e:
		logger.exception(e)
		result['success'] = False
		result['error_message'] = str(e)
	return result


@ajax
@login_required
def clear_all_notifications(request):
	result = {}
	try:
		validate_request(request,['POST'])
		user_login = get_user_login_object(request)
		Notification.objects.filter(notify_to=user_login,status=NEW).update(status=OLD)
		result['success'] = True
	except Exception as e:
		logger.exception(e)
		result['success'] = False
		result['error_message'] = str(e)
	return result


@ajax
@login_required
def update_settings(request):
	result = {}
	try:
		validate_request(request,['POST'])
		data = get_request_params(request,["initial"],['preferred_email'])
		initial = data['initial'].upper()
		preferred_email = EMPTY_STRING
		if "preferred_email" in data: 
			preferred_email = data['preferred_email']
		
		user_login = get_user_login_object(request)
		user_login.userprofile.initial = initial
		if validate_email(preferred_email) or len(preferred_email) == 0:
			user_login.userprofile.preferred_email = preferred_email
			result['settings_snippet'] = generate_html_snippet(request,"user_settings",{"user": user_login, "is_preferred_email_valid": True})
			result['success'] = True
		else:
			result['settings_snippet'] = generate_html_snippet(request,"user_settings",{"user": user_login, "is_preferred_email_valid": False})
			result['success'] = False
		user_login.userprofile.save()
	except Exception as e:
		logger.exception(e)
		result['success'] = False
		result['error_message'] = str(e)
	return result


@ajax
@login_required
def check_exist_email(request):
	result = {}
	try:
		validate_request(request,['GET'])
		data = get_request_params(request,['email'])
		email = data['email']
		users = User.objects.filter(Q(email=email)|Q(userprofile__preferred_email=email))
		if len(users) == 0:
			result['is_exist'] = False
		else:
			user = users[0]
			user_login = get_user_login_object(request)
			if user_login == user:
				result['is_exist'] = False
			else:
				result['is_exist'] = True
		result['success'] = True
	except Exception as e:
		logger.exception(e)
		result['success'] = False
		result['error_message'] = str(e)
	return result


@ajax
@login_required
def send_connection_request(request):
	result = {}
	try:
		validate_request(request,['POST'])
		data = get_request_params(request,['connection_email','connection_relationship'],['connection_request_message'])
		connection_email = data['connection_email']
		connection_relationship = data['connection_relationship']
		connection_request_message = data['connection_request_message']

		user_login = get_user_login_object(request)
		is_approve_user_valid = False
		approve_user = None
		try:
			approve_user = User.objects.get(email=connection_email)
			if approve_user.userprofile.credential.get_credential() == CREDENTIAL_PROFESSIONAL:
				is_approve_user_valid = True
		except User.DoesNotExist:
			pass

		if is_approve_user_valid:
			# Delete all old pending request, override with the new one
			UserConnectionRequest.objects.filter(request_user=user_login,approve_user=approve_user,status='pending').delete()
			UserConnectionRequest.objects.create(request_user=user_login,connection_relationship=connection_relationship,
						approve_user=approve_user,connection_type="professional",message=connection_request_message)

			context = {
				"request_user_email": user_login.email,
				"connection_relationship": connection_relationship,
				"message": connection_request_message,
				"view_connection_url": WEBSITE_URL + '/user/dashboard/account_credential?section=connection_requests',
			}
			email = Email.objects.create(to_emails=approve_user.userprofile.get_sending_email(),email_template="send_connection_request",context_data=str(context))
			send_email.delay(email.unique_id)

			Notification.objects.create(content=render_to_string(NOTIFICATION_SNIPPET_TEMPLATE['new_connection_request'],context),
				notification_type="connection_request",notify_from=user_login,notify_to=approve_user,related_link=context['view_connection_url'])

		result['is_approve_user_valid'] = is_approve_user_valid
		result['success'] = True
	except Exception as e:
		logger.exception(e)
		result['success'] = False
		result['error_message'] = str(e)
	return result


@ajax
@login_required
def update_occupation(request):
	result = {}
	try:
		validate_request(request,['POST'])
		data = get_request_params(request,['occupation'])
		occupation = data['occupation']
		user_login = get_user_login_object(request)
		user_login.userprofile.credential.credential_additional_info = occupation
		user_login.userprofile.credential.save()
		result['account_info_snippet'] = generate_html_snippet(request,"professional_account_info",{"user": user_login})
		result['success'] = True
	except Exception as e:
		logger.exception(e)
		result['success'] = False
		result['error_message'] = str(e)
	return result


@ajax
@login_required
def approve_connection_request(request):
	result = {}
	try:
		validate_request(request,['POST'])
		data = get_request_params(request,['connection_request_unique_id'])
		connection_request_unique_id = data['connection_request_unique_id']

		user_login = get_user_login_object(request)

		connection_request = UserConnectionRequest.objects.get(unique_id=connection_request_unique_id)
		connection_request.status = APPROVED
		connection_request.save()

		credential = connection_request.request_user.userprofile.credential
		credential.credential_type = CREDENTIAL_PROFESSIONAL_CONNECTION
		credential.company = user_login.userprofile.credential.company
		credential.connection = connection_request
		credential.save()

		UserConnectionRequest.objects.filter(request_user=connection_request.request_user,approve_user=user_login,status="pending").delete()

		context = {
			"approve_user_email": user_login.email,
			"connection_relationship": connection_request.connection_relationship,
			"url": WEBSITE_URL + '/user/dashboard/account_credential?section=connection_requests',
		}
		email = Email.objects.create(to_emails=connection_request.request_user.userprofile.get_sending_email(),email_template="connection_request_approved",context_data=str(context))
		send_email.delay(email.unique_id)

		Notification.objects.create(content=render_to_string(NOTIFICATION_SNIPPET_TEMPLATE['approve_connection_request'],context),
			notification_type="connection_request",notify_from=user_login,notify_to=connection_request.request_user,related_link=context['url'])

		result['connection_requests_snippet'] = generate_html_snippet(request,"professional_connection_requests",{"user": user_login})
		result['success'] = True
		print result
	except Exception as e:
		logger.exception(e)
		result['success'] = False
		result['error_message'] = str(e)
	return result


@ajax
@login_required
def reject_connection_request(request):
	result = {}
	try:
		validate_request(request,['POST'])
		data = get_request_params(request,['connection_request_unique_id'])
		connection_request_unique_id = data['connection_request_unique_id']

		user_login = get_user_login_object(request)

		connection_request = UserConnectionRequest.objects.get(unique_id=connection_request_unique_id)
		connection_request.status = REJECTED
		connection_request.save()

		credential = connection_request.request_user.userprofile.credential
		credential.credential_type = CREDENTIAL_OTHERS
		credential.company = None
		credential.connection = None
		credential.save()

		UserConnectionRequest.objects.filter(request_user=connection_request.request_user,approve_user=user_login,status="pending").delete()

		result['connection_requests_snippet'] = generate_html_snippet(request,"professional_connection_requests",{"user": user_login})
		result['success'] = True
	except Exception as e:
		logger.exception(e)
		result['success'] = False
		result['error_message'] = str(e)
	return result


@ajax
@login_required
def get_user_posts_by_status(request):
	result = {}
	try:
		validate_request(request,['GET'])
		data = get_request_params(request,['status'])
		status = data['status']
		user_login = get_user_login_object(request)

		posts_key = "user" + CACHE_KEY_SPECIAL_SEPARATOR + user_login.username + CACHE_KEY_SPECIAL_SEPARATOR + "post" + CACHE_KEY_SPECIAL_SEPARATOR + status 
		posts = cache.get(posts_key)
		if posts == None:
			posts = get_post_list_common_info(Post.objects.filter(user_create=user_login,post_status=status,is_deleted=False).order_by("-create_time"))
			cache.set(posts_key,posts,timeout=CACHE_OBJECT_LIST_TIMEOUT + 30)

		snippet_key = "user" + CACHE_KEY_SPECIAL_SEPARATOR + user_login.username + CACHE_KEY_SPECIAL_SEPARATOR + "post" + CACHE_KEY_SPECIAL_SEPARATOR + status + CACHE_KEY_SPECIAL_SEPARATOR + "template"
		snippet = cache.get(snippet_key)
		if snippet == None:
			result['user_posts_snippet'] = generate_html_snippet(request,"user_posts",{"posts": posts,"status": status})
			cache.set(snippet_key,result['user_posts_snippet'],timeout=CACHE_OBJECT_LIST_TIMEOUT + 30)
		else:
			result['user_posts_snippet'] = snippet

		result['post_status'] = POST_STATUS_MAP[status]
		result['success'] = True
	except Exception as e:
		logger.exception(e)
		result['success'] = False
		result['error_message'] = str(e)
	return result


@ajax
@login_required
def check_company_valid(request):
	result = {}
	try:
		validate_request(request,['GET'])
		data = get_request_params(request,['company_name'])
		company_name = data['company_name']
		is_company_valid = False
		companies = Company.objects.filter(name__iexact=company_name)
		if len(companies) > 0:
			is_company_valid = True
			result['company_unique_id'] = companies[0].unique_id
		result['success'] = True
		result['is_company_valid'] = is_company_valid
		result['company_name'] = company_name
	except Exception as e:
		logger.exception(e)
		result['success'] = False
		result['error_message'] = str(e)
	return result


@ajax
@login_required
def send_message(request):
	result = {}
	try:
		validate_request(request,['POST'])
		data = get_request_params(request,['conversation_unique_id','message_content'])
		conversation_unique_id = data['conversation_unique_id']
		message_content = data['message_content']
		user_login = get_user_login_object(request)
		conversation = Conversation.objects.get(unique_id=conversation_unique_id)
		user_chat = conversation.user1
		if conversation.user2.username != user_login.username:
			user_chat = conversation.user2
		new_message = Message.objects.create(user_send=user_login,user_receive=user_chat,content=message_content,status=NEW)
		conversation.user1_messages.add(new_message)
		conversation.user2_messages.add(new_message)
		conversation.latest_message = new_message
		conversation.save()

		related_link = None
		if conversation.post.user_create == user_login:
			related_link = WEBSITE_URL + "/user/dashboard/message?section=buying&conversation=" + conversation.unique_id
		else:
			related_link = WEBSITE_URL + "/user/dashboard/message?section=selling&conversation=" + conversation.unique_id

		context = {
			"item": conversation.post.title,
			"user_send_credential_info": user_login.userprofile.credential.get_display_credential(),
			"message_content": message_content,
			"url": related_link
		}

		email = Email.objects.create(to_emails=user_chat.userprofile.get_sending_email(),email_template="new_message",context_data=str(context))
		send_email.delay(email.unique_id)

		notification_context = {
			"from_user_credential": user_login.userprofile.credential.get_display_credential(),
			"from_user_initial": user_login.userprofile.initial.upper(),
			"post_title": conversation.post.title
		}

		Notification.objects.create(content=render_to_string(NOTIFICATION_SNIPPET_TEMPLATE['new_message'],notification_context),
				notification_type="new_message",notify_from=user_login,notify_to=user_chat,related_link=related_link)

		result['success'] = True
		result['message_snippet'] = generate_html_snippet(request,"message_item",{"message": new_message})
		result['conversation_unique_id'] = conversation_unique_id
	except Exception as e:
		logger.exception(e)
		result['success'] = False
		result['error_message'] = str(e)
	return result


@ajax
@login_required
def load_conversation_history(request):
	result = {}
	try:
		validate_request(request,['GET'])
		data = get_request_params(request,['conversation_unique_id'])
		conversation_unique_id = data['conversation_unique_id']
		user_login = get_user_login_object(request)
		conversation = Conversation.objects.get(unique_id=conversation_unique_id)

		clear_message_status.delay(conversation,user_login)

		snippet_key = "conversation" + CACHE_KEY_SPECIAL_SEPARATOR + conversation.unique_id + CACHE_KEY_SPECIAL_SEPARATOR + "user_login" + CACHE_KEY_SPECIAL_SEPARATOR
		if conversation.user1 == user_login:
			snippet_key = snippet_key + conversation.user1.username
		else:
			snippet_key = snippet_key + conversation.user2.username
		
		conversation_snippet = cache.get(snippet_key)
		if conversation_snippet == None:
			result['conversation_snippet'] = generate_html_snippet(request,"conversation_history",{"conversation": conversation,"user": user_login})
			cache.set(snippet_key,result['conversation_snippet'],timeout=CACHE_OBJECT_LIST_TIMEOUT+30)
		else:
			result['conversation_snippet'] = conversation_snippet
		result['conversation_post'] = get_post_common_info(conversation.post)
		result['success'] = True
	except Exception as e:
		logger.exception(e)
		result['success'] = False
		result['error_message'] = str(e)
	return result


@ajax
@login_required
def delete_conversation_history(request):
	result = {}
	try:
		validate_request(request,['POST'])
		data = get_request_params(request,['conversation_unique_id'])
		conversation_unique_id = data['conversation_unique_id']
		user_login = get_user_login_object(request)
		conversation = Conversation.objects.get(unique_id=conversation_unique_id)
		if conversation.user1 == user_login:
			conversation.user1_messages.clear()
		elif conversation.user2 == user_login:
			conversation.user2_messages.clear()
		conversation.save()
		result['conversation_unique_id'] = conversation_unique_id
		result['post_unique_id'] = conversation.post.unique_id
		result['success'] = True
	except Exception as e:
		logger.exception(e)
		result['success'] = False
		result['error_message'] = str(e)
	return result


@ajax
@login_required
def create_conversation(request):
	result = {}
	try:
		validate_request(request,['POST'])
		data = get_request_params(request,['post_unique_id','user_chat_username','message_content'])
		post_unique_id = data['post_unique_id']
		user_chat_username = data['user_chat_username']
		message_content = data['message_content']

		user_login = get_user_login_object(request)
		user_chat = User.objects.get(username=user_chat_username)
		message = Message.objects.create(user_send=user_login,user_receive=user_chat,content=message_content,status=NEW)
		post = Post.objects.get(unique_id=post_unique_id)

		conversation = None
		conversations = Conversation.objects.filter(Q(user1=user_login,user2=user_chat)|Q(user1=user_chat,user2=user_login),post=post)
		if len(conversations) == 0:
			conversation = Conversation.objects.create(user1=user_login,user2=user_chat,post=post)
		else:
			conversation = conversations[0]

		conversation.user1_messages.add(message)
		conversation.user2_messages.add(message)
		conversation.latest_message = message
		conversation.save()

		context = {
			"item": post.title,
			"user_send_credential_info": user_login.userprofile.credential.get_display_credential(),
			"message_content": message_content,
			"url": WEBSITE_URL + "/user/dashboard/message?section=selling&conversation=" + str(conversation.unique_id)
		}

		email = Email.objects.create(to_emails=user_chat.userprofile.get_sending_email(),email_template="new_message",context_data=str(context))
		send_email.delay(email.unique_id)

		notification_context = {
			"from_user_credential": user_login.userprofile.credential.get_display_credential(),
			"from_user_initial": user_login.userprofile.initial,
			"post_title": conversation.post.title
		}
		Notification.objects.create(content=render_to_string(NOTIFICATION_SNIPPET_TEMPLATE['new_message'],notification_context),
				notification_type="new_message",notify_from=user_login,notify_to=user_chat,related_link=context['url'])

		result['conversation_unique_id'] = conversation.unique_id
		result['success'] = True
	except Exception as e:
		logger.exception(e)
		result['success'] = False
		result['error_message'] = str(e)
	return result


@ajax
@login_required
def report_conversation(request):
	result = {}
	try:
		validate_request(request,['POST'])
		data = get_request_params(request,['conversation_unique_id','report_content'])
		user_login = get_user_login_object(request)
		conversation = Conversation.objects.get(unique_id=data['conversation_unique_id'])
		ConversationReport.objects.create(conversation_report=conversation,content=data['report_content'],user_create=user_login)
		result['success'] = True
	except Exception as e:
		logger.exception(e)
		result['success'] = False
		result['error_message'] = str(e)
	return result

