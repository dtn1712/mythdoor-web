from django.conf.urls import *

from django.views.generic import RedirectView

from mythdoor.apps.ajax import views

urlpatterns = [

	# Post
   	url(r"^post/load_more_post$", views.load_more_post, name="load_more_post"),
   	url(r"^post/filter_post$", views.filter_post, name="filter_post"),
   	url(r"^post/watch_post$",views.watch_post,name="watch_post"),
   	url(r"^post/unwatch_post$",views.unwatch_post,name="unwatch_post"),
      url(r"^post/report_post$", views.report_post,name="report_post"),
   	url(r"^post/check_post_interest_buyers$", views.check_post_interest_buyers,name="check_post_interest_buyers"),
   	url(r"^post/get_transaction_feedback_template$", views.get_transaction_feedback_template,name="get_transaction_feedback_template"),
   	url(r"^post/create_transaction_feedback$", views.create_transaction_feedback,name="create_transaction_feedback"),

   	# Photo
   	url(r"^photo/delete_upload_photo$",views.delete_upload_photo,name="delete_upload_photo"),

   	# Notification
   	url(r"^notification/clear_notification$",views.clear_notification,name="clear_notification"),
   	url(r"^notification/clear_all_notifications$",views.clear_all_notifications,name="clear_all_notifications"),

   	# Member
   	url(r"^member/update_settings$",views.update_settings,name="update_settings"),
   	url(r"^member/send_connection_request$",views.send_connection_request,name="send_connection_request"),
   	url(r"^member/approve_connection_request$",views.approve_connection_request,name="approve_connection_request"),
   	url(r"^member/reject_connection_request$",views.reject_connection_request,name="reject_connection_request"),
   	url(r"^member/get_user_posts_by_status$",views.get_user_posts_by_status,name="get_user_posts_by_status"),
      url(r"^member/update_occupation$",views.update_occupation,name='update_occupation'),
   	url(r"^member/check_exist_email$",views.check_exist_email,name='check_exist_email'),

      # Company
   	url(r"^company/check_company_valid$",views.check_company_valid,name="check_company_valid"),

      # Message
      url(r"^message/send_message$",views.send_message,name="send_message"),
      url(r"^message/load_conversation_history$",views.load_conversation_history,name="load_conversation_history"),
      url(r"^message/delete_conversation_history$",views.delete_conversation_history,name="delete_conversation_history"),
      url(r"^message/create_conversation$",views.create_conversation,name="create_conversation"),
      url(r"^message/report_conversation$", views.report_conversation,name="report_conversation"),
]
