from django.db import models
from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin
from tastypie.admin import ApiKeyInline

from mythdoor.apps.main.models import *


class UserModelAdmin(UserAdmin):
    inlines = UserAdmin.inlines + [ApiKeyInline]

class PostAdmin(admin.ModelAdmin):
	list_display = ['unique_id','title','price','post_status',"is_deleted","user_create"]

class PostReportAdmin(admin.ModelAdmin):
	list_display = ['unique_id','report_post','content','user_create']

class ConversationReportAdmin(admin.ModelAdmin):
	list_display = ['unique_id','conversation_report','content','user_create']

class PostContactPermissionAdmin(admin.ModelAdmin):
	pass

class PostAutoDetailAdmin(admin.ModelAdmin):
	pass

class PostHousingDetailAdmin(admin.ModelAdmin):
	pass

class PostLocationAdmin(admin.ModelAdmin):
	list_display = ['unique_id','postal_code',"latitude","longitude","city","state"]

class EmailAdmin(admin.ModelAdmin):
    list_display = ['unique_id','to_emails',"email_template","text_content","context_data","status","send_time"]


class UserProfileAdmin(admin.ModelAdmin):
    list_display = ['user']


class SignupRequestAdmin(admin.ModelAdmin):
	list_display = ["unique_id","email","confirmation_key","signup_email_type","request_status","suggest_company"]


class PhotoAdmin(admin.ModelAdmin):
    list_display = ['unique_id','name','width','height','image_url','image_secure_url','upload_time','user_create']


class CompanyAdmin(admin.ModelAdmin):
	list_display = ['unique_id','name',"email_domain","is_verified"]


class UserCredentialAdmin(admin.ModelAdmin):
	list_display = ['unique_id','credential_type',"credential_additional_info","company","connection"]


class UserConnectionRequestAdmin(admin.ModelAdmin):
	list_display = ['unique_id',"request_user","approve_user","status","connection_type","message","connection_relationship","request_time"]


class ConversationAdmin(admin.ModelAdmin):
	list_display = ['unique_id',"user1","user2","post","latest_message"]


class MessageAdmin(admin.ModelAdmin):
	list_display = ['unique_id','user_send',"user_receive","content","status","create_time"]


class NotificationAdmin(admin.ModelAdmin):
	list_display = ['unique_id',"notification_type","status","content","notify_from","notify_to","related_link","create_time"]


class TransactionFeedbackAdmin(admin.ModelAdmin):
	list_display = ['unique_id',"post","interest_buyer","rating","comment"]


admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Email,EmailAdmin)
admin.site.register(Photo, PhotoAdmin)
admin.site.register(SignupRequest,SignupRequestAdmin)
admin.site.register(Company,CompanyAdmin)
admin.site.register(Post,PostAdmin)
admin.site.register(PostContactPermission,PostContactPermissionAdmin)
admin.site.register(UserCredential,UserCredentialAdmin)
admin.site.register(PostAutoDetail,PostAutoDetailAdmin)
admin.site.register(PostHousingDetail,PostHousingDetailAdmin)
admin.site.register(UserConnectionRequest,UserConnectionRequestAdmin)
admin.site.register(Conversation,ConversationAdmin)
admin.site.register(Message, MessageAdmin)
admin.site.register(Notification,NotificationAdmin)
admin.site.register(TransactionFeedback,TransactionFeedbackAdmin)
admin.site.register(PostLocation,PostLocationAdmin)
admin.site.register(PostReport,PostReportAdmin)
admin.site.register(ConversationReport,ConversationReportAdmin)
