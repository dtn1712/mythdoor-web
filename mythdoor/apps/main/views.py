from django.views.generic import ListView, TemplateView
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseServerError
from django.template import RequestContext, engines
from django.shortcuts import render_to_response, get_object_or_404, get_list_or_404
from django.core import serializers
from django.views.generic import DetailView, ListView
from django.db.models import Count

from mythdoor.apps.app_helper import get_template_path
from mythdoor.apps.app_views import AppBaseView
from mythdoor.apps.main.models import Post
from mythdoor.apps.main.constants import ACTIVE_STATUS, POST_SUBCATEGORY_MAP
from mythdoor.apps.main.constants import DEFAULT_PAGE_SIZE, POST_CATEGORY_INFO_MAP
from mythdoor.apps.post.helper import get_filter_post

import logging, django_mobile

logger = logging.getLogger(__name__)

APP_NAME = "main"


class MainView(AppBaseView,ListView):
	app_name = APP_NAME
	template_name = "index"

	def get_queryset(self):
		return []

	def get_context_data(self, **kwargs):
		context = super(MainView, self).get_context_data(**kwargs)
		context['category_info_map'] = POST_CATEGORY_INFO_MAP
		context['subcategory_map'] = POST_SUBCATEGORY_MAP
		context['load_post_start_index'] = 0
		# context['load_post_start_index'] = DEFAULT_PAGE_SIZE
		return context


def handler404(request):
	data = {}
	jinja_engine = engines['jinja']
	template = jinja_engine.get_template('sites/404.jinja.html')
	return HttpResponse(template.render(context=data,request=request))


def handler500(request):
	data = {}
	jinja_engine = engines['jinja']
	template = jinja_engine.get_template('sites/500.jinja.html')
	return HttpResponse(template.render(context=data,request=request))

