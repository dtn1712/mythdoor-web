from django.contrib.auth.models import User
from django.db.models.signals import post_save, post_delete, pre_delete
from django.core.cache import cache
from django.dispatch import receiver
from django.template import engines

from tastypie.models import create_api_key

from mythdoor.apps.main.models import UserProfile, Company, Message, Notification, Post
from mythdoor.apps.member.helper import get_user_common_info
from mythdoor.apps.company.helper import get_company_common_info, generate_company_cache_key
from mythdoor.apps.post.helper import get_post_common_info, generate_post_list_cache_key
from mythdoor.apps.main.tasks import renew_post_cache
from mythdoor.apps.search.tasks import update_search_index
from mythdoor.apps.main.constants import OLD, CACHE_KEY_SPECIAL_SEPARATOR, ACTIVE_STATUS
from mythdoor.settings import CACHE_OBJECT_LIST_TIMEOUT

import datetime


@receiver(post_delete)
@receiver(post_save)
def clear_template_cache(sender, instance, **kwargs):
	list_of_models = ('User', 'UserProfile', 'Post', 'Notification',
					"Message", "Conversation", "UserCredential", "UserConnectionRequest")
	if sender.__name__ in list_of_models:
		cache.delete_pattern("views.decorators.cache.*")


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
	if created:
		UserProfile.objects.create(user=instance)


@receiver(post_save,sender=Message)
def update_new_selling_message_cache(sender, instance, created, **kwargs):
	selling_key = instance.user_receive.username + CACHE_KEY_SPECIAL_SEPARATOR + "message" + CACHE_KEY_SPECIAL_SEPARATOR + "selling"
	selling_count = cache.get(selling_key)
	if created:
		if selling_count == None:
			cache.set(selling_key,1)
		else:
			cache.set(selling_key,selling_count + 1)
	else:
		if instance.status == OLD:
			if selling_count == None:
				cache.set(selling_key,0)
			elif selling_count > 0:
				cache.set(selling_key,selling_count - 1)


@receiver(post_save,sender=Message)
def update_new_buying_message_cache(sender, instance, created, **kwargs):
	buying_key = instance.user_receive.username + CACHE_KEY_SPECIAL_SEPARATOR + "message" + CACHE_KEY_SPECIAL_SEPARATOR + "buying"
	buying_count = cache.get(buying_key)
	if created:
		if buying_count == None:
			cache.set(buying_key,1)
		else:
			cache.set(buying_key,buying_count + 1)
	else:
		if instance.status == OLD:
			if buying_count == None:
				cache.set(buying_key,0)
			elif buying_count > 0:
				cache.set(buying_key,buying_count - 1)


@receiver(post_save,sender=Notification)
def update_new_notification_cache(sender, instance, created, **kwargs):
	key = instance.notify_to.username + CACHE_KEY_SPECIAL_SEPARATOR + "notification"
	count = cache.get(key)
	if created:
		if count == None:
			cache.set(key,1)
		else:
			cache.set(key,count + 1)
	else:
		if instance.status == OLD:
			if count == None:
				cache.set(key,0)
			elif count > 0:
				cache.set(key,count - 1)


@receiver(post_save, sender=Company)
def update_add_company_cache(sender,instance,created,**kwargs):
	companies = cache.get("companies")
	if companies == None:
		companies = {}

	company_info = get_company_common_info(instance)
	companies[instance.unique_id] = company_info

	cache.set(generate_company_cache_key(instance),company_info,timeout=None)
	cache.set("companies",companies,timeout=None)
	cache.set("companies_last_cache_updated",datetime.datetime.now(),timeout=None)
	cache.set("last_cache_updated",datetime.datetime.now(),timeout=None)

	update_search_index.delay()


@receiver(post_save, sender=Post)
def update_add_post_cache(sender,instance,created,**kwargs):
	if instance.post_status == ACTIVE_STATUS:
		all_post_key = generate_post_list_cache_key("all","all",0) 
		all_posts = cache.get(all_post_key)
		if all_posts != None:
			post_info = get_post_common_info(instance)
			all_posts.append(post_info)
			cache.set(all_post_key,all_posts,timeout=CACHE_OBJECT_LIST_TIMEOUT + 30)
		else:
			renew_post_cache.delay()

	update_search_index.delay()


@receiver(pre_delete, sender=Company)
def update_remove_company_cache(sender,instance,**kwargs):
	companies = cache.get("companies")
	if companies == None:
		companies = {}

	if instance.unique_id in companies:
		del companies[instance.unique_id]

	cache.delete(generate_company_cache_key(instance))
	cache.set("companies",companies,timeout=None)
	cache.set("companies_last_cache_updated",datetime.datetime.now(),timeout=None)
	cache.set("last_cache_updated",datetime.datetime.now(),timeout=None)

	update_search_index.delay()


def signals_import():
	post_save.connect(create_api_key, sender=User)

signals_import()
