from django.conf.urls import *

from mythdoor.apps.main.views import MainView

urlpatterns = [
    url(r"^$", MainView.as_view(),name='home'),
]
