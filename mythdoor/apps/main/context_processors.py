from django.contrib.gis.geoip import GeoIP
from django.template import RequestContext

from mythdoor.apps.app_settings import SITE_DATA, DEFAULT_LATITUDE
from mythdoor.apps.app_settings import DEFAULT_LONGITUDE, DEFAULT_CITY
from mythdoor.apps.app_settings import DEFAULT_COUNTRY, DEFAULT_COUNTRY_CODE
from mythdoor.apps.app_settings import DEFAULT_POSTAL_CODE

from mythdoor.apps.main.constants import TRUE, FALSE, AUTO_COLOR_TYPE, AUTO_TITLE_STATUS
from mythdoor.apps.main.constants import AUTO_TRANSMISSION, AUTO_BODY_TYPE, AUTO_CYLINDER_TYPE
from mythdoor.apps.main.constants import AUTO_FUEL_TYPE, HOUSING_NUM_BEDROOM, HOUSING_NUM_BATHROOM
from mythdoor.apps.main.constants import HOUSING_TYPE, NEW, OLD, PENDING_STATUS, ACTIVE_STATUS, EXPIRED_STATUS

from mythdoor.apps.app_helper import get_user_login_object, get_client_ip
from mythdoor.apps.app_helper import get_base_template_path

from mythdoor.settings import STAGE, geoip

import datetime, django_mobile

def site_data(request):
	return SITE_DATA

def global_data(request):
	results = {}
	client_ip = get_client_ip(request)
	results['client_ip'] = client_ip
	results['current_lat'] = DEFAULT_LATITUDE
	results['current_lng'] = DEFAULT_LONGITUDE
	results['current_city'] = DEFAULT_CITY
	results['current_country'] = DEFAULT_COUNTRY
	results['current_country_code'] = DEFAULT_COUNTRY_CODE
	results['current_postal_code'] = DEFAULT_POSTAL_CODE
	results['geo_data'] = None
	if geoip.city(client_ip) != None:
		geo_data = geoip.city(client_ip)
		if geo_data['latitude'] != None: results['current_lat'] = geo_data['latitude']
		if geo_data['longitude'] != None: results['current_lng'] = geo_data['longitude']
		if geo_data['city'] != None: results['current_city'] = geo_data['city']
		if geo_data['country_name'] != None: results['current_country'] = geo_data['country_name']
		if geo_data['country_code'] != None: results['current_country_code'] = geo_data['country_code']
		if geo_data['postal_code'] != None: results['current_postal_code'] = geo_data['postal_code']
		results['geo_data'] = geo_data
		
	results['stage'] = STAGE
	results['base_template'] = get_base_template_path()
	return results

def constants_data(request):
	results = {
		'PENDING_STATUS': PENDING_STATUS,
		'ACTIVE_STATUS': ACTIVE_STATUS,
		'EXPIRED_STATUS': EXPIRED_STATUS,
		'NEW': NEW,
		'OLD': OLD,
		'TRUE': TRUE,
		'FALSE': FALSE,
		'AUTO_COLOR_TYPE': AUTO_COLOR_TYPE,
		'AUTO_TITLE_STATUS': AUTO_TITLE_STATUS,
		'AUTO_BODY_TYPE': AUTO_BODY_TYPE,
		'AUTO_FUEL_TYPE': AUTO_FUEL_TYPE,
		'AUTO_CYLINDER_TYPE': AUTO_CYLINDER_TYPE,
		'AUTO_TRANSMISSION': AUTO_TRANSMISSION,
		'HOUSING_TYPE': HOUSING_TYPE,
		'HOUSING_NUM_BEDROOM': HOUSING_NUM_BEDROOM,
		'HOUSING_NUM_BATHROOM': HOUSING_NUM_BATHROOM,
	}
	return results


