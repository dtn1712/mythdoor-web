from django_jinja import library

import logging, cloudinary

logger = logging.getLogger(__name__)

@library.global_function
def get_image_url(image_id, width, height, crop):
	if width == None:
		return cloudinary.CloudinaryImage(image_id).build_url(height=height,crop=crop)
	elif height == None:
		return cloudinary.CloudinaryImage(image_id).build_url(width=width,crop=crop)
	else:
		return cloudinary.CloudinaryImage(image_id).build_url(width=width,height=height,crop=crop)


@library.global_function
def get_image_element(photo, width, height, crop):
	return "<img src='" + get_image_url(photo.unique_id,width,height,crop) + "' width='" + width + "' height='" + height + "' />"


@library.global_function
def shorten_content(value,length):
	if len(value) <= length: return value
	if value[length] == " ":
		return value[:length] + "..."
	else:
		sub = value[:length]
		return value[:sub.rfind(" ")] + "..."