from django.utils import timezone

from django_jinja import library

from decimal import Decimal

from mythdoor.apps.app_helper import convert_24hr_to_ampm, get_elapse_time_text
from mythdoor.apps.main.constants import MONTH_SHORT_MAP, DAY_SUFFIX, DAY_IN_MONTH_MAP
from mythdoor.apps.main.constants import WEEK_DAY_NUM_MAP, MONTH_FULL_MAP

import datetime, logging

logger = logging.getLogger(__name__)

@library.filter
def valid_numeric(arg):
	if isinstance(arg, (int, float, Decimal)):
		return arg
	try:
		return int(arg)
	except ValueError:
		return float(arg)

@library.filter
def is_today(value):
	today = timezone.datetime.today()
	if today.day == value.day and today.month == value.month and today.year == value.year:
		return True
	return False


@library.filter
def is_tomorrow(value):
	today = timezone.datetime.today()
	if value.day == today.day + 1 and today.month == value.month and today.year == value.year:
		return True
	if today.day == DAY_IN_MONTH_MAP[today.month] and value.month == today.month + 1 and value.day == 1:
		return True
	return False


@library.filter
def first_word_only(value):
	words = value.split()
	return words[0]


@library.filter
def first_char_only(value):
	return value[0]


@library.filter
def words_first_char_upper(value):
	words = value.split()
	result = ""
	for word in words:
		new_word = word[0].upper() + word[1:]
		result += new_word + " "
	return result[:len(result)-1]


@library.filter
def display_elapse_time(value):
	return get_elapse_time_text(value)


@library.filter
def display_price(value,arg):
	if arg.lower() == "usd":
		return format(value, ',.2f')
	else:
		return format(value,",")


@library.filter
def get_dictionary_item_value(value, arg):
	return value[arg]


@library.filter
def initial(value):
	if len(value) == 1:
		return value.upper() + "."
	elif len(value) == 2:
		return value[0].upper() + "." + value[1].upper() + "."
	return value.upper()


