from mythdoor.apps.app_helper import setup_constant_countries_alpha2, setup_constant_countries_alpha3
from mythdoor.apps.app_settings import CREDENTIAL_PROFESSIONAL_CONNECTION, CREDENTIAL_OTHERS, CREDENTIAL_PROFESSIONAL
from mythdoor.apps.app_settings import EMAIL_TYPE_OTHERS, EMAIL_TYPE_COMPANY, EMAIL_TYPE_COMPANY_PENDING
from mythdoor.apps.app_settings import SECONDS_PER_DAY, SECONDS_PER_HOUR, SECONDS_PER_MINUTE

CACHE_KEY_SPECIAL_SEPARATOR = ":::"

EMPTY_STRING = ""

BLANK_CHOICE = ("","------------")
NULL_CHOICE = (None,"")

#SQUARE_FEET = "sq_ft"

VALID_FILE_SIZE = 5242880

DEFAULT_PAGE_SIZE = 25
PAGE_PARAMETER = "offset"
PAGE_SIZE_PARAMETER = "limit"

PASSCODE = "passcode"
ACTIVATE_LINK = "activate_link"

ALL_TYPE = "all"

FREE_TIER = "free"
PAID_TIER = "paid"

PENDING_STATUS = "pending"
ACTIVE_STATUS = "active"
EXPIRED_STATUS = "expired"

APPROVE_STATUS = "approve"

COMPLETED_STATUS = "completed"

DEFAULT_POST_QUOTA = 10

EMAIL_SEPARATOR = ";"

COMMON_SEPARATOR = EMAIL_SEPARATOR

PUBLIC = "1"
PRIVATE = "0"

UNLIMITED = -1

NEW = "1"
OLD = "0"

TRUE = "1"
FALSE = "0"

HIDE = "0"
SHOW = "1"

ACCEPT = "1"
PENDING = "0"
DECLINE = "-1"

APPROVED = "approved"
REJECTED = "rejected"

YES_VALUE = "1"
NO_VALUE = "0"

MONTH_SHORT_MAP = {
    1: "Jan",
    2: "Feb",
    3: "Mar",
    4: "Apr",
    5: "May",
    6: "Jun",
    7: "Jul",
    8: "Aug",
    9: "Sep",
    10: "Oct",
    11: "Nov",
    12: "Dec"
}

MONTH_FULL_MAP = {
    1: "January",
    2: "February",
    3: "March",
    4: "April",
    5: "May",
    6: "June",
    7: "July",
    8: "August",
    9: "September",
    10: "October",
    11: "November",
    12: "December"
}

DAY_IN_MONTH_MAP = {
    1:31,
    2:28,
    3:31,
    4:30,
    5:31,
    6:30,
    7:31,
    8:31,
    9:30,
    10:31,
    11:30,
    12:31
}

WEEK_DAY_TEXT_MAP = {
    "Mon": "Monday",
    "Tue": "Tuesday",
    "Wed": "Wednesday",
    "Thu": "Thursday",
    "Fri": "Friday",
    "Sat": "Saturday",
    "Sun": "Sunday"
}

WEEK_DAY_NUM_MAP = {
    0: "Monday",
    1: "Tuesday",
    2: "Wednesday",
    3: "Thursday",
    4: "Friday",
    5: "Saturday",
    6: "Sunday"
}

WEEK_DAY = (
  ('0',"Monday"),
  ('1',"Tuesday"),
  ('2',"Wednesday"),
  ('3',"Thursday"),
  ('4',"Friday"),
  ('5',"Saturday"),
  ('6',"Sunday")
)

DAY_SUFFIX = {
    1: "st",
    2: "nd",
    3: "rd"
}

USA_STATES = (
   ("AL","Alabama"),("AK","Alaska"),("AZ","Arizona"),("AR","Arkansas"),("CA","California"),
   ("CO","Colorado"),("CT","Connecticut"),("DE","Delaware"),("FL","Florida"),("GA","Georgia"),
   ("HI","Hawaii"),("ID","Idaho"),("IL","Illinois"),("IN","Indiana"),("IA","Iowa"),
   ("KS","Kansas"),("KY","Kentucky"),("LA","Louisana"),("ME","Maine"),("MD","Maryland"),
   ("MA","Massachusetts"),("MI","Michigan"),("MN","Minnesota"),("MS","Mississippi"),("MO","Missouri"),
   ("MT","Montana"),("NE","Nebraska"),("NV","Nevada"),("NH","New Hampshire"),("NJ","New Jersey"),
   ("NM","New Mexico"),("NY","New York"),("NC","North Carolina"),("ND","North Dakota"),("OH","Ohio"),
   ("OK","Oklahoma"),("OR","Oregon"),("PA","Pennsylvania"),("RI","Rhode Island"),("SC","South Carolina"),
   ("SD","South Dakota"),("TN","Tennessee"),("TX","Texas"),("UT","Utah"),("VT","Vermont"),
   ("VA","Virginia"),("WA","Washington"),("WV","West Virginia"),("WI","Wisconsin"),("WY","Wyoming"),
)

COUNTRIES_ALPHA2 = setup_constant_countries_alpha2()
COUNTRIES_ALPHA3 = setup_constant_countries_alpha3()

DEFAULT_COUNTRIES_FORMAT = COUNTRIES_ALPHA2

YES_NO = (
  ("1","Yes"),
  ("0","No"),
)

YES_NO_WITH_BLANK_CHOICE = (
  BLANK_CHOICE,
  ("1","Yes"),
  ("0","No"),
)

MESSAGE_STATUS = (
  (OLD, "old"),
  (NEW, "new"),
)

NOTIFICATION_STATUS = (
  (OLD,"old"),
  (NEW,"new"),
)

APPEARANCE_STATUS = (
  (HIDE, "hide"),
  (SHOW, "show"),
)

PRIVACY_STATUS = (
    (PUBLIC,'Public'),
    (PRIVATE,"Private")
)

PRIVACY_STATUS_MAP = {
  "1": "Public",
  "0": "Private"
}

NOTIFICATION_TYPE = (
  ("new_message","New Message"),
  ("connection_request","Connection Request")
)

CONNECTION_TYPE = (
  ("professional", "Professional"),
)

CONNECTION_REQUEST_STATUS = (
  ("pending","Pending"),
  ("approved","Approved"),
  ("rejected","Rejected")
)

POST_CATEGORY = (
  ("auto","Auto"),
  ("for_sale","For Sale"),
  ("wanted","Wanted"),
  ("housing","Housing"),
  ("services","Services"),
  ("event","Event"),
  ("pet","Pet")
)

POST_CATEGORY_MAP = {
  "auto":"Auto",
  "for_sale":"For Sale",
  "wanted":"Wanted",
  "housing":"Housing",
  "services":"Services",
  "event":"Event",
  "pet":"Pet"
}

POST_CATEGORY_WITH_BLANK_CHOICE = (
  BLANK_CHOICE,
  ("auto","Auto"),
  ("for_sale","For Sale"),
  ("wanted","Wanted"),
  ("housing","Housing"),
  ("services","Services"),
  ("event","Event"),
  ("pet","Pet")
)

POST_SUBCATEGORY = (

  # For sale
  ("antiques","Antiques"),
  ("appliances","Appliances"),
  ("arts_crafts","Arts & Crafts"),
  ("baby_kid","Baby & Kid"),
  ("beauty_health","Beauty & Health"),
  ("bikes","Bikes"),
  ("clothes_acc","Clothes & Accessories"),
  ("collectibles","Collectibles"),
  ("computer","Computer"),
  ("electronic","Electronic"),
  ("furniture","Furniture"),
  ("general","General"),
  ("home_garden","Home & Garden"),
  ("household", "Household"),
  ("jewelry","Jewelry"),
  ("musical_instruments","Musical Instruments"),
  ("photo_video","Photo & Video"),
  ("shoes","Shoes"),
  ("sports_outdoor","Sports & Outdoors"),
  ("toys_game","Toys & Games"),
  ("tools_machinery","Tools & Machinery"),
  ("video_games","Video Games"),

  # Auto
  ("car","Car & Trucks"),
  ("car_part","Auto parts"),
  ("motorcycle","Motorcycle"),

  # Housing
  ("apartment","Apartment"),
  ("house","House"),

  # Pet
  ("pet","Pet"),
  ("pet_supplies","Pet Supplies"),

  # Event
  ("tickets","Tickets"),
  ("meetups","Meetups"),
  ("volunteers","Volunteers"),

  # Service
  ("auto_service","Automotive"),
  ("beauty_service","Beauty"),
  ("legal_service","Legal"),
  ("home_service","Home"),
  ("health_service","Health"),
  ("moving_service","Moving"),
  ("pet_service","Pet"),
)

POST_SUBCATEGORY_ELEMENT_MAP = {

  # For sale
  "antiques":"Antiques",
  "appliances":"Appliances",
  "arts_crafts":"Arts & Crafts",
  "baby_kid":"Baby & Kid",
  "beauty_health":"Beauty & Health",
  "bikes":"Bikes",
  "clothes_acc":"Clothes & Accessories",
  "collectibles":"Collectibles",
  "computer":"Computer",
  "electronic":"Electronic",
  "furniture":"Furniture",
  "general":"General",
  "home_garden":"Home & Garden",
  "household": "Household",
  "jewelry":"Jewelry",
  "musical_instruments":"Musical Instruments",
  "photo_video":"Photo & Video",
  "shoes":"Shoes",
  "sports_outdoor":"Sports & Outdoors",
  "toys_game":"Toys & Games",
  "tools_machinery":"Tools & Machinery",
  "video_games":"Video Games",

  # Auto
  "car":"Car & Trucks",
  "car_part":"Auto parts",
  "motorcycle":"Motorcycle",

  # Housing
  "apartment":"Apartment",
  "house":"House",

  # Pet
  "pet":"Pet",
  "pet_supplies":"Pet Supplies",

  # Event
  "tickets":"Tickets",
  "meetups":"Meetups",
  "volunteers":"Volunteers",

  # Service
  "auto_service":"Automotive",
  "beauty_service":"Beauty",
  "legal_service":"Legal",
  "home_service":"Home",
  "health_service":"Health",
  "moving_service":"Moving",
  "pet_service":"Pet",
}

POST_SUBCATEGORY_WITH_BLANK_CHOICE = (
  BLANK_CHOICE,
  # For sale
  ("antiques","Antiques"),
  ("appliances","Appliances"),
  ("arts_crafts","Arts & Crafts"),
  ("baby_kid","Baby & Kid"),
  ("beauty_health","Beauty & Health"),
  ("bikes","Bikes"),
  ("clothes_acc","Clothes & Accessories"),
  ("collectibles","Collectibles"),
  ("computer","Computer"),
  ("electronic","Electronic"),
  ("furniture","Furniture"),
  ("general","General"),
  ("home_garden","Home & Garden"),
  ("household", "Household"),
  ("jewelry","Jewelry"),
  ("musical_instruments","Musical Instruments"),
  ("photo_video","Photo & Video"),
  ("shoes","Shoes"),
  ("sports_outdoor","Sports & Outdoors"),
  ("toys_game","Toys & Games"),
  ("tools_machinery","Tools & Machinery"),
  ("video_games","Video Games"),

  # Auto
  ("car","Car & Trucks"),
  ("car_part","Auto parts"),
  ("motorcycle","Motorcycle"),

  # Housing
  ("apartment","Apartment"),
  ("house","House"),

  # Pet
  ("pet","Pet"),
  ("pet_supplies","Pet Supplies"),

  # Event
  ("tickets","Tickets"),
  ("meetups","Meetups"),
  ("volunteers","Volunteers"),

  # Service
  ("auto_service","Automotive"),
  ("beauty_service","Beauty"),
  ("legal_service","Legal"),
  ("home_service","Home"),
  ("health_service","Health"),
  ("moving_service","Moving"),
  ("pet_service","Pet"),
)

POST_SUBCATEGORY_WITH_BLANK_CHOICE_MAP = {
  "auto": (
      BLANK_CHOICE,
      ("car","Car & Trucks"),
      ("car_part","Auto parts"),
      ("motorcycle","Motorcycle"),
  ),
  "housing": (
      BLANK_CHOICE,
      ("apartment","Apartment"),
      ("house","House"),
  ),
  "for_sale": (
      BLANK_CHOICE,
      ("antiques","Antiques"),
      ("appliances","Appliances"),
      ("arts_crafts","Arts & Crafts"),
      ("baby_kid","Baby & Kid"),
      ("beauty_health","Beauty & Health"),
      ("bikes","Bikes"),
      ("clothes_acc","Clothes & Accessories"),
      ("collectibles","Collectibles"),
      ("computer","Computer"),
      ("electronic","Electronic"),
      ("furniture","Furniture"),
      ("general","General"),
      ("home_garden","Home & Garden"),
      ("household", "Household"),
      ("jewelry","Jewelry"),
      ("musical_instruments","Musical Instruments"),
      ("photo_video","Photo & Video"),
      ("shoes","Shoes"),
      ("sports_outdoor","Sports & Outdoors"),
      ("toys_game","Toys & Games"),
      ("tools_machinery","Tools & Machinery"),
      ("video_games","Video Games"),
  ),
  "wanted": None,
  "services": (
      BLANK_CHOICE,
      ("auto_service","Automotive"),
      ("beauty_service","Beauty"),
      ("legal_service","Legal"),
      ("home_service","Home"),
      ("health_service","Health"),
      ("moving_service","Moving"),
      ("pet_service","Pet"),
  ),
  "pet": (
      BLANK_CHOICE,
      ("pet","Pet"),
      ("pet_supplies","Pet Supplies"),
  ),
  "event": (
      BLANK_CHOICE,
      ("tickets","Tickets"),
      ("meetups","Meetups"),
      ("volunteers","Volunteers"),
  )
}

POST_SUBCATEGORY_MAP = {
  "auto": (
      ("car","Car & Trucks"),
      ("car_part","Auto parts"),
      ("motorcycle","Motorcycle"),
  ),
  "housing": (
      ("apartment","Apartment"),
      ("house","House"),
  ),
  "for_sale": (
      ("antiques","Antiques"),
      ("appliances","Appliances"),
      ("arts_crafts","Arts & Crafts"),
      ("baby_kid","Baby & Kid"),
      ("beauty_health","Beauty & Health"),
      ("bikes","Bikes"),
      ("clothes_acc","Clothes & Accessories"),
      ("collectibles","Collectibles"),
      ("computer","Computer"),
      ("electronic","Electronic"),
      ("furniture","Furniture"),
      ("general","General"),
      ("home_garden","Home & Garden"),
      ("household", "Household"),
      ("jewelry","Jewelry"),
      ("musical_instruments","Musical Instruments"),
      ("photo_video","Photo & Video"),
      ("shoes","Shoes"),
      ("sports_outdoor","Sports & Outdoors"),
      ("toys_game","Toys & Games"),
      ("tools_machinery","Tools & Machinery"),
      ("video_games","Video Games"),
  ),
  "wanted": None,
  "services": (
      ("auto_service","Automotive"),
      ("beauty_service","Beauty"),
      ("legal_service","Legal"),
      ("home_service","Home"),
      ("health_service","Health"),
      ("moving_service","Moving"),
      ("pet_service","Pet"),
  ),
  "pet": (
      ("pet","Pet"),
      ("pet_supplies","Pet Supplies"),
  ),
  "event": (
      ("tickets","Tickets"),
      ("meetups","Meetups"),
      ("volunteers","Volunteers"),
  )
}

POST_STATUS = (
  ("pending", "Pending"),
  ("active", "Active"),
  ("expired","Expired"),
  ("sold","Sold"),
)

POST_STATUS_MAP = {
  "pending": "Pending",
  "active": "Active",
  "expired": "Expired",
  "sold": "Sold"
}

POST_TIER = (
  ("free","Free Tier"),
  ("paid","Paid Tier")
)

SIGNUP_EMAIL_TYPE = (
  (EMAIL_TYPE_OTHERS,"Others"),
  (EMAIL_TYPE_COMPANY,"Company"),
  (EMAIL_TYPE_COMPANY_PENDING,"Company Pending"),
)

SIGNUP_REQUEST_STATUS = (
  ("pending","Pending"),
  ("completed","Completed")
)

USER_EMAIL_TYPE = SIGNUP_EMAIL_TYPE

CREDENTIAL_TYPE = (
  (CREDENTIAL_PROFESSIONAL,CREDENTIAL_PROFESSIONAL),
  (CREDENTIAL_PROFESSIONAL_CONNECTION,CREDENTIAL_PROFESSIONAL_CONNECTION),
  (CREDENTIAL_OTHERS,CREDENTIAL_OTHERS),
)

AUTO_TRANSMISSION = (
  ("automatic","Automatic"),
  ("manual","Manual"),
  ("other","Other")
)

AUTO_TITLE_STATUS = (
  ("clean","Clean"),
  ("salvage","Salvage"),
  ("rebuilt","Rebuilt"),
  ("parts_only","Parts Only"),
  ("lien","Lien")
)

AUTO_FUEL_TYPE = (
  ("gas","Gas"),
  ("diesel","Diesel"),
  ("hybrid","Hybrid"),
  ("electric","Electric"),
  ("other","Other"),
)

AUTO_COLOR_TYPE = (
  ("black","Black"),
  ("blue","Blue"),
  ("brown","Brown"),
  ("green","Green"),
  ("grey","Grey"),
  ("orange","Orange"),
  ("purple","Purple"),
  ("red","Red"),
  ("silver","Silver"),
  ("white","White"),
  ("yellow","Yellow"),
  ("custom","Custom")
)

AUTO_CYLINDER_TYPE = (
  ("3","3 Cylinders"),
  ("4","4 Cylinders"),
  ("5","5 Cylinders"),
  ("6","6 Cylinders"),
  ("8","8 Cylinders"),
  ("10","10 Cylinders"),
  ("12","12 Cylinders"),
)

AUTO_BODY_TYPE = (
  ("bus","Bus"),
  ("convertible","Convertible"),
  ("coupe","Coupe"),
  ("hatchback","Hatchback"),
  ("minivan","Mini Van"),
  ("sedan","Sedan"),
  ("truck","Truck"),
  ("suv","SUV"),
  ("wagon","Wagon"),
  ("van","Van"),
  ("other","Other"),
)

HOUSING_TYPE = (
  ("apartment","Apartment"),
  ("condo","Condo"),
  ("cottage_cabin","Cottage/Cabin"),
  ("duplex","Duplex"),
  ("flat","Flat"),
  ("house","House"),
  ("loft","Loft"),
  ("townhouse","Townhouse"),
)

HOUSING_NUM_BEDROOM = (
  (1,"1+ bedroom"),
  (2,"2+ bedroom"),
  (3,"3+ bedroom"),
  (4,"4+ bedroom"),
  (5,"5+ bedroom"),
  (6,"6+ bedroom"),
  (7,"7+ bedroom"),
  (8,"8+ bedroom"),
)

HOUSING_NUM_BATHROOM = (
  (1,"1+ bathroom"),
  (2,"2+ bathroom"),
  (3,"3+ bathroom"),
  (4,"4+ bathroom"),
  (5,"5+ bathroom"),
  (6,"6+ bathroom"),
  (7,"7+ bathroom"),
  (8,"8+ bathroom"),
)

# AREA_UNIT_TYPE = (
#   ("sq_ft","Square Feet"),
#   ("",""),
# )

# AREA_UNIT_TYPE_MAP = {
#   "sq_ft":"Sq/ft"
# }

DISTANCE_RANGE = (
  (50,"50 Miles"),
  (40,"40 Miles"),
  (30,"30 Miles"),
  (20,"20 Miles"),
  (10,"10 Miles")
)

DISTANCE_RANGE_MAP = {
  50:"50 Miles",
  40:"40 Miles",
  30:"30 Miles",
  20:"20 Miles",
  10:"10 Miles",
}

ITEM_CONDITION = (
  ("excellent","Excellent"),
  ("very_good","Very Good"),
  ("good","Good"),
  ("acceptable","Average"),
)

ITEM_CONDITION_WITH_BLANK_CHOICE = (
  BLANK_CHOICE,
  ("excellent","Excellent"),
  ("very_good","Very Good"),
  ("good","Good"),
  ("acceptable","Acceptable"),
)

AUTO_DETAIL_FIELD_LABEL_TYPE_MAP = {
  "vin": ["Vin","char"],
  "make": ['Make',"char"],
  "model": ["Model","char"],
  "year": ["Year","char"],
  "mileage": ["Mileage","int"],
  "color": ["Color","char"],
  "title_status": ["Title","char"],
  "body_type": ["Body Type","char"],
  "transmission": ["Tranmission","char"],
  "cylinder": ["Cylinder","char"],
  "fuel": ["Fuel Type","char"],
}

HOUSING_DETAIL_FIELD_LABEL_TYPE_MAP = {
  "total_area": ["Square Feet","int"],
  "num_bedroom" : ["# Bedroom","int"],
  "num_bathroom" : ["# Bathroom","int"],
  "is_allow_cat" : ['Cat OK',"boolean"],
  "is_allow_dog" : ["Dog OK","boolean"],
  "housing_type" : ['Type',"char"],
  "available_on": ['Available',"date"]
}

POST_CATEGORY_INFO_MAP = {
  "auto":("Auto","fa-car"),
  "for_sale":("For Sale","fa-buysellads"),
  "wanted":("Wanted","fa-hand-paper-o"),
  "housing":("Housing",'fa-home'),
  "services":("Services",'fa-wrench'),
  "event":("Event",'fa-calendar'),
  "pet":("Pet",'fa-paw')
}

CURRENCY_ICON_MAP =  {
  "USD": "$"
}

