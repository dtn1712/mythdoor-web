from celery import shared_task

from django.db.models import Count
from django.core.cache import cache

from mythdoor.apps.app_helper import data_backup as data_backup_helper
from mythdoor.apps.app_helper import generate_html_snippet
from mythdoor.apps.main.constants import ACTIVE_STATUS, EXPIRED_STATUS, DEFAULT_PAGE_SIZE
from mythdoor.apps.post.helper import get_filter_post, get_post_list_common_info
from mythdoor.apps.post.helper import generate_post_list_cache_key, generate_post_list_template_cache_key
from mythdoor.apps.main.models import Post
from mythdoor.settings import CACHE_OBJECT_LIST_TIMEOUT


@shared_task
def data_backup():
	data_backup_helper()


@shared_task
def check_post_status():
	posts = Post.objects.filter(post_status=ACTIVE_STATUS)
	post_unique_ids = []
	for post in posts:
		if post.is_expired():
			post_unique_ids.append(post.unique_id)
	Post.objects.filter(unique_id__in=post_unique_ids).update(post_status=EXPIRED_STATUS)


@shared_task
def renew_post_cache():
	key = generate_post_list_cache_key("all","all",0)
	template_key = generate_post_list_template_cache_key("all","all",0)
	posts = get_post_list_common_info(Post.objects.annotate(null_photo=Count('main_photo')).filter(post_status=ACTIVE_STATUS,is_deleted=False).order_by("-create_time","-null_photo").select_related()[0:DEFAULT_PAGE_SIZE])
	template_snippet = generate_html_snippet(None,"post_list",{"posts":posts})
	cache.set(key,posts,timeout=CACHE_OBJECT_LIST_TIMEOUT + 30)
	cache.set(template_key,template_snippet,timeout=CACHE_OBJECT_LIST_TIMEOUT + 30)
