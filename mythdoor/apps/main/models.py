from django.db import models
from django.db.models import Count
from django.contrib.auth.models import User
from django.contrib import admin
from django.core.cache import cache
from django.conf import settings
from django.utils import timezone
from django.template.loader import render_to_string
from django.db.models import Q
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from django.contrib.gis.geos import Point

from djmoney.models.fields import MoneyField

from mythdoor.settings import MEDIA_URL, DEFAULT_FROM_EMAIL, SITE_NAME, EMAIL_USE_TLS
from mythdoor.apps.account.settings import UNIQUE_EMAIL, EMAIL_CONFIRMATION_EXPIRE_DAYS
from mythdoor.apps.main.constants import MESSAGE_STATUS, NEW, OLD, CONNECTION_TYPE, DEFAULT_POST_QUOTA
from mythdoor.apps.main.constants import CONNECTION_REQUEST_STATUS, POST_CATEGORY, POST_SUBCATEGORY
from mythdoor.apps.main.constants import CREDENTIAL_PROFESSIONAL, CREDENTIAL_OTHERS, CREDENTIAL_PROFESSIONAL_CONNECTION
from mythdoor.apps.main.constants import SIGNUP_REQUEST_STATUS, SIGNUP_EMAIL_TYPE, USER_EMAIL_TYPE, POST_STATUS
from mythdoor.apps.main.constants import AUTO_TRANSMISSION, AUTO_TITLE_STATUS, AUTO_BODY_TYPE
from mythdoor.apps.main.constants import AUTO_CYLINDER_TYPE, AUTO_FUEL_TYPE, HOUSING_TYPE, PASSCODE, ACTIVATE_LINK
from mythdoor.apps.main.constants import PENDING_STATUS, POST_TIER, AUTO_COLOR_TYPE, ITEM_CONDITION
from mythdoor.apps.main.constants import FREE_TIER, AUTO_DETAIL_FIELD_LABEL_TYPE_MAP, HOUSING_DETAIL_FIELD_LABEL_TYPE_MAP
from mythdoor.apps.main.constants import CREDENTIAL_TYPE, ACTIVE_STATUS, APPROVE_STATUS, NOTIFICATION_STATUS, NOTIFICATION_TYPE
from mythdoor.apps.main.constants import POST_CATEGORY_MAP, POST_SUBCATEGORY_ELEMENT_MAP
from mythdoor.apps.main.constants import CACHE_KEY_SPECIAL_SEPARATOR
from mythdoor.apps.main.managers import EmailAddressManager, EmailConfirmationManager
from mythdoor.apps.app_helper import generate_unique_id, get_model_value_fields

from geopy.distance import vincenty

from cloudinary.models import CloudinaryField

from imagekit.models import ProcessedImageField

from dateutil.relativedelta import relativedelta

import logging, ast, datetime, uuid

logger = logging.getLogger(__name__)

try:
	storage = settings.MULTI_IMAGES_FOLDER + '/'
except AttributeError:
	storage = 'upload_storage/'


def get_unique_id():
	return generate_unique_id()


class Email(models.Model):
	unique_id = models.CharField(max_length=100,primary_key=True,editable=False,default=get_unique_id)
	to_emails = models.TextField()
	email_template = models.CharField(max_length=200)
	subject = models.CharField(max_length=350,blank=True)
	text_content = models.TextField(blank=True)
	context_data = models.TextField()
	status = models.CharField(max_length=50,default="pending")
	send_time = models.DateTimeField(blank=True,null=True)
	request_time = models.DateTimeField(auto_now_add=True)


class SignupRequest(models.Model):
	unique_id = models.CharField(max_length=100,primary_key=True,editable=False,default=get_unique_id)
	email = models.EmailField()
	confirmation_key = models.CharField(max_length=64, unique=True)
	request_status = models.CharField(max_length=20,choices=SIGNUP_REQUEST_STATUS,default=PENDING_STATUS)
	signup_email_type = models.CharField(max_length=20,choices=SIGNUP_EMAIL_TYPE,blank=True)
	suggest_company = models.ForeignKey("Company",blank=True,null=True,related_name="email_signup_suggest_company",on_delete=models.SET_NULL)
	request_time = models.DateTimeField(auto_now_add=True)


class Photo(models.Model):
	unique_id = models.CharField(max_length=100,primary_key=True,editable=False,default=get_unique_id)
	name = models.CharField(max_length=150,blank=True)
	image = ProcessedImageField(upload_to=storage+"/photo/%Y/%m/%d",format='JPEG',options={'quality': 70},blank=True,null=True)
	rotation_angle = models.IntegerField(default=0)
	image_url = models.CharField(max_length=300,blank=True)
	image_secure_url = models.CharField(max_length=300,blank=True)
	width = models.IntegerField()
	height = models.IntegerField()
	generated_aliases = models.CharField(max_length=300,blank=True)
	is_all_aliases_generated = models.BooleanField(default=False)
	upload_time = models.DateTimeField(auto_now_add=True)
	upload_success = models.BooleanField(default=True)
	user_create = models.ForeignKey(User,blank=True,null=True,on_delete=models.SET_NULL)

	#image = ThumbnailerImageField(upload_to=storage+"/photo/%Y/%m/%d")

	# fixed_medium_height_thumbnail = ImageSpecField(source='image',id='mythdoor:photo:fixed_medium_height_thumbnail')
	# fixed_medium_width_thumbnail = ImageSpecField(source='image',id='mythdoor:photo:fixed_medium_width_thumbnail')
	# mini_thumbnail = ImageSpecField(source="image",processors=[ResizeToFill(50,50)],format='JPEG',options={'quality':70})
	# small_thumbnail = ImageSpecField(source="image",processors=[ResizeToFill(100,100)],format='JPEG',options={'quality':70})
	# medium_thumbnail = ImageSpecField(source="image",processors=[ResizeToFill(300,200)],format='JPEG',options={'quality':70})
	# large_thumbnail = ImageSpecField(source="image",processors=[ResizeToFill(600,400)],format='JPEG',options={'quality':70})

	def __unicode__(self):
		return self.name

class Message(models.Model):
	unique_id = models.CharField(max_length=100,primary_key=True,editable=False,default=get_unique_id)
	user_send = models.ForeignKey(User,related_name="user_send")
	user_receive = models.ForeignKey(User,related_name="user_receive")
	content = models.TextField()
	status = models.CharField(max_length=1,choices=MESSAGE_STATUS,default=NEW)
	create_time = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return self.content

class Notification(models.Model):
	unique_id = models.CharField(max_length=100,primary_key=True,editable=False,default=get_unique_id)
	content = models.TextField()
	status = models.CharField(max_length=1,choices=NOTIFICATION_STATUS,default=NEW)
	notification_type = models.CharField(max_length=30,choices=NOTIFICATION_TYPE,null=True)
	notify_to = models.ForeignKey(User, related_name='notify_to')
	notify_from = models.ForeignKey(User, related_name='notify_from',blank=True,null=True,on_delete=models.SET_NULL)
	related_link = models.TextField(blank=True)
	create_time = models.DateTimeField(auto_now_add=True)

class ConversationReport(models.Model):
	unique_id = models.CharField(max_length=100,primary_key=True,editable=False,default=get_unique_id)
	conversation_report = models.ForeignKey("Conversation",related_name='conversation_report')
	content = models.TextField()
	user_create = models.ForeignKey(User,related_name='user_report_conversation')
	create_time = models.DateTimeField(auto_now_add=True)

class Conversation(models.Model):
	unique_id = models.CharField(max_length=100,primary_key=True,editable=False,default=get_unique_id)
	user1 = models.ForeignKey(User,related_name="conversation_user1")
	user2 = models.ForeignKey(User,related_name="conversation_user2")
	user1_messages = models.ManyToManyField("Message",related_name="conversation_user1_messages")
	user2_messages = models.ManyToManyField("Message",related_name="conversation_user2_messages")
	post = models.ForeignKey("Post",related_name="post_conversation")
	latest_message = models.ForeignKey("Message",related_name="conversation_latest_message",null=True,on_delete=models.SET_NULL)

	def __unicode__(self):
		return unicode(self.unique_id)


class TransactionFeedback(models.Model):
	unique_id = models.CharField(max_length=100,primary_key=True,editable=False,default=get_unique_id)
	post = models.ForeignKey("Post",related_name="transaction_feedback_post")
	interest_buyer = models.ForeignKey(User,related_name="transaction_feedback_user")
	rating = models.IntegerField()
	comment = models.TextField(blank=True)
	create_time = models.DateTimeField(auto_now_add=True)


class PostReport(models.Model):
	unique_id = models.CharField(max_length=100,primary_key=True,editable=False,default=get_unique_id)
	report_post = models.ForeignKey("Post",related_name='report_post')
	content = models.TextField()
	user_create = models.ForeignKey(User,related_name='user_report_post')
	create_time = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return unicode(self.unique_id)


class PostAutoDetail(models.Model):
	unique_id = models.CharField(max_length=100,primary_key=True,editable=False,default=get_unique_id)
	vin = models.CharField(max_length=25,blank=True)
	make = models.CharField(max_length=25,blank=True)
	model = models.CharField(max_length=25,blank=True)
	year = models.CharField(max_length=4,blank=True)
	mileage = models.CharField(max_length=25,blank=True)
	color = models.CharField(max_length=15,choices=AUTO_COLOR_TYPE,blank=True)
	title_status = models.CharField(max_length=25,choices=AUTO_TITLE_STATUS,blank=True)
	body_type = models.CharField(max_length=25,choices=AUTO_BODY_TYPE,blank=True)
	transmission = models.CharField(max_length=25,choices=AUTO_TRANSMISSION,blank=True)
	cylinder = models.CharField(max_length=20,choices=AUTO_CYLINDER_TYPE,blank=True)
	fuel = models.CharField(max_length=20,choices=AUTO_FUEL_TYPE,blank=True)

	def __unicode__(self):
		return unicode(self.unique_id)

	def is_empty(self):
		if len(self.vin) != 0 or len(self.make) != 0 or len(self.model) != 0 or len(self.year) != 0:
			return False
		if len(self.mileage) != 0 or len(self.color) != 0 or len(self.title_status) != 0 or len(self.body_type) != 0:
			return False
		if len(self.transmission) != 0 or len(self.cylinder) != 0 or len(self.fuel) != 0:
			return False
		return True

	def get_value_fields(self):
		return get_model_value_fields(self,AUTO_DETAIL_FIELD_LABEL_TYPE_MAP)


class PostHousingDetail(models.Model):
	unique_id = models.CharField(max_length=100,primary_key=True,editable=False,default=get_unique_id)
	address = models.CharField(max_length=300,blank=True)
	total_area = models.IntegerField(blank=True,null=True)
	num_bedroom = models.IntegerField(blank=True,null=True)
	num_bathroom = models.IntegerField(blank=True,null=True)
	is_allow_cat = models.NullBooleanField(blank=True)
	is_allow_dog = models.NullBooleanField(blank=True)
	housing_type = models.CharField(max_length=30,choices=HOUSING_TYPE,blank=True)
	available_on = models.DateField(blank=True,null=True)

	def __unicode__(self):
		return unicode(self.unique_id)

	def is_empty(self):
		if self.total_area != None or self.num_bedroom != None or self.is_allow_cat != None:
			return False
		if self.num_bathroom != None or self.is_allow_dog != None:
			return False
		if len(self.housing_type) != 0 or self.available_on != None:
			return False
		return True

	def get_value_fields(self):
		value_map = get_model_value_fields(self,HOUSING_DETAIL_FIELD_LABEL_TYPE_MAP)
		return value_map

class PostContactPermission(models.Model):
	unique_id = models.CharField(max_length=100,primary_key=True,editable=False,default=get_unique_id)
	group_permission_type = models.CharField(max_length=50)
	companies = models.ManyToManyField("Company",blank=True)

	def __unicode__(self):
		return unicode(self.unique_id)

class PostLocation(models.Model):
	unique_id = models.CharField(max_length=100,primary_key=True,editable=False,default=get_unique_id)
	postal_code = models.CharField(max_length=20)
	latitude = models.FloatField()
	longitude = models.FloatField()
	neighborhood = models.CharField(max_length=100,blank=True)
	state = models.CharField(max_length=2,db_index=True)
	city = models.CharField(max_length=100)

	def __unicode__(self):
		return unicode(self.unique_id)


class Post(models.Model):
	unique_id = models.CharField(max_length=100,primary_key=True,editable=False,default=get_unique_id)
	title = models.CharField(max_length=300)
	content = models.TextField()
	price =  MoneyField(max_digits=10, decimal_places=2, default_currency='USD',blank=True,null=True)
	main_photo = models.ForeignKey("Photo",blank=True,null=True,related_name="post_main_photo",on_delete=models.SET_NULL)
	photos = models.ManyToManyField("Photo",blank=True,related_name="post_photos")
	user_create = models.ForeignKey(User,related_name="post_user_create")
	post_status = models.CharField(max_length=25,choices=POST_STATUS,default=PENDING_STATUS,db_index=True)
	auto_detail = models.ForeignKey("PostAutoDetail",blank=True,null=True,on_delete=models.SET_NULL)
	housing_detail = models.ForeignKey("PostHousingDetail",blank=True,null=True,on_delete=models.SET_NULL)
	item_condition = models.CharField(max_length=25,choices=ITEM_CONDITION,blank=True)
	category = models.CharField(max_length=50,choices=POST_CATEGORY,db_index=True)
	subcategory = models.CharField(max_length=100,choices=POST_SUBCATEGORY,db_index=True)
	post_tier = models.CharField(max_length=25,choices=POST_TIER,default=FREE_TIER)
	contact_permission = models.ForeignKey("PostContactPermission",related_name="post_contact_permission")
	location = models.ForeignKey("PostLocation",related_name="post_location")
	duration = models.IntegerField(default=30)
	create_time = models.DateTimeField(auto_now_add=True)
	last_edit_time = models.DateTimeField(blank=True,null=True)
	num_repost = models.IntegerField(default=0)
	last_repost_time = models.DateTimeField(blank=True,null=True)
	is_deleted = models.BooleanField(default=False)

	def __unicode__(self):
		return unicode(self.unique_id)

	def get_location(self):
		return Point(self.location.latitude,self.location.longitude)

	def get_distance(self,lat,lng):
		point1 = (self.location.latitude,self.location.longitude)
		point2 = (lat,lng)
		return "%.1f" % vincenty(point1, point2).miles

	def is_have_additional_detail(self):
		if self.auto_detail == None and self.housing_detail == None:
			return False

		result = False
		if self.auto_detail != None and self.auto_detail.is_empty() == False:
			result = True

		if self.housing_detail != None and self.housing_detail.is_empty() == False:
			result = True

		return result

	def get_total_photos(self):
		result = []
		if self.main_photo != None:
			result.append(self.main_photo)
			for photo in self.photos.all():
				if photo != None and photo.unique_id != self.main_photo.unique_id:
					result.append(photo)
		return result

	def get_expired_time(self):
		if self.post_status != ACTIVE_STATUS: return None
		if self.last_repost_time != None:
			return self.last_repost_time + relativedelta(days=self.duration)
		else:
			return self.create_time + relativedelta(days=self.duration)

	def is_expired(self):
		expired_time = self.get_expired_time()
		if expired_time != None and timezone.now() > expired_time:
			return True
		return False

	def get_display_category(self):
		return POST_CATEGORY_MAP[self.category]

	def get_display_subcategory(self):
		return POST_SUBCATEGORY_ELEMENT_MAP[self.subcategory]


class Company(models.Model):
	unique_id = models.CharField(max_length=100,primary_key=True,editable=False,default=get_unique_id)
	name = models.CharField(max_length=300)
	description = models.TextField(blank=True)
	url = models.URLField(blank=True)
	email_domain = models.CharField(max_length=100)
	posts = models.ManyToManyField("Post",blank=True,related_name="company_posts")
	company_type = models.CharField(max_length=100,blank=True,null=True)
	company_related_transaction_rating = models.DecimalField(max_digits=3,decimal_places=1,blank=True,null=True)
	is_verified = models.BooleanField(default=True)

	def __unicode__(self):
		return unicode(self.name)


class UserConnectionRequest(models.Model):
	unique_id = models.CharField(max_length=100,primary_key=True,editable=False,default=get_unique_id)
	request_user = models.ForeignKey(User,related_name="connection_request_user")
	approve_user = models.ForeignKey(User,related_name="connection_approve_user")
	status = models.CharField(max_length=10,choices=CONNECTION_REQUEST_STATUS,default='pending')
	connection_type = models.CharField(max_length=25,choices=CONNECTION_TYPE)
	message = models.TextField(blank=True)
	connection_relationship = models.CharField(max_length=50)
	request_time = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return unicode(self.unique_id)


class UserCredential(models.Model):
	unique_id = models.CharField(max_length=100,primary_key=True,editable=False,default=get_unique_id)
	credential_type = models.CharField(max_length=50,choices=CREDENTIAL_TYPE)
	credential_additional_info = models.CharField(max_length=100,blank=True)
	company = models.ForeignKey("Company",blank=True,null=True,related_name="user_credential_company",on_delete=models.SET_NULL)
	connection = models.ForeignKey("UserConnectionRequest",blank=True,null=True,related_name="user_credential_connection",on_delete=models.SET_NULL)

	def get_credential(self):
		return self.credential_type

	def get_display_credential(self):
		credential = self.get_credential()
		if credential == CREDENTIAL_PROFESSIONAL:
			additional_info = self.credential_additional_info if len(self.credential_additional_info) > 0 else "Employee"
			return self.company.name + " " + additional_info
		elif credential == CREDENTIAL_PROFESSIONAL_CONNECTION:
			relationship = self.connection.connection_relationship
			return relationship[0].upper() + relationship[1:].lower() + " of " + self.company.name + " Employee"
		else:
			return "Anonymous"

	def get_simple_display_credential(self):
		credential = self.get_credential()
		if credential == CREDENTIAL_PROFESSIONAL:
			return self.company.name + " Employee"
		elif credential == CREDENTIAL_PROFESSIONAL_CONNECTION:
			return self.company.name + " Connection"
		else:
			return "Anonymous"

	def __unicode__(self):
		return unicode(self.unique_id)


class UserProfile(models.Model):
	user = models.OneToOneField(User)

	email_type = models.CharField(max_length=20,choices=USER_EMAIL_TYPE)
	credential = models.ForeignKey("UserCredential",related_name="user_profile_credential",blank=True,null=True,on_delete=models.SET_NULL)
	initial = models.CharField(max_length=2)
	preferred_email = models.EmailField(blank=True,null=True)
	post_quota = models.IntegerField(default=DEFAULT_POST_QUOTA)
	watching_posts = models.ManyToManyField(Post,related_name="user_watching_posts",blank=True)

	def __unicode__(self):
		return unicode(self.user)

	def get_new_selling_messages_count(self):
		key = self.user.username + CACHE_KEY_SPECIAL_SEPARATOR + "message" + CACHE_KEY_SPECIAL_SEPARATOR + "selling"
		cache_count = cache.get(key)
		if cache_count == None:
			count = Conversation.objects.filter(latest_message__user_receive=self.user,latest_message__status=NEW,post__user_create=self.user).count()
			cache.set(key,count)
			return count
		else:
			return cache_count

	def get_new_buying_messages_count(self):
		key = self.user.username + CACHE_KEY_SPECIAL_SEPARATOR + "message" + CACHE_KEY_SPECIAL_SEPARATOR + "buying"
		cache_count = cache.get(key)
		if cache_count == None:
			count = Conversation.objects.filter(latest_message__user_receive=self.user,latest_message__status=NEW).exclude(post__user_create=self.user).count()
			cache.set(key,count)
			return count
		else:
			return cache_count

	def get_new_notifications_count(self):
		key = self.user.username + CACHE_KEY_SPECIAL_SEPARATOR + "notification"
		cache_count = cache.get(key)
		if cache_count == None:
			count = Notification.objects.filter(status=NEW,notify_to=self.user).count()
			cache.set(key,count)
			return count
		else:
			return cache_count

	def get_sending_email(self):
		if self.preferred_email != None:
			return self.preferred_email
		return self.user.email

	def get_notifications(self):
		notifications = []
		try:
			notifications = Notification.objects.filter(notify_to=self.user).order_by("-create_time").select_related("notify_to","notify_from")
		except Exception as e:
			logger.exception(e)
		return notifications

	def get_listing_posts(self):
		posts = []
		try:
			posts = Post.objects.filter(user_create=self.user,is_deleted=False).order_by("-create_time")
		except Exception as e:
			logger.exception(e)
		return posts

	def get_selling_posts_conversations(self):
		result = {}
		try:
			conversations = Conversation.objects.filter(Q(user1=self.user)|Q(user2=self.user))
			for conversation in conversations:
				if conversation.post.user_create == self.user:
					if (conversation.user1 == self.user and conversation.user1_messages.count() > 0) or (conversation.user2 == self.user and conversation.user2_messages.count() > 0):
						post = conversation.post
						if post in result:
							post_conversations = result[post]
							post_conversations.append(conversation)
							result[post] = post_conversations
						else:
							result[post] = [conversation]
		except Exception as e:
			logger.exception(e)
		return result

	def get_pending_connection_requests(self):
		return UserConnectionRequest.objects.filter(approve_user=self.user,status="pending")

	def get_buying_conversations(self):
		result = []
		try:
			conversations = Conversation.objects.filter(Q(user1=self.user)|Q(user2=self.user))
			for conversation in conversations:
				if conversation.post.user_create != self.user:
					if (conversation.user1 == self.user and conversation.user1_messages.count() > 0) or (conversation.user2 == self.user and conversation.user2_messages.count() > 0):
						result.append(conversation)
		except Exception as e:
			logger.exception(e)
		return result

	def is_watching_post(self,post):
		return post in self.watching_posts.filter(is_deleted=False)




@python_2_unicode_compatible
class EmailAddress(models.Model):

	user = models.ForeignKey(User,verbose_name=_('main_user'),related_name="user_email_address")
	email = models.EmailField(unique=True,verbose_name=_('e-mail address'))
	verified = models.BooleanField(verbose_name=_('verified'), default=False)
	primary = models.BooleanField(verbose_name=_('primary'), default=False)

	objects = EmailAddressManager()

	class Meta:
		verbose_name = _("email address")
		verbose_name_plural = _("email addresses")
		if not UNIQUE_EMAIL:
			unique_together = [("user", "email")]

	def __str__(self):
		return u"%s (%s)" % (self.email, self.user)

	def set_as_primary(self, conditional=False):
		old_primary = EmailAddress.objects.get_primary(self.user)
		if old_primary:
			if conditional:
				return False
			old_primary.primary = False
			old_primary.save()
		self.primary = True
		self.save()
		user_email(self.user, self.email)
		self.user.save()
		return True

	def send_confirmation(self, request, confirm_type, signup=False):
		confirmation = None
		try:
			confirmation = EmailConfirmation.objects.get(email_address=self)
		except EmailConfirmation.DoesNotExist:
			confirmation = EmailConfirmation.create(self)

		confirmation.send(request,confirm_type,signup=signup)
		return confirmation

	def change(self, request, new_email, confirm=True):
		"""
		Given a new email address, change self and re-confirm.
		"""
		with transaction.commit_on_success():
			user_email(self.user, new_email)
			self.user.save()
			self.email = new_email
			self.verified = False
			self.save()
			if confirm:
				self.send_confirmation(request)


@python_2_unicode_compatible
class EmailConfirmation(models.Model):

	email_address = models.ForeignKey(EmailAddress,verbose_name=_('e-mail address'))
	created = models.DateTimeField(verbose_name=_('created'),default=timezone.now)
	sent = models.DateTimeField(verbose_name=_('sent'), null=True)
	email_sent = models.ForeignKey(Email,related_name="email_sent_confirmation",blank=True,null=True,on_delete=models.SET_NULL)
	key = models.CharField(verbose_name=_('key'), max_length=64, unique=True)
	passcode = models.CharField(verbose_name=_('passcode'), max_length=5)

	objects = EmailConfirmationManager()

	class Meta:
		verbose_name = _("email confirmation")
		verbose_name_plural = _("email confirmations")

	def __str__(self):
		return u"confirmation for %s" % self.email_address

	@classmethod
	def create(cls, email_address):
		key = random_token([email_address.email])
		passcode = str(random.randrange(10000, 99999, 3))
		return cls._default_manager.create(email_address=email_address,
										   key=key,passcode=passcode)

	def key_expired(self):
		expiration_date = self.sent \
			+ datetime.timedelta(days=EMAIL_CONFIRMATION_EXPIRE_DAYS)
		return expiration_date <= timezone.now()
	key_expired.boolean = True

	def confirm(self, request):
		if not self.key_expired() and not self.email_address.verified:
			email_address = self.email_address
			email_address.verified = True
			email_address.set_as_primary(conditional=True)
			email_address.save()
			signals.email_confirmed.send(sender=self.__class__,
										 request=request,
										 email_address=email_address)
			return email_address

	def send(self, request, confirm_type, signup=False, **kwargs):
		context = {}
		email_template = None
		if confirm_type == PASSCODE:
			context_data = { "passcode": self.passcode }
			email_template = "confirmation_passcode_signup" if signup else "confirmation_passcode"
		elif confirm_type == ACTIVATE_LINK:
			activate_url = "/api/v1/auth/activate?confirm_type=activate_link&value=" + self.key + "?format=json"
			activate_url = request.build_absolute_uri(activate_url)
			context_data = {"activate_url": activate_url }
			email_template = "confirmation_link_signup" if signup else "confirmation_link"
		else:
			raise Exception(message=("Incorrect confirm type. Please choose either {passcode} or {activate_link}".format(passcode=PASSCODE,activate_link=ACTIVATE_LINK)))

		email = Email.objects.create(to_emails=self.email_address.email,email_template=email_template,context_data=str(context_data))

		self.email_sent = email
		self.sent = timezone.now()
		self.save()
