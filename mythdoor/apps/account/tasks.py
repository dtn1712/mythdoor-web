from celery import shared_task

from mythdoor.apps.main.models import SignupRequest, Email, Company
from mythdoor.apps.mailer.tasks import send_email
from mythdoor.apps.app_settings import PUBLIC_CONSUMER_EMAIL_DOMAIN
from mythdoor.apps.main.constants import COMPLETED_STATUS
from mythdoor.apps.company.service import CompanyService
from mythdoor.settings import WEBSITE_URL

import logging

logger = logging.getLogger(__name__)

@shared_task
def analyze_signup_request(unique_id,email_unique_id):
	signup_request = SignupRequest.objects.get(unique_id=unique_id)
	email = signup_request.email
	email_domain = None
	try:
		email_domain = email[email.index("@")+1:]
	except Exception as e:
		logger.exception(e)

	if email_domain != None:
		if email_domain not in PUBLIC_CONSUMER_EMAIL_DOMAIN and email_domain[len(email_domain)-3:] != "edu":

			# Check from database first
			companies = Company.objects.filter(email_domain=email_domain,is_verified=True)
			if len(companies) == 0:

				# If not, check from company service
				company_service = CompanyService()
				company = company_service.find_company_by_email_domain(email_domain)
				if company != None:
					company_service.save_company(company)
					signup_request.suggest_company = company
					signup_request.signup_email_type = "company"
				else:
					signup_request.signup_email_type = "company_pending"
			else:
				signup_request.suggest_company = companies[0]
				signup_request.signup_email_type = "company"
		else:
			signup_request.signup_email_type = "others"

		signup_request.save()
		send_email.delay(email_unique_id)
	else:
		context = { "signup_url": '%s%s' % (WEBSITE_URL, '/account/signup/') }
		email = Email.objects.create(to_emails=email,email_template='signup_request_incorrect_email',context_data=str(context))
		send_email.delay(email.unique_id)



