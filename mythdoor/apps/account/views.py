from django.http import HttpResponse, HttpResponseRedirect, HttpResponseServerError, Http404
from django.template import RequestContext
from django.shortcuts import render_to_response, get_object_or_404, redirect, render
from django.core.urlresolvers import reverse, reverse_lazy
from django.utils.http import base36_to_int
from django.views.generic import RedirectView
from django.views.generic.base import ContextMixin
from django.views.generic.base import TemplateResponseMixin, View, TemplateView
from django.views.generic.edit import FormView
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.tokens import default_token_generator
from django.utils.cache import patch_response_headers
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import (HttpResponseRedirect, Http404,
						 HttpResponsePermanentRedirect)
from django.views.decorators.debug import sensitive_post_parameters
from django.utils.decorators import method_decorator

from mythdoor.apps.account.utils import get_user_model, complete_signup
from mythdoor.apps.account.utils import get_login_redirect_url, get_next_redirect_url
from mythdoor.apps.account import signals
from mythdoor.apps.account import settings as app_settings

from mythdoor.apps.main.models import SignupRequest
from mythdoor.apps.app_helper import get_user_login_object, get_template_path
from mythdoor.apps.account.forms import SignupRequestForm, ResetPasswordForm, ResetPasswordKeyForm
from mythdoor.apps.account.forms import LoginForm, SignupCompleteForm, ChangePasswordForm, SetPasswordForm
from mythdoor.apps.app_views import AppBaseView

import logging, requests, json

User = get_user_model()

logger = logging.getLogger(__name__)

APP_NAME = "account"

sensitive_post_parameters_m = method_decorator(
	sensitive_post_parameters('password', 'password1', 'password2'))


def get_ajax_response(request, response, redirect_to=None, form=None):
		data = {}
		status = response.status_code

		if redirect_to:
			status = 200
			data['location'] = redirect_to
		if form:
			if form.is_valid():
				status = 200
			else:
				status = 400
				data['form_errors'] = form._errors
			if hasattr(response, 'render'):
				response.render()
			data['html'] = response.content.decode('utf8')
		return HttpResponse(json.dumps(data),
							status=status,
							content_type='application/json')


def _ajax_response(request, response, form=None):
	if request.is_ajax():
		if (isinstance(response, HttpResponseRedirect)
				or isinstance(response, HttpResponsePermanentRedirect)):
			redirect_to = response['Location']
		else:
			redirect_to = None
		response = get_ajax_response(request,response,form=form,redirect_to=redirect_to)
	return response


class RedirectAuthenticatedUserMixin(object):
	def dispatch(self, request, *args, **kwargs):
		# WORKAROUND: https://code.djangoproject.com/ticket/19316
		self.request = request
		# (end WORKAROUND)
		if request.user.is_authenticated() and \
				app_settings.AUTHENTICATED_LOGIN_REDIRECTS:
			redirect_to = self.get_authenticated_redirect_url()
			response = HttpResponseRedirect(redirect_to)
			return _ajax_response(request, response)
		else:
			response = super(RedirectAuthenticatedUserMixin,
							 self).dispatch(request,
											*args,
											**kwargs)
		return response

	def get_authenticated_redirect_url(self):
		redirect_field_name = self.redirect_field_name
		return get_login_redirect_url(self.request,
									  url=self.get_success_url(),
									  redirect_field_name=redirect_field_name)


class AjaxCapableProcessFormViewMixin(object):

	def post(self, request, *args, **kwargs):
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		if form.is_valid():
			response = self.form_valid(form)
		else:
			response = self.form_invalid(form)
		return _ajax_response(self.request, response, form=form)


class ResendConfirmEmailView(RedirectView):
	def get(self, request, *args, **kwargs):
		user_login = get_user_login_object(request)
		email = user_login.email
		try:
			email_address = EmailAddress.objects.get(
					user=request.user,
					email=email,
				)
			email_address.send_confirmation(request)
			return HttpResponseRedirect("/?action=resend_confirm_email&result=success&session_id=" + request.session.session_key)
		except EmailAddress.DoesNotExist:
			return HttpResponseRedirect("/?action=resend_confirm_email&result=error&session_id=" + request.session.session_key)

resend_confirm_email = ResendConfirmEmailView.as_view(permanent=False)


class LoginView(RedirectAuthenticatedUserMixin,
				AjaxCapableProcessFormViewMixin,
				FormView,AppBaseView):
	app_name = APP_NAME
	template_name = "main"
	form_class = LoginForm
	success_url = reverse_lazy("home")
	redirect_field_name = "next"

	def form_valid(self, form):
		success_url = self.get_success_url()
		return form.login(self.request, redirect_url=success_url)

	def get_success_url(self):
		# Explicitly passed ?next= URL takes precedence
		ret = (get_next_redirect_url(self.request,
									 self.redirect_field_name)
			   or self.success_url)
		return ret

	def get_context_data(self, **kwargs):
		context = super(LoginView, self).get_context_data(**kwargs)
		if "form" in context:
			context['login_form'] = context['form']
		else:
			context['login_form'] = LoginForm()
		context['signup_form'] = SignupRequestForm()
		return context

login = LoginView.as_view()

class SignupRequestView(RedirectAuthenticatedUserMixin, AppBaseView, FormView):
	app_name = APP_NAME
	template_name = "main"
	form_class = SignupRequestForm
	success_url = reverse_lazy("home")
	redirect_field_name = None

	def form_valid(self, form):
		form.save(self.request)
		self.template_name = "signup_activation"
		context = self.get_context_data()
		context['email'] = form.clean_email()
		return self.render_to_response(context)


	def get_context_data(self, **kwargs):
		context = super(SignupRequestView, self).get_context_data(**kwargs)
		context['login_form'] = LoginForm()
		if "form" in context:
			context['signup_form'] = context['form']
		else:
			context['signup_form'] = SignupRequestForm()
		return context

signup_request = SignupRequestView.as_view()


class SignupCompleteView(RedirectAuthenticatedUserMixin,AppBaseView,FormView):
	app_name = APP_NAME
	template_name = "signup_complete"
	form_class = SignupCompleteForm
	success_url = reverse_lazy("post_dashboard")
	redirect_field_name = None
	is_already_signup = False
	signup_request = None

	def dispatch(self, request, *args, **kwargs):
		try:
			confirmation_key = self.kwargs['confirmation_key']
			signup_request = SignupRequest.objects.get(confirmation_key=confirmation_key)
			self.signup_request = signup_request
			if signup_request.request_status == "completed":
				return HttpResponseRedirect("/")
			else:
				self.initial = { "request_id": signup_request.unique_id }
			return super(SignupCompleteView,self).dispatch(request,*args,**kwargs)
		except SignupRequest.DoesNotExist:
			return HttpResponseRedirect("/")

	def form_valid(self, form):
		user = form.save(self.request)
		return complete_signup(self.request, user,
							   app_settings.EMAIL_VERIFICATION,
							   self.get_success_url())

	def get_context_data(self, **kwargs):
		context = super(SignupCompleteView, self).get_context_data(**kwargs)
		context['signup_request'] = self.signup_request
		context['confirmation_key'] = self.kwargs['confirmation_key']
		return context

signup_complete = SignupCompleteView.as_view()


class PasswordChangeView(FormView,AppBaseView):
	app_name = APP_NAME
	template_name = "password_change"
	form_class = ChangePasswordForm
	success_url = reverse_lazy("login")

	def dispatch(self, request, *args, **kwargs):
		if not request.user.has_usable_password():
			return HttpResponseRedirect(reverse('account_set_password'))
		return super(PasswordChangeView, self).dispatch(request, *args,
														**kwargs)

	def get_form_kwargs(self):
		kwargs = super(PasswordChangeView, self).get_form_kwargs()
		kwargs["user"] = self.request.user
		return kwargs

	def form_valid(self, form):
		form.save()
		signals.password_changed.send(sender=self.request.user.__class__,
									  request=self.request,
									  user=self.request.user)
		return super(PasswordChangeView, self).form_valid(form)

password_change = login_required(PasswordChangeView.as_view())


class PasswordSetView(FormView,AppBaseView):
	app_name = APP_NAME
	template_name = "password_set"
	form_class = SetPasswordForm
	success_url = reverse_lazy("login")

	def dispatch(self, request, *args, **kwargs):
		if request.user.has_usable_password():
			return HttpResponseRedirect(reverse('account_change_password'))
		return super(PasswordSetView, self).dispatch(request, *args, **kwargs)

	def get_form_kwargs(self):
		kwargs = super(PasswordSetView, self).get_form_kwargs()
		kwargs["user"] = self.request.user
		return kwargs

	def form_valid(self, form):
		form.save()
		signals.password_set.send(sender=self.request.user.__class__,
								  request=self.request, user=self.request.user)
		return super(PasswordSetView, self).form_valid(form)

password_set = login_required(PasswordSetView.as_view())


class PasswordResetView(FormView,AppBaseView):
	app_name = APP_NAME
	template_name = "password_reset"
	form_class = ResetPasswordForm

	def form_valid(self, form):
		email = form.save()
		user = User.objects.get(email=email)
		self.success_url = reverse_lazy("reset_password_done") + "?user=" + user.username
		return super(PasswordResetView, self).form_valid(form)

password_reset = PasswordResetView.as_view()


class PasswordResetDoneView(TemplateView,AppBaseView):
	app_name = APP_NAME
	template_name = "password_reset_done"

	def get_context_data(self, **kwargs):
		context = super(PasswordResetDoneView, self).get_context_data(**kwargs)
		username = self.request.GET.get("user",None)
		if username != None:
			try:
				user = User.objects.get(username=username)
				context['email'] = user.email
			except User.DoesNotExist:
				pass
		return context

password_reset_done = PasswordResetDoneView.as_view()


class PasswordResetFromKeyView(FormView,AppBaseView):
	app_name = APP_NAME
	template_name = "password_reset_from_key"
	form_class = ResetPasswordKeyForm
	token_generator = default_token_generator

	def _get_user(self, uidb36):
		# pull out user
		try:
			uid_int = base36_to_int(uidb36)
		except ValueError:
			raise Http404
		return get_object_or_404(User, id=uid_int)

	def dispatch(self, request, uidb36, key, **kwargs):
		self.request = request
		self.uidb36 = uidb36
		self.key = key
		self.reset_user = self._get_user(uidb36)

		if not self.token_generator.check_token(self.reset_user, key):
			return HttpResponseRedirect("/")
		else:
			return super(PasswordResetFromKeyView, self).dispatch(request,
																  uidb36,
																  key,
																  **kwargs)

	def get_form_kwargs(self):
		kwargs = super(PasswordResetFromKeyView, self).get_form_kwargs()
		kwargs["user"] = self.reset_user
		kwargs["temp_key"] = self.key
		return kwargs

	def form_valid(self, form):
		form.save()
		signals.password_reset.send(sender=self.reset_user.__class__,
									request=self.request,
									user=self.reset_user)
		return super(PasswordResetFromKeyView, self).form_valid(form)

	def get_success_url(self):
		return "/account/login?action=password_reset&result=success&show=1" 


password_reset_from_key = PasswordResetFromKeyView.as_view()


class LogoutView(TemplateResponseMixin, View):

	redirect_url = "/"
	redirect_field_name = "next"

	def get(self, *args, **kwargs):
		if app_settings.LOGOUT_ON_GET:
			return self.post(*args, **kwargs)
		if not self.request.user.is_authenticated():
			return redirect(self.get_redirect_url())
		ctx = self.get_context_data()
		return self.render_to_response(ctx)

	def post(self, *args, **kwargs):
		url = self.get_redirect_url()
		if self.request.user.is_authenticated():
			self.logout()
		return redirect(url)

	def logout(self):
		auth_logout(self.request)

	def get_context_data(self, **kwargs):
		ctx = kwargs
		redirect_field_value = self.request.REQUEST \
			.get(self.redirect_field_name)
		ctx.update({
			"redirect_field_name": self.redirect_field_name,
			"redirect_field_value": redirect_field_value})
		return ctx

	def get_redirect_url(self):
		return self.redirect_url

logout = LogoutView.as_view()



