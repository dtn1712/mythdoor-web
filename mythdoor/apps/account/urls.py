from django.conf.urls import *
from django.views.generic import RedirectView

from mythdoor.apps.account import views
from mythdoor.apps.account import forms

urlpatterns = [
    url(r"^signup$", views.signup_request, name="signup"),
    url(r"^signup/$",RedirectView.as_view(url='/accounts/signup',permanent=False)),
    url(r"^signup/(?P<confirmation_key>\w+)$", views.signup_complete,name="signup_complete"),

    url(r"^login$", views.login, name="login"),
    url(r"^login/$",RedirectView.as_view(url='/accounts/login',permanent=False)),

    url(r"^logout$", views.logout, name="logout"),
    url(r"^logout/$",RedirectView.as_view(url='/accounts/logout',permanent=False)),

    url(r"^password/change$", views.password_change,name="change_password"),
    url(r"^password/set$", views.password_set, name="set_password"),

    url(r"^password/reset$", views.password_reset,name="reset_password"),
    url(r"^password/reset/$", RedirectView.as_view(url='/accounts/password/reset',permanent=False)),
    url(r"^password/reset/done$", views.password_reset_done,name="reset_password_done"),
    url(r"^password/reset/key/(?P<uidb36>[0-9A-Za-z]+)-(?P<key>.+)$",views.password_reset_from_key,name="reset_password_from_key"),
]
