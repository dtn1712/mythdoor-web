import re, unicodedata, json

from django.db.models import Q
from django.core.exceptions import ImproperlyConfigured
from django.core.validators import validate_email, ValidationError
from django.db.models import FieldDoesNotExist
from django.db.models.fields import (DateTimeField, DateField,
									 EmailField, TimeField)
from django.utils import six
try:
	from django.utils.encoding import force_text
except ImportError:
	from django.utils.encoding import force_unicode as force_text

from mythdoor.apps.main.models import UserProfile
from mythdoor.apps.account import settings as account_settings
from mythdoor.apps import app_settings


def _generate_unique_username_base(txts):
	username = None
	for txt in txts:
		username = unicodedata.normalize('NFKD', force_text(txt))
		username = username.encode('ascii', 'ignore').decode('ascii')
		username = force_text(re.sub('[^\w\s@+.-]', '', username).lower())
		# Django allows for '@' in usernames in order to accomodate for
		# project wanting to use e-mail for username. In allauth we don't
		# use this, we already have a proper place for putting e-mail
		# addresses (EmailAddress), so let's not use the full e-mail
		# address and only take the part leading up to the '@'.
		username = username.split('@')[0]
		username = username.strip()
		if username:
			break
	return username or 'user'


def generate_unique_username(txts):
	from mythdoor.apps.account.settings import USER_MODEL_USERNAME_FIELD
	username = _generate_unique_username_base(txts)
	User = get_user_model()
	try:
		max_length = User._meta.get_field(USER_MODEL_USERNAME_FIELD).max_length
	except FieldDoesNotExist:
		raise ImproperlyConfigured(
			"USER_MODEL_USERNAME_FIELD does not exist in user-model"
		)
	i = 0
	while True:
		try:
			if i:
				pfx = str(i + 1)
			else:
				pfx = ''
			ret = username[0:max_length - len(pfx)] + pfx
			query = {USER_MODEL_USERNAME_FIELD + '__iexact': ret}
			User.objects.get(**query)
			i += 1
		except User.DoesNotExist:
			return ret


def valid_email_or_none(email):
	ret = None
	try:
		if email:
			validate_email(email)
			if len(email) <= EmailField().max_length:
				ret = email
	except ValidationError:
		pass
	return ret


def email_address_exists(email, exclude_user=None):
	users = get_user_model().objects
	if exclude_user:
		users = users.exclude(pk=exclude_user.pk)

	return users.filter(Q(email=email)|Q(userprofile__preferred_email=email)).exists()


def import_attribute(path):
	assert isinstance(path, six.string_types)
	pkg, attr = path.rsplit('.', 1)
	ret = getattr(importlib.import_module(pkg), attr)
	return ret


def import_callable(path_or_callable):
	if not hasattr(path_or_callable, '__call__'):
		ret = import_attribute(path_or_callable)
	else:
		ret = path_or_callable
	return ret


def get_user_model():
	from django.apps import apps

	try:
		app_label, model_name = app_settings.USER_MODEL.split('.')
	except ValueError:
		raise ImproperlyConfigured("AUTH_USER_MODEL must be of the"
								   " form 'app_label.model_name'")
	user_model = apps.get_model(app_label, model_name)
	if user_model is None:
		raise ImproperlyConfigured("AUTH_USER_MODEL refers to model"
								   " '%s' that has not been installed"
								   % app_settings.USER_MODEL)
	return user_model

def get_request_param(request, param, default=None):
	return request.POST.get(param) or request.GET.get(param, default)



import hashlib, random

from datetime import timedelta
try:
	from django.utils.timezone import now
except ImportError:
	from datetime import datetime
	now = datetime.now

from django.contrib import messages
from django.core.urlresolvers import reverse
from django.shortcuts import render
from django.conf import settings as django_settings
from django.contrib.auth import login
from django.http import HttpResponseRedirect
from django.utils.http import urlencode

from mythdoor.apps.account import signals
from mythdoor.apps.account.settings import EmailVerificationMethod


def get_next_redirect_url(request, redirect_field_name="next"):
	"""
	Returns the next URL to redirect to, if it was explicitly passed
	via the request.
	"""
	redirect_to = get_request_param(request, redirect_field_name)
	# if not get_adapter().is_safe_url(redirect_to):
	#     redirect_to = None
	return redirect_to


def get_login_redirect_url(request, url=None, redirect_field_name="next"):
	if url and callable(url):
		# In order to be able to pass url getters around that depend
		# on e.g. the authenticated state.
		url = url()
	redirect_url = (url or get_next_redirect_url(request,redirect_field_name=redirect_field_name))
	return redirect_url

_user_display_callable = None


def default_user_display(user):
	if account_settings.USER_MODEL_USERNAME_FIELD:
		return getattr(user, account_settings.USER_MODEL_USERNAME_FIELD)
	else:
		return force_text(user)


def user_display(user):
	global _user_display_callable
	if not _user_display_callable:
		f = getattr(django_settings, "ACCOUNT_USER_DISPLAY",
					default_user_display)
		_user_display_callable = import_callable(f)
	return _user_display_callable(user)


def user_field(user, field, *args):
	"""
	Gets or sets (optional) user model fields. No-op if fields do not exist.
	"""
	if field and hasattr(user, field):
		if args:
			# Setter
			v = args[0]
			if v:
				User = get_user_model()
				v = v[0:User._meta.get_field(field).max_length]
			setattr(user, field, v)
		else:
			# Getter
			return getattr(user, field)


def user_username(user, *args):
	return user_field(user, account_settings.USER_MODEL_USERNAME_FIELD, *args)


def user_email(user, *args):
	return user_field(user, account_settings.USER_MODEL_EMAIL_FIELD, *args)


def perform_login(request, user, email_verification,
				  redirect_url=None, signal_kwargs={},
				  signup=False):
	"""
	Keyword arguments:

	signup -- Indicates whether or not sending the
	email is essential (during signup), or if it can be skipped (e.g. in
	case email verification is optional and we are only logging in).
	"""
	from mythdoor.apps.main.models import EmailAddress
	has_verified_email = EmailAddress.objects.filter(user=user,
													 verified=True).exists()
	if email_verification == EmailVerificationMethod.NONE:
		pass
	elif email_verification == EmailVerificationMethod.OPTIONAL:
		# In case of OPTIONAL verification: send on signup.
		if not has_verified_email and signup:
			send_email_confirmation(request, user, signup=signup)
	elif email_verification == EmailVerificationMethod.MANDATORY:
		if not has_verified_email:
			send_email_confirmation(request, user, signup=signup)
			return HttpResponseRedirect(
				reverse('account_email_verification_sent'))
	# Local users are stopped due to form validation checking
	# is_active, yet, adapter methods could toy with is_active in a
	# `user_signed_up` signal. Furthermore, social users should be
	# stopped anyway.
	if not user.is_active:
		return HttpResponseRedirect(reverse('account_inactive'))
	# HACK: This may not be nice. The proper Django way is to use an
	# authentication backend, but I fail to see any added benefit
	# whereas I do see the downsides (having to bother the integrator
	# to set up authentication backends in settings.py
	if not hasattr(user, 'backend'):
		user.backend = "mythdoor.apps.account.backends.AuthenticationBackend"
	signals.user_logged_in.send(sender=user.__class__,
								request=request,
								user=user,
								**signal_kwargs)
	login(request, user)

	return HttpResponseRedirect(get_login_redirect_url(request, redirect_url))


def complete_signup(request, user, email_verification, success_url,
					signal_kwargs={}):
	signals.user_signed_up.send(sender=user.__class__,
								request=request,
								user=user,
								**signal_kwargs)
	return perform_login(request, user,
						 email_verification=email_verification,
						 signup=True,
						 redirect_url=success_url,
						 signal_kwargs=signal_kwargs)



def send_email_confirmation(request, user, signup=False):
	"""
	E-mail verification mails are sent:
	a) Explicitly: when a user signs up
	b) Implicitly: when a user attempts to log in using an unverified
	e-mail while EMAIL_VERIFICATION is mandatory.

	Especially in case of b), we want to limit the number of mails
	sent (consider a user retrying a few times), which is why there is
	a cooldown period before sending a new mail.
	"""
	from mythdoor.apps.main.models import EmailAddress, EmailConfirmation

	COOLDOWN_PERIOD = timedelta(minutes=3)
	email = user_email(user)
	if email:
		try:
			email_address = EmailAddress.objects.get_for_user(user, email)
			if not email_address.verified:
				send_email = not EmailConfirmation.objects \
					.filter(sent__gt=now() - COOLDOWN_PERIOD,
							email_address=email_address) \
					.exists()
				if send_email:
					email_address.send_confirmation(request,
													signup=signup)
			else:
				send_email = False
		except EmailAddress.DoesNotExist:
			send_email = True
			email_address = EmailAddress.objects.add_email(request,
														   user,
														   email,
														   signup=signup,
														   confirm=True)
			assert email_address
		# At this point, if we were supposed to send an email we have sent it.
		# if send_email:
		#     get_adapter().add_message(request,
		#                               messages.INFO,
		#                               'account/messages/'
		#                               'email_confirmation_sent.txt',
		#                               {'email': email})
	if signup:
		request.session['account_user'] = user.pk


def filter_users_by_email(email):
	"""Return list of users by email address

	Typically one, at most just a few in length.  First we look through
	EmailAddress table, than customisable User model table. Add results
	together avoiding SQL joins and deduplicate.
	"""
	from mythdoor.apps.main.models import EmailAddress
	User = get_user_model()
	mails = EmailAddress.objects.filter(email__iexact=email)
	users = [e.user for e in mails.prefetch_related('user')]
	if account_settings.USER_MODEL_EMAIL_FIELD:
		q_dict = {account_settings.USER_MODEL_EMAIL_FIELD + '__iexact': email}
		users += list(User.objects.filter(**q_dict))
	return list(set(users))


def random_token(extra=None, hash_func=hashlib.sha256):
	if extra is None:
		extra = []
	bits = extra + [str(random.SystemRandom().getrandbits(512))]
	return hash_func("".join(bits).encode('utf-8')).hexdigest()
