from django import forms
from django.contrib.auth.models import User
from django.shortcuts import render_to_response, get_object_or_404, redirect, render
from django.utils.translation import pgettext, ugettext_lazy as _, ugettext
from django.contrib.auth.tokens import default_token_generator as token_generator
from django.core.urlresolvers import reverse
from django.utils.http import int_to_base36
from django.contrib.auth import authenticate
from django.db.models import Q
from django.template.loader import render_to_string

from mythdoor.apps.member.helper import get_user_common_info
from mythdoor.apps.account.utils import perform_login, random_token
from mythdoor.apps.main.models import SignupRequest, Email, UserProfile
from mythdoor.apps.main.models import UserCredential, UserConnectionRequest, Company, Notification
from mythdoor.apps.account.tasks import analyze_signup_request
from mythdoor.apps.main.constants import EMPTY_STRING, CREDENTIAL_PROFESSIONAL, CREDENTIAL_OTHERS
from mythdoor.apps.main.constants import COMPLETED_STATUS, TRUE, FALSE
from mythdoor.apps.app_helper import check_key_not_blank
from mythdoor.apps.mailer.tasks import send_email
from mythdoor.settings import WEBSITE_URL
from mythdoor.apps.app_settings import NOTIFICATION_SNIPPET_TEMPLATE
from mythdoor.apps.account import settings as app_settings
from mythdoor.apps.account.utils import email_address_exists, generate_unique_username, valid_email_or_none

import logging

logger = logging.getLogger(__name__)

class UserForm(forms.Form):

    def __init__(self, user=None, *args, **kwargs):
        self.user = user
        super(UserForm, self).__init__(*args, **kwargs)


class SetPasswordField(forms.CharField):

	def clean(self, value):
		value = super(SetPasswordField, self).clean(value)
		min_length = app_settings.PASSWORD_MIN_LENGTH
		if len(value) < min_length:
			raise forms.ValidationError(_("Password must be a minimum of {0} "
										  "characters.").format(min_length))
		return value


class LoginForm(forms.Form):

	email = forms.EmailField(label=_("Email"),max_length=60,widget=forms.TextInput(attrs={'placeholder': 'Email','class': "form-control"}))
	password = forms.CharField(label=_("Password"),widget=forms.PasswordInput(attrs={'placeholder':"Password", "class": "form-control"}))

	def user_credentials(self):
		"""
		Provides the credentials required to authenticate the user for
		login.
		"""
		credentials = {
		  "email": self.cleaned_data["email"],
		  "password": self.cleaned_data["password"]
		}
		return credentials

	def clean(self):
		if self._errors:
			return
		user = authenticate(**self.user_credentials())
		if user:
			if user.is_active:
				self.user = user
			else:
				raise forms.ValidationError(_("This account is currently"
											  " inactive."))
		else:
			error = _("The e-mail address and/or password you specified"
						  " are not correct.")
			raise forms.ValidationError(error)
		return self.cleaned_data

	def login(self, request, redirect_url=None):
		ret = perform_login(request, self.user,
							email_verification=app_settings.EMAIL_VERIFICATION,
							redirect_url=redirect_url)
		request.session.set_expiry(60 * 60 * 24 * 7 * 3)
		return ret


class SignupRequestForm(forms.Form):
	email = forms.EmailField(label=_("Email"),max_length=60,widget=forms.TextInput(attrs={'placeholder': 'Email','class': "form-control"}))

	def clean_email(self):
		value = self.cleaned_data["email"]
		if valid_email_or_none(value) == None:
			raise forms.ValidationError(_("Invalid email"))
		elif email_address_exists(value):
			raise forms.ValidationError(_("A user is already registered"
									  " with this e-mail address."))
		return value

	def save(self, request):
		email_address = self.cleaned_data['email']
		signup_request = None
		signup_requests = SignupRequest.objects.filter(email=email_address,request_status="pending")
		confirmation_key = random_token([email_address])
		if len(signup_requests) == 0:
			signup_request = SignupRequest.objects.create(
				email=email_address,
				confirmation_key=confirmation_key
			)
		else:
			signup_request = signup_requests[0]
			confirmation_key = signup_request.confirmation_key
			if len(signup_requests) > 1:
				SignupRequest.objects.exclude(unique_id=signup_request.unique_id).delete()

		activate_url = '%s%s%s' % (WEBSITE_URL, '/account/signup/', confirmation_key)
		context = {"activate_url": activate_url}
		email = Email.objects.create(to_emails=email_address,email_template='signup_request_activation',context_data=str(context))
		analyze_signup_request.delay(signup_request.unique_id,email.unique_id)


class SignupCompleteForm(forms.Form):

	request_id = forms.CharField(required=False, max_length=100, widget=forms.HiddenInput())
	is_company_email = forms.CharField(required=False)
	company = forms.CharField(label=_("Company"),required=False,widget=forms.TextInput(attrs={'class': "form-control","required":"required"}))
	occupation = forms.CharField(label=_("Occupation"),required=False,widget=forms.TextInput(attrs={'class': "form-control"}))
	password1 = SetPasswordField(label=_("Password*"),widget=forms.PasswordInput(attrs={'placeholder':"Password", "class": "form-control","required":"required"}))
	password2 = forms.CharField(label=_("Confirm Password*"),widget=forms.PasswordInput(attrs={'placeholder': 'Confirm Password','class': "form-control","required":"required"}))
	request_approve_email = forms.CharField(label=_("Approver Email"),required=False,widget=forms.TextInput(attrs={'class': "form-control"}))
	connection_message = forms.CharField(label=_("Message"),required=False,widget=forms.Textarea(attrs={'class': "form-control","rows":"3"}))
	connection_relationship = forms.CharField(label=_("Comnection Relationship"),required=False,widget=forms.TextInput(attrs={'placeholder': 'Friend/Parent/Children','class': "form-control"}))
	name_initial = forms.CharField(label=_("Initial*"),max_length=2,widget=forms.TextInput(attrs={'placeholder': 'Your initial (maximum 2 character)','class': "form-control", "required": "required"}))
	preferred_email = forms.CharField(label=_("Preferred Email"),required=False,widget=forms.TextInput(attrs={'class': "form-control"}))

	def clean(self):
		super(SignupCompleteForm, self).clean()
		if "password1" in self.cleaned_data and "password2" in self.cleaned_data:
			if self.cleaned_data["password1"] != self.cleaned_data["password2"]:
				raise forms.ValidationError(_("You must type the same password each time."))

		if "is_company_email" in self.cleaned_data and self.cleaned_data['is_company_email'] == TRUE:
			if check_key_not_blank(self.cleaned_data,"company") == False:
				raise forms.ValidationError(_("You must provide company name if your email is company email"))
		return self.cleaned_data

	def clean_request_approve_email(self):
		if check_key_not_blank(self.cleaned_data,"request_approve_email"):
			value = self.cleaned_data["request_approve_email"]
			exist_users = UserProfile.objects.filter(user__email=value).exclude(email_type="normal")
			if len(exist_users) == 0:
				raise forms.ValidationError(_("There is no authorized user with that email. Please try another one"))
			return value
		return EMPTY_STRING

	def clean_preferred_email(self):
		if check_key_not_blank(self.cleaned_data,"preferred_email"):
			value = self.cleaned_data["preferred_email"]
			if valid_email_or_none(value) == None:
				raise forms.ValidationError(_("Invalid email"))
			elif email_address_exists(value):
				raise forms.ValidationError(_("A user already used this preferred email"))
			return value
		return EMPTY_STRING

	def clean_connection_relationship(self):
		if check_key_not_blank(self.cleaned_data,"request_approve_email"):
			if check_key_not_blank(self.cleaned_data,"connection_relationship") == False:
				raise forms.ValidationError(_("You have to specify your relationship with your connection if you request the approval"))
			return self.cleaned_data['connection_relationship']
		return EMPTY_STRING

	def save(self, request):
		data = self.cleaned_data
		try:
			signup_request = get_object_or_404(SignupRequest,unique_id=data['request_id'])
			email = signup_request.email
			email_type = signup_request.signup_email_type
			username = generate_unique_username([email,"user"])
			user = User.objects.create(username=username,email=email)
			user.set_password(data['password1'])
			credential = UserCredential()
			if email_type == "company":
				credential.company = signup_request.suggest_company
				credential.credential_type = CREDENTIAL_PROFESSIONAL
				if check_key_not_blank(data,"occupation"):
					credential.credential_additional_info = data['occupation']
			else:
				if "is_company_email" in data and data['is_company_email'] == TRUE:
					signup_request.signup_email_type = "company_pending"
					company_name = data['company']
					email_domain = signup_request.email[signup_request.email.index("@")+1:]
					company = Company.objects.create(name=company_name,email_domain=email_domain,is_verified=False)
					credential.company = company
					credential.credential_type = CREDENTIAL_OTHERS
					if check_key_not_blank(data,"occupation"):
						credential.credential_additional_info = data['occupation']
				else:
					signup_request.signup_email_type = "others"
					credential.credential_type = CREDENTIAL_OTHERS
					if check_key_not_blank(data,"request_approve_email"):
						request_approve_email = data['request_approve_email']
						try:
							approve_user = User.objects.get(email=request_approve_email)
							connection_relationship = data['connection_relationship']
							message = data['connection_message'] if check_key_not_blank(data,"connection_message") else EMPTY_STRING
							if approve_user.userprofile.email_type == "company":
								UserConnectionRequest.objects.create(request_user=user,connection_relationship=connection_relationship,
															approve_user=approve_user,connection_type="professional",message=message)

								context = {
									"request_user_email": user.email,
									"connection_relationship": connection_relationship,
									"message": message,
									"view_connection_url": WEBSITE_URL + '/user/dashboard?tab=account_credential',
								}
								email = Email.objects.create(to_emails=approve_user.email,email_template="send_connection_request",context_data=str(context))
								send_email.delay(email.unique_id)

								Notification.objects.create(content=render_to_string(NOTIFICATION_SNIPPET_TEMPLATE['new_connection_request'],context),
									notification_type="connection_request",notify_from=user,notify_to=approve_user)
						except User.DoesNotExist:
							pass

			credential.save()
			
			user.userprofile.initial = data['name_initial'].upper()
			if check_key_not_blank(data,"preferred_email"):
				user.userprofile.preferred_email = data['preferred_email']

			user.userprofile.credential = credential
			user.userprofile.email_type = email_type
			user.userprofile.save()
			user.save()
			signup_request.request_status = COMPLETED_STATUS
			signup_request.save()
			return user
		except Exception as e:
			logger.exception(e)
			raise

class ResetPasswordForm(forms.Form):

	email = forms.EmailField(label=_("Email"),max_length=60,widget=forms.TextInput(attrs={'placeholder': 'Email','class': "form-control"}))

	def clean_email(self):
		email = self.cleaned_data["email"]
		self.users = User.objects.filter(Q(email__iexact=email)
								| Q(user_email_address__email__iexact=email)).distinct()
		if not self.users.exists():
			raise forms.ValidationError(_("The e-mail address is not assigned"
										  " to any user account"))
		return self.cleaned_data["email"]

	def save(self, **kwargs):

		email = self.cleaned_data["email"]

		for user in self.users:

			temp_key = token_generator.make_token(user)

			# save it to the password reset model
			# password_reset = PasswordReset(user=user, temp_key=temp_key)
			# password_reset.save()

			# send the password reset email
			path = reverse("reset_password_from_key",kwargs=dict(uidb36=int_to_base36(user.id),key=temp_key))

			url = '%s%s' % (WEBSITE_URL,path)
			context = {"password_reset_url": url}
			email = Email.objects.create(to_emails=email,email_template='password_reset',context_data=str(context))
			send_email.delay(email.unique_id)
		return self.cleaned_data["email"]

class ResetPasswordKeyForm(forms.Form):

	password1 = SetPasswordField(label=_("New Password"),widget=forms.PasswordInput(attrs={'placeholder':"Password", "class": "form-control"}))
	password2 = forms.CharField(label=_("Confirm Password"),widget=forms.PasswordInput(attrs={'placeholder':"Confirm Password", "class": "form-control"}))

	def __init__(self, *args, **kwargs):
		self.user = kwargs.pop("user", None)
		self.temp_key = kwargs.pop("temp_key", None)
		super(ResetPasswordKeyForm, self).__init__(*args, **kwargs)

	# FIXME: Inspecting other fields -> should be put in def clean(self) ?
	def clean_password2(self):
		if ("password1" in self.cleaned_data
				and "password2" in self.cleaned_data):
			if (self.cleaned_data["password1"]
					!= self.cleaned_data["password2"]):
				raise forms.ValidationError(_("You must type the same"
											  " password each time."))
		return self.cleaned_data["password2"]

	def save(self):
		# set the new user password
		user = self.user
		user.set_password(self.cleaned_data["password1"])
		user.save()


class ChangePasswordForm(UserForm):

    oldpassword = forms.CharField(label=_("Current Password"),widget=forms.PasswordInput(attrs={'placeholder':"Confirm Password", "class": "form-control"}))
    password1 = SetPasswordField(label=_("New Password"),widget=forms.PasswordInput(attrs={'placeholder':"Password", "class": "form-control"}))
    password2 = forms.CharField(label=_("Confirm Password"),widget=forms.PasswordInput(attrs={'placeholder':"Confirm Password", "class": "form-control"}))

    def clean_oldpassword(self):
        if not self.user.check_password(self.cleaned_data.get("oldpassword")):
            raise forms.ValidationError(_("Please type your current"
                                          " password."))
        return self.cleaned_data["oldpassword"]

    def clean_password2(self):
        if ("password1" in self.cleaned_data
                and "password2" in self.cleaned_data):
            if (self.cleaned_data["password1"]
                    != self.cleaned_data["password2"]):
                raise forms.ValidationError(_("You must type the same password"
                                              " each time."))
        return self.cleaned_data["password2"]

    def save(self):
        self.user.set_password(self.cleaned_data["password1"])
        self.user.save()


class SetPasswordForm(UserForm):

    password1 = SetPasswordField(label=_("Password"),widget=forms.PasswordInput(attrs={'placeholder':"Password", "class": "form-control"}))
    password2 = forms.CharField(label=_("Confirm Password"),widget=forms.PasswordInput(attrs={'placeholder':"Confirm Password", "class": "form-control"}))

    def clean_password2(self):
        if ("password1" in self.cleaned_data
                and "password2" in self.cleaned_data):
            if (self.cleaned_data["password1"]
                    != self.cleaned_data["password2"]):
                raise forms.ValidationError(_("You must type the same password"
                                              " each time."))
        return self.cleaned_data["password2"]

    def save(self):
        self.user.set_password(self.cleaned_data["password1"])
        self.user.save()

