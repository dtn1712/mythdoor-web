from celery import shared_task

from mythdoor.apps.main.constants import NEW, OLD


@shared_task
def clear_message_status(conversation,user_login):
	if conversation.user1 == user_login:
		conversation.user1_messages.filter(status=NEW).exclude(user_send=user_login).update(status=OLD)
	else:
		conversation.user2_messages.filter(status=NEW).exclude(user_send=user_login).update(status=OLD)
