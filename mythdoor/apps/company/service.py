import clearbit

from mythdoor.settings import CLEARBIT_API_KEY
from mythdoor.settings import CLEARBIT_API_COMPANY_VERSION
from mythdoor.settings import CLEARBIT_API_PERSON_VERSION

from mythdoor.apps.main.models import Company

class CompanyService():

	def __init__(self):
		self.clearbit_client = clearbit
		self.clearbit_client.key = CLEARBIT_API_KEY
		self.clearbit_client.Company.version = CLEARBIT_API_COMPANY_VERSION
		self.clearbit_client.Person.version = CLEARBIT_API_PERSON_VERSION

	def find_company_by_email_domain(self,email_domain):
		if email_domain == "yahoo.com": return None
		company = self.clearbit_client.Company.find(domain=email_domain)
		print company
		if company != None:
			result = Company(name=company['name'],email_domain=email_domain,
				description=company['description'],url=company['url'],
				company_type=company['category']['subIndustry'])
			return result
		return None

	def save_company(self,company):
		company.save()
