import json, logging

from mythdoor.apps.main.constants import CACHE_KEY_SPECIAL_SEPARATOR

logger = logging.getLogger(__name__)

def get_company_common_info(company):
	try:
		data = {
			"unique_id": company.unique_id,
			"name": company.name,
			"email_domain": company.email_domain
		}
		return data
	except Exception as e:
		logger.exception(e)
	return None

def get_company_common_info_json(company):
	data = get_company_common_info(user)
	if data == None:
		return json.dumps({})
	else:
		return json.dumps(data)


def generate_company_cache_key(company):
	value = company.name.lower().rstrip()
	if len(value) < 50:
		return str(company.unique_id) + CACHE_KEY_SPECIAL_SEPARATOR + "company" + CACHE_KEY_SPECIAL_SEPARATOR + str(value.replace(" ","_"))
	else:
		return str(company.unique_id) + CACHE_KEY_SPECIAL_SEPARATOR + "company" + CACHE_KEY_SPECIAL_SEPARATOR + str(value[:50].replace(" ","_"))
