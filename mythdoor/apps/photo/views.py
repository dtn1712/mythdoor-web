from django.http import HttpResponse, HttpResponseRedirect, HttpResponseBadRequest
from django.http import HttpResponseServerError, Http404, HttpResponseForbidden
from django.template import RequestContext
from django.shortcuts import render_to_response, get_object_or_404, get_list_or_404
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User, AnonymousUser
from django.utils import timezone
from django.core import serializers
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.core.urlresolvers import reverse, reverse_lazy
from django.views.generic import CreateView, View

from mythdoor.apps.app_helper import get_user_login_object, generate_unique_id
from mythdoor.apps.photo.helper import get_scaling_image_size
from mythdoor.apps.main.models import Photo
from mythdoor.apps.photo.tasks import generate_photo_aliases

import json, logging, datetime, cloudinary

logger = logging.getLogger(__name__)

APP_NAME = "photo"

class UploadPhotoView(View):

	@method_decorator(login_required)
	def dispatch(self, *args, **kwargs):
		return super(UploadPhotoView, self).dispatch(*args, **kwargs)

	def get(self, request, *args, **kwargs):
		return HttpResponseRedirect("/")

	def post(self, request, *args, **kwargs):
		user_login = get_user_login_object(request)

		if user_login == None:
			return HttpResponseForbidden()

		if request.FILES == None:
			return HttpResponseBadRequest('Must have files attached!')

		print request.POST

		result = {}
		image = request.FILES['image']
		width, height = get_scaling_image_size(image)
		photo = Photo(name=image.name,width=width,height=height,unique_id=generate_unique_id())
		try:
			rotation_angle = 0 if "rotation_angle" not in request.POST or len(request.POST['rotation_angle']) == 0 else int(request.POST['rotation_angle'])
			upload_result = cloudinary.uploader.upload(image,public_id=photo.unique_id,width=width,height=height,format="jpg",quality=70,angle=rotation_angle)
			photo.image_url = upload_result['url']
			photo.image_secure_url = upload_result['secure_url']
			result['unique_id'] = photo.unique_id
			result['url'] = upload_result['url']
			result['secure_url'] = upload_result['secure_url']
			result['success'] = True
			generate_photo_aliases.delay(photo.unique_id)
		except Exception as e:
			logger.exception(e)
			photo.upload_success = False
			result['success'] = False
			result['error_message'] = str(e)

		photo.save()

		return HttpResponse(json.dumps(result,indent=2), content_type='application/json')

