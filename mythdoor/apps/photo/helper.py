from mythdoor.settings import MAX_IMAGE_WIDTH, MAX_IMAGE_HEIGHT
from mythdoor.settings import DEFAULT_IMAGE_WIDTH, DEFAULT_IMAGE_HEIGHT

from PIL import Image

import logging

logger = logging.getLogger(__name__)

def get_scaling_image_size(image):
	try:
		im = Image.open(image)
		width = im.size[0]
		height = im.size[1]
		if width > height:
			if width > MAX_IMAGE_WIDTH:
				ratio = width / (MAX_IMAGE_WIDTH * 1.0)
				return (MAX_IMAGE_WIDTH, int(height / ratio))
			else:
				return (width,height)
		else:
			if height > MAX_IMAGE_HEIGHT:
				ratio = height / (MAX_IMAGE_HEIGHT * 1.0)
				return (int(width / ratio), MAX_IMAGE_HEIGHT)
			else:
				return (width,height)
	except Exception as e:
		logger.exception(e)

	return (DEFAULT_IMAGE_WIDTH, DEFAULT_IMAGE_HEIGHT)

