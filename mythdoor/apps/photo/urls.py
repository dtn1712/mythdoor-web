from django.conf.urls import *

from django.views.generic import RedirectView

from mythdoor.apps.photo.views import UploadPhotoView

urlpatterns = [
   url(r"^upload/$", UploadPhotoView.as_view(), name="upload_photo"),
]
