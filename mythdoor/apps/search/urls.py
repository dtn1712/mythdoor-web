from django.contrib.auth.models import User
from django.conf.urls import *

from mythdoor.apps.search.views import PostSearchView

urlpatterns = [
    url(r'^', PostSearchView.as_view(), name='post_search'),
]
