from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response, get_object_or_404, get_list_or_404
from django.contrib.auth.models import User
from django.template import RequestContext

from haystack.generic_views import SearchView
from haystack.query import SearchQuerySet
from haystack.utils.geo import Point, D

from mythdoor.apps.app_views import AppBaseView
from mythdoor.apps.main.models import Post
from mythdoor.apps.main.constants import ACTIVE_STATUS, POST_CATEGORY_INFO_MAP
from mythdoor.apps.main.constants import POST_SUBCATEGORY_MAP, DEFAULT_PAGE_SIZE
from mythdoor.apps.main.constants import ITEM_CONDITION_WITH_BLANK_CHOICE, DISTANCE_RANGE
from mythdoor.apps.app_helper import get_current_postal_code, get_request_geodata
from mythdoor.apps.search.forms import PostSearchForm
from mythdoor.apps.app_settings import DEFAULT_DISTANCE_RANGE, DEFAULT_POSTAL_CODE

import json, logging, datetime

logger = logging.getLogger(__name__)

APP_NAME = "search"

class PostSearchView(SearchView,AppBaseView):
	app_name = APP_NAME
	template_name = "post"
	queryset = SearchQuerySet().models(Post)
	form_class = PostSearchForm

	def get_queryset(self):
		queryset = super(PostSearchView, self).get_queryset()
		query = self.request.GET.get('q', "")
		distance = self.request.GET.get('distance_range',DEFAULT_DISTANCE_RANGE)
		lat = self.request.GET.get("lat","")
		lng = self.request.GET.get("lng","")
		if len(query.strip()) == 0:
			if len(lat) == 0 or len(lng) == 0:
				return queryset.filter(status=ACTIVE_STATUS,is_deleted='false')
			else:
				point = Point(float(lat),float(lng))
				return queryset.filter(status=ACTIVE_STATUS,is_deleted='false').dwithin("location",point,D(mi=distance))
		else:
			if len(lat) == 0 or len(lng) == 0:
				return queryset.filter(text=query,status=ACTIVE_STATUS,is_deleted='false')
			else:
				point = Point(float(lat),float(lng))
				return queryset.filter(text=query,status=ACTIVE_STATUS,is_deleted='false').dwithin("location",point,D(mi=distance))

	def get_context_data(self, **kwargs):
		context = super(PostSearchView, self).get_context_data(**kwargs)
		context['load_post_start_index'] = DEFAULT_PAGE_SIZE
		context['is_show_header_search'] = False
		context['query'] = self.request.GET.get("q","")
		context['subcategory_map'] = POST_SUBCATEGORY_MAP
		context['distance_range'] = DISTANCE_RANGE
		context['search_location'] = self.request.GET.get("location","")
		context['search_lat'] = self.request.GET.get("lat","")
		context['search_lng'] = self.request.GET.get("lng","")
		return context




