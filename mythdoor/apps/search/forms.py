from django import forms
from django.utils.translation import pgettext, ugettext_lazy as _, ugettext

from haystack.forms import SearchForm

from moneyed import Money, USD

from mythdoor.apps.main.constants import ITEM_CONDITION_WITH_BLANK_CHOICE, POST_CATEGORY_WITH_BLANK_CHOICE
from mythdoor.apps.main.constants import DISTANCE_RANGE, YES_NO_WITH_BLANK_CHOICE

class PostSearchForm(SearchForm):
    category = forms.ChoiceField(required=False,choices=POST_CATEGORY_WITH_BLANK_CHOICE,widget=forms.Select(attrs={"class":"form-control"}))
    subcategory = forms.CharField(required=False,widget=forms.HiddenInput())
    condition = forms.ChoiceField(required=False,choices=ITEM_CONDITION_WITH_BLANK_CHOICE,widget=forms.Select(attrs={"class":"form-control"}))
    postal_code = forms.CharField(required=False)
    distance_range = forms.ChoiceField(choices=DISTANCE_RANGE,widget=forms.Select(attrs={"class":"form-control right-half-form"}))
    min_price = forms.DecimalField(required=False,widget=forms.TextInput(attrs={'placeholder': 'Min Price','class': "form-control left-half-form"}))
    max_price = forms.DecimalField(required=False,widget=forms.TextInput(attrs={'placeholder': 'Max Price','class': "form-control right-half-form"}))
    company = forms.CharField(required=False,widget=forms.TextInput(attrs={'placeholder': 'Company','class': "form-control"}))

    vin = forms.CharField(required=False)
    make = forms.CharField(required=False)
    model = forms.CharField(required=False)
    min_year = forms.CharField(required=False)
    max_year = forms.CharField(required=False)
    min_mileage = forms.CharField(required=False)
    max_mileage = forms.CharField(required=False)
    color = forms.CharField(required=False)
    title_status = forms.CharField(required=False)
    body_type = forms.CharField(required=False)
    transmission = forms.CharField(required=False)
    cylinder = forms.CharField(required=False)
    fuel = forms.CharField(required=False)

    min_area = forms.CharField(required=False)
    max_area = forms.CharField(required=False)
    num_bedroom = forms.CharField(required=False)
    num_bathroom = forms.CharField(required=False)
    is_allow_cat = forms.CharField(required=False)
    is_allow_dog = forms.CharField(required=False)
    housing_type = forms.CharField(required=False)

    def clean_min_area(self):
        value = self.cleaned_data["min_area"]
        if value != None and len(value) != 0:
            try:
                return int(float(value))
            except:
                raise forms.ValidationError(_("Please enter input as number"))
        return value


    def clean_max_area(self):
        value = self.cleaned_data["max_area"]
        if value != None and len(value) != 0:
            try:
                return int(float(value))
            except:
                raise forms.ValidationError(_("Please enter input as number"))
        return value

    def search_auto(self,sqs):
        if self.cleaned_data['vin']:
            sqs = sqs.filter(vin=self.cleaned_data['vin'])
        if self.cleaned_data['make']:
            sqs = sqs.filter(make__icontains=self.cleaned_data['make'])
        if self.cleaned_data['model']:
            sqs = sqs.filter(model__icontains=self.cleaned_data['model'])
        if self.cleaned_data['color']:
            sqs = sqs.filter(color=self.cleaned_data['color'])
        if self.cleaned_data['title_status']:
            sqs = sqs.filter(title_status=self.cleaned_data['title_status'])
        if self.cleaned_data['body_type']:
            sqs = sqs.filter(body_type=self.cleaned_data['body_type'])
        if self.cleaned_data['transmission']:
            sqs = sqs.filter(transmission=self.cleaned_data['transmission'])
        if self.cleaned_data['cylinder']:
            sqs = sqs.filter(cylinder=self.cleaned_data['cylinder'])
        if self.cleaned_data['fuel']:
            sqs = sqs.filter(fuel=self.cleaned_data['fuel'])

        if self.cleaned_data['min_year'] and self.cleaned_data['max_year']:
            sqs = sqs.filter(year__gte=self.cleaned_data['min_year'],year__lte=self.cleaned_data['max_year'])
        elif self.cleaned_data['min_price']:
            sqs = sqs.filter(year__gte=self.cleaned_data['min_year'])
        elif self.cleaned_data['max_price']:
            sqs = sqs.filter(year__lte=self.cleaned_data['max_year'])

        if self.cleaned_data['min_mileage'] and self.cleaned_data['max_mileage']:
            sqs = sqs.filter(mileage__gte=self.cleaned_data['min_mileage'],mileage__lte=self.cleaned_data['max_mileage'])
        elif self.cleaned_data['min_mileage']:
            sqs = sqs.filter(mileage__gte=self.cleaned_data['min_mileage'])
        elif self.cleaned_data['max_mileage']:
            sqs = sqs.filter(mileage__lte=self.cleaned_data['max_mileage'])

        return sqs

    def search_housing(self,sqs):
        if self.cleaned_data['min_area'] and self.cleaned_data['max_area']:
            sqs = sqs.filter(area__gte=self.cleaned_data['min_area'],area__lte=self.cleaned_data['max_area'])
        elif self.cleaned_data['min_area']:
            sqs = sqs.filter(area__gte=self.cleaned_data['min_area'])
        elif self.cleaned_data['max_area']:
            sqs = sqs.filter(area__lte=self.cleaned_data['max_area'])

        if self.cleaned_data['num_bedroom']:
            sqs = sqs.filter(num_bedroom=self.cleaned_data['num_bedroom'])
        if self.cleaned_data['num_bathroom']:
            sqs = sqs.filter(num_bathroom=self.cleaned_data['num_bathroom'])
        if self.cleaned_data['housing_type']:
            sqs = sqs.filter(housing_type=self.cleaned_data['housing_type'])
        if self.cleaned_data['is_allow_dog']:
            sqs = sqs.filter(is_allow_dog=int(self.cleaned_data['is_allow_dog']))
        if self.cleaned_data['is_allow_cat']:
            sqs = sqs.filter(is_allow_cat=int(self.cleaned_data['is_allow_cat']))

        return sqs


    def search(self):

        if not self.is_valid():
            return self.no_query_found()

        sqs = self.searchqueryset

        if self.load_all:
            sqs = sqs.load_all()
        else:

            if self.cleaned_data['category']:
                sqs = sqs.filter(category=self.cleaned_data['category'])
                if self.cleaned_data['category'] == "auto":
                    sqs = self.search_auto(sqs)
                elif self.cleaned_data['category'] == "housing":
                    sqs = self.search_housing(sqs)

            if self.cleaned_data['subcategory']:
                sqs = sqs.filter(subcategory=self.cleaned_data['subcategory'])

            if self.cleaned_data['condition']:
                sqs = sqs.filter(condition=self.cleaned_data['condition'])

            if self.cleaned_data['company']:
                sqs = sqs.filter(author=self.cleaned_data['company'].lower())

            if self.cleaned_data['min_price'] and self.cleaned_data['max_price']:
                sqs = sqs.filter(price__gte=self.cleaned_data['min_price'],price__lte=self.cleaned_data['max_price'])
            elif self.cleaned_data['min_price']:
                sqs = sqs.filter(price__gte=self.cleaned_data['min_price'])
            elif self.cleaned_data['max_price']:
                sqs = sqs.filter(price__lte=self.cleaned_data['max_price'])

        return sqs




