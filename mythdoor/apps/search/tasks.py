from celery import shared_task

from mythdoor.apps.search.helper import update_search_index as update_search_index_helper

@shared_task(max_retries=3)
def update_search_index():
	try:
		update_search_index_helper()
	except Exception as exc:
		raise update_search_index.retry(exc=exc,countdown=60*update_search_index.request.retries)
