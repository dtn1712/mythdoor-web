from haystack import indexes

from mythdoor.apps.main.models import Post

class PostIndex(indexes.SearchIndex, indexes.Indexable):
	text = indexes.CharField(document=True,use_template=True)
	author = indexes.CharField(model_attr='user_create')
	status = indexes.CharField(model_attr="post_status")
	is_deleted = indexes.BooleanField(model_attr='is_deleted',default='false')
	pub_date = indexes.DateTimeField(model_attr='create_time')
	content = indexes.CharField(model_attr="title")
	content_auto = indexes.EdgeNgramField(model_attr='title')
	category = indexes.CharField(model_attr="category")
	subcategory = indexes.CharField(model_attr="subcategory")
	condition = indexes.CharField(model_attr="item_condition")
	price = indexes.FloatField(model_attr="price",null=True)
	location = indexes.LocationField(model_attr="get_location")

	vin = indexes.CharField(model_attr="auto_detail__vin",null=True)
	make = indexes.CharField(model_attr="auto_detail__make",null=True)
	model = indexes.CharField(model_attr="auto_detail__model",null=True)
	year = indexes.CharField(model_attr="auto_detail__year",null=True)
	mileage = indexes.CharField(model_attr="auto_detail__mileage",null=True)
	color = indexes.CharField(model_attr="auto_detail__color",null=True)
	title_status = indexes.CharField(model_attr="auto_detail__title_status",null=True)
	body_type = indexes.CharField(model_attr="auto_detail__body_type",null=True)
	transmission = indexes.CharField(model_attr="auto_detail__transmission",null=True)
	cylinder = indexes.CharField(model_attr="auto_detail__cylinder",null=True)
	fuel = indexes.CharField(model_attr="auto_detail__fuel",null=True)

	address = indexes.CharField(model_attr="housing_detail__address",null=True)
	total_area = indexes.IntegerField(model_attr="housing_detail__total_area",null=True)
	num_bedroom = indexes.IntegerField(model_attr="housing_detail__num_bedroom",null=True)
	num_bathroom = indexes.IntegerField(model_attr="housing_detail__num_bathroom",null=True)
	is_allow_cat = indexes.BooleanField(model_attr="housing_detail__is_allow_cat",null=True)
	is_allow_dog = indexes.BooleanField(model_attr="housing_detail__is_allow_dog",null=True)
	housing_type = indexes.CharField(model_attr="housing_detail__housing_type",null=True)
	available_on = indexes.DateTimeField(model_attr="housing_detail__available_on",null=True)

	def get_model(self):
		return Post

	def index_queryset(self, using=None):
		return self.get_model().objects.all().select_related('user_create',"auto_detail","housing_detail")

	def prepare_price(self,obj):
		if obj.price != None:
			return float(obj.price.amount)
		return None

	def prepare_author(self,obj):
		return obj.user_create.userprofile.credential.get_display_credential().lower()


