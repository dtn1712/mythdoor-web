from tastypie.authorization import Authorization

class CustomAuthorization(Authorization):

	def base_checks(self, request, model_klass):
		# If it doesn't look like a model, we can't check permissions.
		if not model_klass or not getattr(model_klass, '_meta', None):
			return False

		# User must be logged in to check permissions.
		if not hasattr(request, 'user'):
			return False

		return model_klass

	def read_list(self, object_list, bundle):
		# GET-style methods are always allowed.
		return object_list

	def read_detail(self, object_list, bundle):
		# GET-style methods are always allowed.
		return True

	def create_list(self, object_list, bundle):
		klass = self.base_checks(bundle.request, object_list.model)

		if klass is False:
			return []

		# Logged in user is allowed to create object
		return object_list

	def create_detail(self, object_list, bundle):
		klass = self.base_checks(bundle.request, bundle.obj.__class__)

		if klass is False:
			raise Unauthorized("You are not allowed to access that resource.")

		# Logged in user is allowed to create object
		return True

	def update_list(self, object_list, bundle):
		klass = self.base_checks(bundle.request, bundle.obj.__class__)

		if klass is False:
			raise Unauthorized("You are not allowed to access that resource.")

		if bundle.request.user.is_staff:
			return object_list

		allowed = []
		for obj in object_list:
			if obj.user == bundle.request.user:
				allowed.append(obj)

		return allowed

	def update_detail(self, object_list, bundle):
		klass = self.base_checks(bundle.request, bundle.obj.__class__)

		if klass is False:
			raise Unauthorized("You are not allowed to access that resource.")

		return bundle.obj.user_post == bundle.request.user or bundle.request.user.is_staff

	def delete_list(self, object_list, bundle):
		klass = self.base_checks(bundle.request, bundle.obj.__class__)

		if klass is False:
			raise Unauthorized("You are not allowed to access that resource.")

		if bundle.request.user.is_staff:
			return object_list

		allowed = []
		for obj in object_list:
			if obj.user == bundle.request.user:
				allowed.append(obj)

		return allowed

	def delete_detail(self, object_list, bundle):
		klass = self.base_checks(bundle.request, bundle.obj.__class__)

		if klass is False:
			raise Unauthorized("You are not allowed to access that resource.")

		if hasattr(bundle.obj, 'user_post'):
			return bundle.obj.user_post == bundle.request.user
		else:
			return bundle.request.user.is_staff
