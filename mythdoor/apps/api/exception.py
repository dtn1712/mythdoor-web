import json

from tastypie.exceptions import TastypieError
from tastypie.http import HttpBadRequest, HttpApplicationError
from tastypie.http import HttpNotFound, HttpForbidden, HttpUnauthorized


class BaseHttpException(TastypieError):
    def __init__(self, message="",exception=None):
        response = {
            "error": {
                "message": message or "No error message was provided."
            }
        }
        if exception != None:
            response['error']['exception'] = exception

        self._response = response



class BadRequestException(BaseHttpException):

    @property
    def response(self):
        return HttpBadRequest(json.dumps(self._response),content_type='application/json')


class InternalErrorException(BaseHttpException):

    @property
    def response(self):
        return HttpApplicationError(json.dumps(self._response),content_type='application/json')


class NotFoundException(BaseHttpException):

    @property
    def response(self):
        return HttpNotFound(json.dumps(self._response),content_type='application/json')


class ForbiddenException(BaseHttpException):

    @property
    def response(self):
        return HttpForbidden(json.dumps(self._response),content_type='application/json')


class UnauthorizedException(BaseHttpException):

    @property
    def response(self):
        return HttpUnauthorized(json.dumps(self._response),content_type='application/json')




