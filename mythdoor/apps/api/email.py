from django.contrib.auth.models import User
from django.conf.urls import patterns, url, include
from django.core.validators import validate_email
from django.core.mail import send_mail

from tastypie.resources import ModelResource
from tastypie.authentication import MultiAuthentication, ApiKeyAuthentication
from tastypie.authorization import Authorization, ReadOnlyAuthorization
from tastypie.http import HttpCreated

from mythdoor.apps.api.serializer import UrlEncodeSerializer
from mythdoor.apps.main.constants import EMAIL_SEPARATOR
from mythdoor.apps.main.models import Email
from mythdoor.apps.mailer.tasks import send_email, send_mass_emails
from mythdoor.apps.api.exception import NotFoundException, BadRequestException
from mythdoor.apps.api import BaseAppResource

import ast

class EmailResource(BaseAppResource):

	class Meta:
		authentication = MultiAuthentication(ApiKeyAuthentication())
		authorization = ReadOnlyAuthorization()
		allowed_methods = ["get","post"]
		always_return_data = True
		serializer = UrlEncodeSerializer()
		resource_name = 'email'

	def prepend_urls(self):
		return [
			url(r"^(?P<resource_name>%s)/send/$" % self._meta.resource_name, self.wrap_view('send_email'), name="api_send_email"),
			url(r"^(?P<resource_name>%s)/(?P<unique_id>[\w\d_.-]+)/info/$" % self._meta.resource_name, self.wrap_view('get_email_info'), name="api_get_email_info"),
		]

	def send_email(self,request,**kwargs):
		self.method_check(request, allowed=['post'])
		self.is_authenticated(request)
		self.throttle_check(request)

		user_login = get_user_login_object(request)
		if user_login.is_staff == False:
			raise UnauthorizedException(message="You are not allow to perform this request")

		data = self.get_request_data(request)
		email_unique_id = data.get("unique_id",None)
		if email_unique_id != None:
			send_email.delay(email_unique_id)
			self.log_throttled_access(request)
			return self.create_response(request, {"status": "pending"})
		else:
			self.check_required_field(data,["to_emails","email_template","context_data"])

			to_emails = data['to_emails']
			to_emails_list = to_emails.split(EMAIL_SEPARATOR)
			for email in to_emails_list:
				if validate_email(email) == False:
					raise BadRequestException(message="Email {email} is not valid".format(email=email))

			email_template = data['email_template']
			context_data = data['context_data']
			try:
				ast.literal_eval(context_data)
			except Exception as e:
				raise BadRequestException(message="Context data have to be valid key-value map")

			email = Email.objects.create(to_emails=to_emails,email_template=email_template,context_data=context_data)
			send_email.delay(email.unique_id)

			self.log_throttled_access(request)
			return self.create_response(request, {"email_id": email.unique_id}, HttpCreated)


	def get_email_info(self,request,**kwargs):
		self.method_check(request, allowed=['get'])
		self.is_authenticated(request)
		self.throttle_check(request)

		user_login = get_user_login_object(request)
		if user_login == None or user_login.is_staff == False:
			raise UnauthorizedException(message="You are not allow to perform this request")

		unique_id = kwargs['unique_id']
		try:
			email = Email.objects.get(unique_id=unique_id)
			self.log_throttled_access(request)
			return self.create_response(request,{
				"to_emails": email.to_emails,
				"status": email.status,
				"email_template": email.email_template,
				"request_time": email.request_time,
			})
		except Email.DoesNotExist:
			raise NotFoundException(message="Cannot find email with this id")



