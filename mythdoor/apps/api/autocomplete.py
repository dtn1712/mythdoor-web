from django.core.cache import cache
from django.conf.urls import patterns, include, url
from django.contrib.auth.models import User

from tastypie.resources import ModelResource
from tastypie.authentication import Authentication
from tastypie.authorization import ReadOnlyAuthorization

from mythdoor.apps.api.exception import InternalErrorException
from mythdoor.apps.main.constants import DEFAULT_PAGE_SIZE, CACHE_KEY_SPECIAL_SEPARATOR
from mythdoor.apps.main.models import Company, UserProfile
from mythdoor.apps.company.helper import get_company_common_info
from mythdoor.apps.member.helper import get_user_common_info

from haystack.query import SearchQuerySet

import logging

logger = logging.getLogger(__name__)

def search_redis_key(limit,raw_query,key_type=None):
	if len(raw_query) == 0:
		return None
	else:
		objects = {}
		query = raw_query.lower().replace(" ","_")
		key_query = None
		if key_type == None:
			key_query = "*" + CACHE_KEY_SPECIAL_SEPARATOR + "*" + query + "*"
		else:
			key_query = "*" + CACHE_KEY_SPECIAL_SEPARATOR + key_type + CACHE_KEY_SPECIAL_SEPARATOR + "*" + query + "*"
		iterator = cache.iter_keys(key_query)
		for i in range(0,limit):
			try:
				obj = iterator.next()
				print obj
				objects[obj.split(CACHE_KEY_SPECIAL_SEPARATOR)[0]] = cache.get(obj)
			except StopIteration:
				break
		return objects


def search_haystack_companies(limit,query=None):
	objects = {}
	sqs = None
	if query != None and limit != None:
		sqs = SearchQuerySet().models(Company).autocomplete(content_auto=query)[:limit]
	else:
		sqs = SearchQuerySet().models(Company).all()[:limit]

	for result in sqs:
		if "company" in result.model_name.lower():
			objects[result.object.unique_id] = get_company_common_info(result.object)

	return objects


def search_haystack_all(limit,query=None):
	objects = {}
	sqs = None
	if query != None and limit != None:
		sqs = SearchQuerySet().autocomplete(content_auto=query)[:limit]
	else:
		sqs = SearchQuerySet().all()[:limit]

	for result in sqs:
		if "company" in result.model_name.lower():
			objects[result.object.unique_id] = get_school_common_info(result.object)

	return objects



class AutocompleteResource(ModelResource):

	class Meta:
		allowed_methods = ['get']
		resource_name = 'autocomplete'
		authentication = Authentication()
		authorization = ReadOnlyAuthorization()

	def prepend_urls(self):
		return [
			url(r"^(?P<resource_name>%s)/all/$" % self._meta.resource_name, self.wrap_view('get_autocomplete_query_all'), name="api_get_autocomplete_query_all"),
			url(r"^(?P<resource_name>%s)/company/$" % self._meta.resource_name, self.wrap_view('get_autocomplete_query_company'), name="api_get_autocomplete_query_company"),
			#url(r"^(?P<resource_name>%s)/school/$" % self._meta.resource_name, self.wrap_view('get_autocomplete_query_school'), name="api_get_autocomplete_query_school"),
			url(r"^(?P<resource_name>%s)/company/all/$" % self._meta.resource_name, self.wrap_view('get_autocomplete_all_companies'), name="api_get_autocomplete_all_companies"),
			#url(r"^(?P<resource_name>%s)/school/all/$" % self._meta.resource_name, self.wrap_view('get_autocomplete_all_schools'), name="api_get_autocomplete_all_schools"),
		]

	def get_autocomplete_query_all(self, request, **kwargs):
		self.method_check(request, allowed=['get'])
		self.is_authenticated(request)
		self.throttle_check(request)

		raw_query = request.GET.get("q","").rstrip()
		limit = request.GET.get("limit",DEFAULT_PAGE_SIZE)

		'''
			Try Redis Search first
		'''
		objects = None
		last_updated = None
		try:
			objects = search_redis_key(limit,raw_query)
			last_updated = cache.get("last_cache_updated")
		except Exception as e:
			logger.exception(e)

		'''
			If failed, try haystack
		'''
		if objects == None or last_updated == None:
			last_updated = cache.get("last_index_updated")
			try:
				objects = search_haystack_all(limit,raw_query)
			except Exception as e:
				logger.exception(e)
				raise InternalErrorException(message="Failed to load our autocomplete data. The team was noticed and will fix it soon")

		data = {
			'objects': objects,
			"last_updated": last_updated
		}

		self.log_throttled_access(request)
		return self.create_response(request, data)


	def get_autocomplete_query_company(self, request, **kwargs):
		self.method_check(request, allowed=['get'])
		self.is_authenticated(request)
		self.throttle_check(request)

		raw_query = request.GET.get("q","").rstrip()
		limit = request.GET.get("limit",DEFAULT_PAGE_SIZE)

		'''
			Try Redis Search first
		'''
		objects = None
		last_updated = None
		try:
			objects = search_redis_key(limit,raw_query,"company")
			last_updated = cache.get("companies_last_cache_updated")
		except Exception as e:
			logger.exception(e)

		'''
			If failed, try haystack
		'''
		if objects == None or last_updated == None:
			last_updated = cache.get("last_index_updated")
			try:
				objects = search_haystack_companies(limit,raw_query)
			except Exception as e:
				logger.exception(e)
				raise InternalErrorException(message="Failed to load our autocomplete data. The team was noticed and will fix it soon")

		is_list = False
		try:
			is_list = bool(int(request.GET.get("isList","0")))
		except:
			pass

		data = None
		if is_list == False:
			data = {
				'objects': objects,
				"last_updated": last_updated
			}
		else:
			data = []
			for key,value in objects.iteritems():
				data.append({"value":key,"label":value['name']})

		self.log_throttled_access(request)
		return self.create_response(request, data)


	def get_autocomplete_all_companies(self, request, **kwargs):
		self.method_check(request, allowed=['get'])
		self.is_authenticated(request)
		self.throttle_check(request)

		objects = None
		last_updated = None

		try:
			objects = cache.get("companies")
			last_updated = cache.get("companies_last_cache_updated")
		except Exception as e:
			logger.exception(e)

		if objects == None or last_updated == None:
			last_updated = cache.get("last_index_updated")
			try:
				objects = search_haystack_companies(None)
			except Exception as e:
				logger.exception(e)
				raise InternalErrorException(message="Failed to load our autocomplete data. The team was noticed and will fix it soon")

		is_list = False
		try:
			is_list = bool(int(request.GET.get("isList","0")))
		except:
			pass

		data = None
		if is_list == False:
			data = {
				'objects': objects,
				"last_updated": last_updated
			}
		else:
			data = []
			for key,value in objects.iteritems():
				data.append({"value":key,"label":value['name']})

		self.log_throttled_access(request)
		return self.create_response(request, data)



