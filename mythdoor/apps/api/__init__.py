
from django.http import QueryDict

from tastypie.resources import ModelResource

from mythdoor.apps.api.serializer import UrlEncodeSerializer
from mythdoor.apps.api.exception import BadRequestException

import json, traceback

class MultipartResource(object):

	def deserialize(self, request, data, format=None):
		if not format:
			format = request.META.get('CONTENT_TYPE', 'application/json')

		if format == 'application/x-www-form-urlencoded':
			return request.POST

		if format.startswith('multipart'):
			data = request.POST.copy()
			data.update(request.FILES)
			return data

		return super(MultipartResource, self).deserialize(request, data, format)

	def put_detail(self, request, **kwargs):
		if request.META.get('CONTENT_TYPE').startswith('multipart') and \
				not hasattr(request, '_body'):
			request._body = ''

		return super(MultipartResource, self).put_detail(request, **kwargs)

	def patch_detail(self, request, **kwargs):
		if request.META.get('CONTENT_TYPE').startswith('multipart') and \
				not hasattr(request, '_body'):
			request._body = ''

		return super(MultipartResource, self).patch_detail(request, **kwargs)


class BaseAppResource(MultipartResource,ModelResource):

	def check_required_field(self,data,fields):
		for field in fields:
			if field not in data:
				raise BadRequestException(message="Must provide {missing_key} when want answer for the question".format(missing_key=field))
			else:
				if data[field] == None or len(data[field]) == 0:
					raise BadRequestException(message="Must provide value for {field}".format(field=field))

	def get_request_data(self,request):
		content_type = request.META.get("CONTENT_TYPE","application/json")
		data = None
		if "text/plain" in content_type or  "application/json" in content_type:
			try:
				data = json.loads(request.body)
			except Exception as e:
				raise BadRequestException(message='Your data is not in correct format json. Please try again',exception=traceback.format_exc())
		else:
			try:
				data = self.deserialize(request, request.body, format=content_type)
			except Exception as e:
				raise BadRequestException(message="Failed to deserialize your data",exception=traceback.format_exc())

		if data == None:
			raise BadRequestException(message="Your request data is not in correct format. Please try again")

		return data
