from django.contrib.auth.models import User
from django.core.paginator import Paginator, InvalidPage
from django.db import models
from django.contrib.auth import authenticate, login
from django.conf.urls import patterns, url, include
from django.shortcuts import get_object_or_404

from tastypie import fields
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.authentication import MultiAuthentication
from tastypie.cache import SimpleCache
from tastypie.utils import trailing_slash
from tastypie.paginator import Paginator as TastypiePaginator
from tastypie.http import HttpCreated

from mythdoor.apps.api.authentication import AdvancedApiKeyAuthentication
from mythdoor.apps.main.models import Post
from mythdoor.apps.api.serializer import UrlEncodeSerializer
from mythdoor.apps.api.exception import NotFoundException, InternalErrorException, BadRequestException
from mythdoor.apps.api.authorization import CustomAuthorization
from haystack.query import SearchQuerySet

from urlparse import urlparse

import logging, traceback

logger = logging.getLogger(__name__)

class PostResource(ModelResource):

	user_create = fields.ForeignKey("mythdoor.apps.api.user.UserResource", 'user_create',full=True)

	class Meta:
		queryset = Post.objects.all()
		always_return_data = True
		detail_uri_name = "unique_id"
		resource_name = 'post'
		allowed_methods = ['get','post','put','patch','delete']
		serializer = UrlEncodeSerializer()
		cache = SimpleCache(timeout=10)
		authorization = CustomAuthorization()
		authentication = MultiAuthentication(AdvancedApiKeyAuthentication())
		paginator_class = TastypiePaginator
		ordering = ["create_time","content",'title']
		filtering = {
			'title': ALL,
			'content': ALL,
			'user_create': ALL_WITH_RELATIONS,
			'category': ALL,
			'subcategory': ALL,
			'create_time': ['exact', 'lt', 'lte', 'gte', 'gt'],
		}

	def prepend_urls(self):
		return [
			url(r"^(?P<resource_name>%s)/(?P<unique_id>[\w\d_.-]+)/$" % self._meta.resource_name, self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),
		]

	def dehydrate(self, bundle):
		bundle.data['user_create_display_credential'] = bundle.obj.user_create.get_profile().credential.get_display_credential()
		return bundle



