from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
from django.core.paginator import Paginator, InvalidPage
from django.db import models, IntegrityError
from django.contrib.auth import authenticate, login
from django.conf.urls import patterns, url, include
from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect, HttpResponseNotFound

from tastypie import fields
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.serializers import Serializer
from tastypie.authentication import MultiAuthentication, ApiKeyAuthentication, BasicAuthentication, Authentication
from tastypie.cache import SimpleCache
from tastypie.authorization import Authorization
from tastypie.utils import trailing_slash
from tastypie.models import ApiKey
from tastypie.http import HttpForbidden, HttpUnauthorized
from tastypie.paginator import Paginator as TastypiePaginator

from mythdoor.apps.api.authentication import AdvancedApiKeyAuthentication
from mythdoor.apps.member.helper import set_readable_username, MINIMUM_PASSWORD_LENGTH, validate_password
from mythdoor.apps.main.models import UserProfile
from mythdoor.apps.main.constants import DEFAULT_PAGE_SIZE, PAGE_PARAMETER, PAGE_SIZE_PARAMETER
from mythdoor.apps.api.exception import NotFoundException, InternalErrorException, ForbiddenException, BadRequestException
from mythdoor.apps.api.authorization import CustomAuthorization
from mythdoor.apps.api.serializer import UrlEncodeSerializer

from haystack.query import SearchQuerySet

import json, logging, traceback

logger = logging.getLogger(__name__)


class ProfileResource(ModelResource):

	user = fields.ToOneField("mythdoor.apps.api.user.UserResource",'user')

	class Meta:
		queryset = UserProfile.objects.exclude(user__is_staff=True)
		resource_name = 'profile'
		excludes = ['privacy_status']


class UserResource(ModelResource):

	userprofile = fields.ToOneField("mythdoor.apps.api.user.ProfileResource","userprofile",full=True)

	class Meta:
		authentication = MultiAuthentication(AdvancedApiKeyAuthentication())
		authorization = CustomAuthorization()
		allowed_methods = ['get', 'patch', 'put']
		always_return_data = True
		cache = SimpleCache(timeout=10)
		serializer = UrlEncodeSerializer()
		resource_name = 'user'
		queryset = User.objects.exclude(is_staff=True)
		excludes = ["id", 'email', 'password', 'is_active', 'is_staff', 'is_superuser','date_joined','last_login']
		detail_uri_name = "username"
		paginator_class = TastypiePaginator
		filtering = {
			'username': ALL,
			"first_name": ALL,
			"last_name": ALL,
		}

	def prepend_urls(self):
		return [
			url(r"^(?P<resource_name>%s)/(?P<username>[\w\d_.-]+)/$" % self._meta.resource_name, self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),

		]


