from django.views.generic.base import TemplateResponseMixin, ContextMixin
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView

from mythdoor.apps.app_helper import get_template_path, handle_request_get_message
from mythdoor.apps.app_helper import capitalize_first_letter, get_user_login_object
from mythdoor.apps.app_helper import get_base_template_path
from mythdoor.settings import SITE_NAME, DEFAULT_TEMPLATE_FILE_EXTENSION

import django_mobile

class AppBaseView(TemplateResponseMixin,ContextMixin):
	sub_path = "/page/"

	def dispatch(self, request, *args, **kwargs):
		if "session_id" in request.GET and request.GET['session_id'] != request.session.session_key:
			return HttpResponseRedirect("/")
			
		user_login = get_user_login_object(request)
		self.user_login = user_login
		return super(AppBaseView, self).dispatch(request, *args,**kwargs)

	def get_context_data(self, **kwargs):
		context = super(AppBaseView, self).get_context_data(**kwargs)
		context['user_login'] = self.user_login
		context['app_name'] = self.app_name
		context['page_type'] = self.app_name + "_" + self.template_name

		data = {
			'user_login': self.user_login,
			'site_name': capitalize_first_letter(SITE_NAME)
		}

		if self.user_login:
			context['new_notifications_count'] = self.user_login.userprofile.get_new_notifications_count()
			context['new_selling_messages_count'] = self.user_login.userprofile.get_new_selling_messages_count()
			context['new_buying_messages_count'] = self.user_login.userprofile.get_new_buying_messages_count()

		context['request_message'] =  handle_request_get_message(self.request,data)
		context['is_show_header_search'] = True
		context['app_base_template'] = get_base_template_path(self.app_name)
		# context['app_base_template'] = get_base_template_path(django_mobile.get_flavour(self.request),self.app_name)

		context['template_type'] = "normal"
		flavour = django_mobile.get_flavour(self.request)
		if flavour != None:
			context['template_type'] = "normal" if flavour == "full" else "responsive"

		return context

	def get_template_names(self):
		return [get_template_path(self.app_name,self.template_name,self.sub_path)]

	# def get_template_names(self):
	# 	return [get_template_path(self.app_name,self.template_name,django_mobile.get_flavour(self.request),self.sub_path)]

