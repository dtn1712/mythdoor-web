from django.conf.urls import patterns, include, url
from django.conf.urls import handler404, handler500
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

from tastypie.api import Api

from mythdoor import settings
from mythdoor.apps.api.autocomplete import AutocompleteResource
from mythdoor.apps.api.email import EmailResource

#from djrill import DjrillAdminSite

#admin.site = DjrillAdminSite()
admin.autodiscover()

v1_api = Api(api_name='v1')
v1_api.register(AutocompleteResource())
v1_api.register(EmailResource())

urlpatterns = [
    # Admin URL
    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin/defender/', include('defender.urls')), # defender admin

    # Auth
    url(r'^account/', include("mythdoor.apps.account.urls")),

    # Haystack app URL
    url(r'^search/', include("mythdoor.apps.search.urls")),

    url(r'^ajax/', include("mythdoor.apps.ajax.urls")),

    # API
    url(r'^api/', include(v1_api.urls)),

    # mythdoor URL
    url(r'^$', include('mythdoor.apps.main.urls')),
    url(r'^user/',include('mythdoor.apps.member.urls')),
    url(r'^post/',include('mythdoor.apps.post.urls')),
    url(r'^photo/',include('mythdoor.apps.photo.urls')),
    url(r'^about/',include('mythdoor.apps.about.urls')),

    url(r'^i18n/', include('django.conf.urls.i18n')),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

handler404 = "mythdoor.apps.main.views.handler404"
handler500 = "mythdoor.apps.main.views.handler500"

