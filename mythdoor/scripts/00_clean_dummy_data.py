import os, sys, string, datetime

SETTING_PATH = os.path.abspath(__file__ + "/../../")
PROJECT_PATH = os.path.abspath(__file__ + "/../../../")

sys.path.append(SETTING_PATH)
sys.path.append(PROJECT_PATH)
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'

import django
django.setup()

from django import db
from django.contrib.auth.models import User

from mythdoor.apps.main.models import *

def main():
	print "... RUNNING CLEANING DUMMY DATA SCRIPT ..."
	try:

		print "Clean Dummy Data Successfully"
		Question.objects.all().delete()
		User.objects.all().delete()
		Answer.objects.all().delete()
	except:
		print "Clean Dummy Data Failed"
		raise

	db.close_connection()

if __name__ == '__main__':
	stage = sys.argv[1]
	if len(sys.argv) == 4 and stage == "dev" and sys.argv[3] == "--cleandummydata":
		main()
