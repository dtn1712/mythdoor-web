import os, sys

SETTING_PATH = os.path.abspath(__file__ + "/../../")
PROJECT_PATH = os.path.abspath(__file__ + "/../../../")

sys.path.append(SETTING_PATH)
sys.path.append(PROJECT_PATH)
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'

import django
django.setup()

from django.contrib.auth.models import User
from django import db
from django.utils import timezone

from mythdoor.apps.app_helper import get_any_admin_object, generate_unique_id

names = ["Smith","Anderson","Clark","Wright","Mitchell","Johnson","Thomas","Rodriguez","Lopez"]

PASSWORD = "123456"

def main():
	print "...RUNNING GENERATE USERS SCRIPT..."
	try:
		admin = get_any_admin_object()
		i = 1
		for name in names:
			test_email = "test"+str(i)+"@univtop.com"
			try:
				check_user = User.objects.get(email=test_email)
			except User.DoesNotExist:
				user = User.objects.create(username=name.lower(),email=test_email,last_login=timezone.now())
				user.set_password(PASSWORD)
				user.first_name = name 
				user.save()
				
			i += 1
		print "Generate Users Successfully"
	except:
		print "Generate User Failed"
		raise

if __name__ == "__main__":
	stage = sys.argv[1]
	if stage == "dev": 
		main()
