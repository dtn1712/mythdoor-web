import sys,os

SETTING_PATH = os.path.abspath(__file__ + "/../../")
PROJECT_PATH = os.path.abspath(__file__ + "/../../../")

sys.path.append(SETTING_PATH)
sys.path.append(PROJECT_PATH)

os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'

import django
django.setup()

from django import db
from django.contrib.auth.models import User

from mythdoor.apps.main.models import UserProfile, UserCredential


def main():
	print "...SETUP ADMIN INFO..."
	try:
		admins = User.objects.filter(is_staff=True,is_superuser=True)
		for admin in admins:
			profiles = UserProfile.objects.filter(user=admin)
			profile = None
			if len(profiles) == 0:
				profile = UserProfile.objects.create(user=user)
			else:
				profile = profiles[0]

			profile.initial = admin.username[0:2]
			profile.email_type = "others"
			profile.save()

			if admin.userprofile.credential == None:
				credential = UserCredential.objects.create()
				credential.credential_type = "others"
				admin.userprofile.credential = credential
				admin.userprofile.save()
				
			admin.save()

		print "Setup Admin Info Successfully"
	except:
		stage = sys.argv[1]
		if stage == "prod":
			print "Seem like the environment is " \
					"not correct. Note that this script supposed to " \
					"work in the server environment for prod stage (e.g Heroku) " \
					"Please double check and try again"
		else:
			raise


if __name__ == "__main__":
    main()
