import sys, os

PATH = os.path.abspath(__file__ + "/../../")

PROJECT_NAME = "mythdoor"

def reset_db():
	db_path = PROJECT_NAME + "/db/dev/" + PROJECT_NAME + ".db"
	if os.path.exists(db_path) :
		remove_db_cmd = "rm " + db_path
		print "Remove sqlite database file: " + remove_db_cmd
		os.system(remove_db_cmd)
	else:
		reset_db_cmd = "python manage.py flush"
		os.system(reset_db_cmd)

	build_cmd = "python build.py dev"
	print "Build settings: " + build_cmd
	try:
		os.system(build_cmd)
	except:
		pass

	# syncdb_cmd = "python manage.py syncdb"
	# print "Syncdb: " + syncdb_cmd
	# os.system(syncdb_cmd)

	remove_migration_cmd = "rm -rf " + PROJECT_NAME + "/apps/main/migrations"
	print "Remove old migration: " + remove_migration_cmd
	os.system(remove_migration_cmd)

	initial_migration_cmd = "python manage.py makemigrations"
	print "Initialize main app migration:" + initial_migration_cmd
	os.system(initial_migration_cmd)

	# f = open("Procfile","r")
	# for line in f:
	# 	if "migration" in line:
	# 		migration_cmd = line.split(":")[1]
	# 		print "Perform migration: " + migration_cmd
	# 		os.system(migration_cmd)

	migration_cmd = "python manage.py migrate"
	print "Run migration:" + migration_cmd
	os.system(migration_cmd)

	print "Rebuild: " + build_cmd
	os.system(build_cmd)

	print "Load Data From Fixtures: "
	fixture_json_src =  PATH + "/" + PROJECT_NAME + "/db/fixtures/json/"
	files = os.listdir(fixture_json_src)
	files.sort()
	for filename in files:
		cmd = "python manage.py loaddata " + fixture_json_src + filename
		os.system(cmd)

	# import_zip_cmd = "python manage.py import_zipdata " + PROJECT_NAME + "/db/geolocation/data/CivicSpaceUSZIPCodes/zipcode.csv"
	# os.system(import_zip_cmd)

def main():
	confirm = ""
	while confirm.lower() != "y" and confirm.lower() != "n":
		confirm = raw_input("This command will complete reset the database. Your data will be lost. This command will only work for development database. Please enter y to proceed or n to cancel:")

	if confirm.lower() == "y":
		reset_db()


if __name__ == "__main__":
    main()

