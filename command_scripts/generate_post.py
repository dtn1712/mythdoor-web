import os, sys

PATH = os.path.abspath(__file__ + "/../../")

sys.path.append(PATH)

PROJECT_NAME = "mythdoor"

os.environ['DJANGO_SETTINGS_MODULE'] = PROJECT_NAME + '.settings'


from django import db

from mythdoor.apps.app_helper import generate_unique_id
from mythdoor.apps.main.models import Post

import random

def main():
	print "...RUNNING GENERATE POSTS SCRIPT..."
	try:
		list_id = []
		posts = Post.objects.all()
		for post in posts:
			list_id.append(post.pk)

		for i in range(0,100):
			clone_post = posts[0]
			new_pk = random.randint(1,1000)
			while new_pk in list_id:
				new_pk = random.randint(1,1000)
			clone_post.pk = new_pk
			clone_post.unique_id = generate_unique_id()
			clone_post.save()
		print "Generate Posts Successfully"
	except:
		print "Generate Post Failed"
		raise

	db.close_connection()

if __name__ == "__main__":
	main()
