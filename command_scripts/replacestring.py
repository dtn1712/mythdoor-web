import sys, os

PATH = os.path.abspath(__file__ + "/../../")

sys.path.append(PATH)

SOURCE_NAME = "WEBSITE_HOMEPAGE"
TARGET_NAME = "WEBSITE_URL"

file_types = ['.py',".js",".css"]

for path, dirs, files in os.walk(PATH):
	for filename in files:
		fullpath = os.path.join(path,filename)
		for file_type in file_types:
			if file_type in fullpath and ".pyc" not in fullpath and __file__ not in fullpath and "venv" not in fullpath:
				with open(fullpath, "r") as f:
					altered_lines = []
					for line in f:
						trim_line = line.rstrip()
						altered_lines.append(trim_line.replace(SOURCE_NAME,TARGET_NAME))
				with open(fullpath, "w") as f:
					f.write('\n'.join(altered_lines) + '\n')
