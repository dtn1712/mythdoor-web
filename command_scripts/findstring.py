import sys, os

PATH = os.path.abspath(__file__ + "/../../")

sys.path.append(PATH)

STRING = "CACHE_KEY_SPECIAL_SEPARATOR"

file_types = ['.py',".js",".css",".html",".txt"]

for path, dirs, files in os.walk(PATH):
	for filename in files:
		fullpath = os.path.join(path,filename)
		for file_type in file_types:
			if file_type in fullpath and ".pyc" not in fullpath and __file__ not in fullpath and "venv" not in fullpath:
				with open(fullpath, "r") as f:
					for line in f:
						if STRING in line:
							print fullpath
							break
