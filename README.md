# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Version: 1.0

### Before Setup ###

* OS: Unix-based (Linux and/or OS X). No Windows support.

* Required libraries: 
    + [python-pip](https://pypi.python.org/pypi/pip)
        - Linux: `sudo apt-get install python-pip`.
        - Mac: `sudo easy_install pip`.
    + [virtualenv](https://virtualenv.pypa.io/en/latest/): `sudo pip install virtualenv`.

* Configuration: View directory `univtop/config`. There are common configurations for all stages and separate configuration for each stage (dev and prod).  

* Dependencies: View folder `reqs`. There are common dependencies and also dependencies for dev stage and prod stage.

### Setup ###

1. Create virtual environment for the project: `virtualenv venv-univtop`.

2. Source the virtual environment: `source venv-univtop/bin/activate`.

3. Install dependencies: `pip install -r reqs/dev.txt`. 

    **Note**: You might need to install python-dev and libxml to be able to install dependency lxml.

    + Linux: `sudo apt-get install libxml2-dev libxslt1-dev python-dev zlib1g-dev libssl-dev libmemcached-dev`.
    + Mac: `brew install python libxml2 libmemcached`.
        - If you encounter error " 'libmemcached/memcached.h' file not found ", try to install pylibmc separately: `pip install pylibmc --install-option="--with-libmemcached=/usr/local/Cellar/libmemcached/<version_number>/"`.
        - If you encounter error " 'libxml/xmlversion.h' file not found ", try `xcode-select --install` to install or upgrade command line tool for Xcode.

    Use Google if you failed to install some dependencies. The problems are often just missing some library packages.

4. Setup database.
    + Create `dev` folder in `univtop/db` if it is not created yet (`mkdir univtop/db/dev`). 
    + Create the database: `python manage.py syncdb`.
    + Run the migration: `python manage.py migrate allauth.socialaccount && python manage.py migrate allauth.socialaccount.providers.facebook && python manage.py migrate djcelery && python manage.py migrate cities_light && python manage.py migrate main`. For the migrate command, you can view the "migration" line in `Procfile`.

5. Build the configuration: `python build.py dev`. The `build.py` script is written to ease the process of adding support script. It basically run all the script file in the folder `univtop/scripts`. You can always add your own script in the file CATALOGUE. Currently, the scripts contain code to build the final `settings.py` file (combine `common.py` configuration and the stage configuration file), and also script for dev ops and minimize static file.

6. Install elastic search. Download and follow the instruction to run it here. It is pretty straightforward: https://www.elastic.co/downloads/elasticsearch

7. Run the server.
    + For local dev, `python manage.py runserver`.
    + For beta testing, push it to heroku.
    + For prod, follow [this guide](http://michal.karzynski.pl/blog/2013/06/09/django-nginx-gunicorn-virtualenv-supervisor/).
    
### Docker Setup ###
Docker is a container technology that that allows running applications inside a container environment.  

1) Generate ssh keys with ssh-keygen -q -t rsa -N '' -f repo-key which will give you repo-key and repo-key.pub files.

2) Add repo-key.pub to your repository deployment keys.

3) go to [your repository] -> Settings -> Deploy keys

4).  Install Docker tool set and follow installation instructions at https://docs.docker.com/mac/step_one/ 

5).  Open docker quick start terminal and  Create a docker project directory eg:- /MythDoor 

6).  Copy Dockerfile from bitbucket git repository to /MythDoor directory

7).  Run Docker build command with a local docker image repository name eg:- docker build -t <name>/<repoName>:v1 . 

8).  Docker will build image and store the custom image in <repoName> repository with <name> and tag v1

9).  Run 'Docker images' and copy ImageID of the custom image built and Run Docker using comand 'docker run -d -p 8000:8000 <ImageID>

10).  Run 'Docker-machine ip default' and note IP address output.  Use IP:8000 in web browser and check the output.