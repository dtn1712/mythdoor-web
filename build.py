import sys, os, shortuuid, netifaces

STAGE_NAME = {"dev":None, "prod": None}

if len(sys.argv) < 2 or sys.argv[1] not in STAGE_NAME:
	valid_stage = str(STAGE_NAME.keys()).replace("[","").replace("]","")
	print "Usage: python " + __file__ + " argv[1]"
	print "argv[1] can be one of the following: " + valid_stage
	sys.exit()

CATALOGUE = "CATALOGUE"
COMMENT_CHARACTER = "<!--"

PROJECT_ROOT = os.path.dirname(__file__)
PROJECT_NAME = "mythdoor"
PROJECT_SCRIPT = "scripts"
PROJECT_PATH = os.path.join(PROJECT_ROOT, PROJECT_NAME, PROJECT_SCRIPT)

CATALOGUE_PATH = os.path.join(PROJECT_PATH,CATALOGUE)


argument = " ".join(sys.argv)

list_file = []
f = open(CATALOGUE_PATH,"r")
for filename in f:
    if len(filename.replace("\n","")) != 0 and filename.startswith(COMMENT_CHARACTER) == False:
        list_file.append(filename.replace("\n",""))

build_version_id = shortuuid.uuid()

if sys.argv[1] != 'dev':
	private_ip = netifaces.ifaddresses('eth0')[2][0]['addr']
	f1 = open("gunicorn.py.ini",'r')
	f2 = open("gunicorn-local.py.ini","w")
	for line in f1:
		if "bind" not in line:
			f2.write(line)
		else:
			f2.write("bind = '" + private_ip + ":8000'\n")
	f1.close()
	f2.close()
	os.system("rm gunicorn.py.ini && mv gunicorn-local.py.ini gunicorn.py.ini")

for script in list_file:
	cmd = "python -Wi " + PROJECT_PATH + "/" + script + argument[argument.find(" "):] + " " + build_version_id
	os.system(cmd)

if sys.argv[1] != 'dev':
	os.system("python manage.py collectstatic")
	os.system("sudo supervisorctl restart all && sudo service nginx restart")
